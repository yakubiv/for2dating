<?php
include 'header.php';
?>
      <!-- display-one -->

      <div class="display-one">
                
        <div class="center-all-block">
          
          <div class="left-center-all-block">

            <!-- header-display-one -->

            <div class="header-display-one">
              <span class="big">
                Развивающий  конструктор-липучка
              </span>

              <span class="small">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit
              </span>
            </div>

            <!-- end-header-display-one -->

            <!-- slider-display-one -->

            <div class="slider-display-one">

              <div class="regular slider">

                <!--  -->

                <div>
                  <div class="into-slider-one">

                    <div class="left-into-slider-one">
                      <div class="null-block">
                        <div class="bg-stock">
                          <div class="icon-pig">
                            <img src="img/icon-pig.png" alt="not-image">
                          </div>
                        </div>
                      </div>

                      <img src="img/three-image-catalog.png" alt="not-image">
                    </div>

                    <div class="right-into-slider-one">
                      <div class="block-text-right-into-slider">
                        <span class="text">
                          Конструктор-липучка  Bunchems 150 шт.
                        </span>

                        <span class="price-line">
                          1990
                        </span>

                        <span class="price-under">
                          399 руб.
                        </span>
                      </div>

                      <div class="block-btn-slider">
                        <a href="small-basket.php" id="wicartbutton_003" onclick="cart.addToCart(this, '003', priceList['003'])" class="btn-slider">
                          <span>
                            Купить сейчас
                          </span>
                        </a>
                      </div>
                    </div>

                  </div>                  
                </div>

                <!--  -->

                <!--  -->

                <div>
                  <div class="into-slider-one">

                    <div class="left-into-slider-one">
                      <div class="null-block">
                        <div class="bg-stock">
                          <div class="icon-pig">
                            <img src="img/icon-pig.png" alt="not-image">
                          </div>
                        </div>
                      </div>

                      <img src="img/eight-image-catalog.png" alt="not-image">
                    </div>

                    <div class="right-into-slider-one">
                      <div class="block-text-right-into-slider">
                        <span class="text">
                          Квадратный чемодан с Bunchems 400 шт.
                        </span>

                        <span class="price-line">
                          1500
                        </span>

                        <span class="price-under">
                          299 руб.
                        </span>
                      </div>

                      <div class="block-btn-slider">
                        <a href="small-basket.php" id="wicartbutton_008" onclick="cart.addToCart(this, '008', priceList['008'])" class="btn-slider">
                          <span>
                            Купить сейчас
                          </span>
                        </a>
                      </div>
                    </div>

                  </div>                  
                </div>

                <!--  -->

                <!--  -->

                <div>
                  <div class="into-slider-one">

                    <div class="left-into-slider-one">
                      <div class="null-block">
                        <div class="bg-stock">
                          <div class="icon-pig">
                            <img src="img/icon-pig.png" alt="not-image">
                          </div>
                        </div>
                      </div>

                      <img src="img/elewen-image-catalog.png" alt="not-image">
                    </div>

                    <div class="right-into-slider-one">
                      <div class="block-text-right-into-slider">
                        <span class="text">
                          Конструктор-липучка  Bunchems 800 шт.
                        </span>

                        <span class="price-line">
                          1990
                        </span>

                        <span class="price-under">
                          399 руб.
                        </span>
                      </div>

                      <div class="block-btn-slider">
                        <a href="small-basket.php" id="wicartbutton_011" onclick="cart.addToCart(this, '011', priceList['011'])" class="btn-slider">
                          <span>
                            Купить сейчас
                          </span>
                        </a>
                      </div>
                    </div>

                  </div>                  
                </div>

                <!--  -->

              </div>

            </div>

            <!-- end-slider-display-one -->

          </div>

        </div>

      </div>

      <!-- end-display-one -->

      <!-- display-two -->

      <div class="bg-header-display-two">
        <span>
          Выберите свой конструктор Bunchems
        </span>
      </div>

      <!-- content-display-two -->

      <div class="content-display-two">

        <div class="center-content-display-two">

          <!-- left-sidebar -->

          <div class="left-sidebar">

            <div class="center-left-sidebar">

              <!--  -->

              <div class="one-of-left-sidebar one">
                <div class="block-icon-sidebar">
                  <div class="icon-sidebar">
                    <img src="img/icon-one-sidebar.png" alt="not-image">
                  </div>
                </div>

                <div class="block-text-sidebar">
                  <span>
                    Ребенок учится<br>
                    фантазировать и творчески<br>
                    мыслить
                  </span>
                </div>
              </div>

              <!--  -->

              <!--  -->

              <div class="one-of-left-sidebar">
                <div class="block-icon-sidebar">
                  <div class="icon-sidebar">
                    <img src="img/icon-two-sidebar.png" alt="not-image">
                  </div>
                </div>

                <div class="block-text-sidebar">
                  <span>
                    Становится более<br>
                    усидчивым и внимательным
                  </span>
                </div>
              </div>

              <!--  -->

              <!--  -->

              <div class="one-of-left-sidebar">
                <div class="block-icon-sidebar">
                  <div class="icon-sidebar">
                    <img src="img/icon-three-sidebar.png" alt="not-image">
                  </div>
                </div>

                <div class="block-text-sidebar">
                  <span>
                    Развивает способность<br>
                    к точным наукам
                  </span>
                </div>
              </div>

              <!--  -->

            </div>

          </div>

          <!-- end-left-sidebar -->

          <!-- catalog-display-two -->

          <div class="catalog-display-two">
            
            <div class="header-catalog-display-two">
              <span>
                Мини
              </span>
            </div>

            <!-- line-block-catalog -->

            <div class="line-block-catalog">

              <div class="center-line-block-catalog">

                <!--  -->

                <div class="one-of-line-block-catalog one">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="bunchems-50.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/one-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Конструктор-липучка<br>
                      Bunchems 50 шт.
                    </span>

                    <span class="price-line">
                      1450
                    </span>

                    <span class="price-under">
                      149 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_001" onclick="cart.addToCart(this, '001', priceList['001'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

                <!--  -->

                <div class="one-of-line-block-catalog">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="bunchems-100.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/two-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Конструктор-липучка<br>
                      Bunchems 100 шт.
                    </span>

                    <span class="price-line">
                      1500
                    </span>

                    <span class="price-under">
                      299 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_002" onclick="cart.addToCart(this, '002', priceList['002'])">Купить</button>
                  </div>

                </div>

                <!--  -->

                <!--  -->

                <div class="one-of-line-block-catalog three">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="bunchems-150.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/three-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Конструктор-липучка<br>
                      Bunchems 150 шт.
                    </span>

                    <span class="price-line">
                      1990
                    </span>

                    <span class="price-under">
                      399 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_003" onclick="cart.addToCart(this, '003', priceList['003'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

              </div>

            </div>

            <!-- end-line-block-catalog -->

            <div class="header-two-catalog-display-two">
              <span>
                Стандарт
              </span>
            </div>

            <!-- line-block-catalog -->

            <div class="line-block-catalog">

              <div class="center-line-block-catalog">

                <!--  -->

                <div class="one-of-line-block-catalog one">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="bunchems-200.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/four-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Конструктор-липучка<br>
                      Bunchems 200 шт.
                    </span>

                    <span class="price-line">
                      2200
                    </span>

                    <span class="price-under">
                      149 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_004" onclick="cart.addToCart(this, '004', priceList['004'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

                <!--  -->

                <div class="one-of-line-block-catalog">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="bunchems-300.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/five-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Конструктор-липучка<br>
                      Bunchems 300 шт.
                    </span>

                    <span class="price-line">
                      1500
                    </span>

                    <span class="price-under">
                      299 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_005" onclick="cart.addToCart(this, '005', priceList['005'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

                <!--  -->

                <div class="one-of-line-block-catalog three">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="bunchems-400.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/six-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Конструктор-липучка<br>
                      Bunchems 400 шт.
                    </span>

                    <span class="price-line">
                      1990
                    </span>

                    <span class="price-under">
                      399 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_006" onclick="cart.addToCart(this, '006', priceList['006'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

              </div>

            </div>

            <!-- end-line-block-catalog -->

            <div class="header-two-catalog-display-two plus-box">
              <span>
                Супер-удобно<br> <span>(+ коробка)</span>
              </span>
            </div>

            <!-- line-block-catalog -->

            <div class="line-block-catalog">

              <div class="center-line-block-catalog">

                <!--  -->

                <div class="one-of-line-block-catalog one">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="apple-box-bunchems-400.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/seven-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Чемодан-яблоко<br>
                      с Bunchems 400 шт.
                    </span>

                    <span class="price-line">
                      2200
                    </span>

                    <span class="price-under">
                      149 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_007" onclick="cart.addToCart(this, '007', priceList['007'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

                <!--  -->

                <div class="one-of-line-block-catalog">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="quadro-box-bunchems-400.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/eight-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Квадратный чемодан<br>
                      с Bunchems 400 шт.
                    </span>

                    <span class="price-line">
                      1500
                    </span>

                    <span class="price-under">
                      299 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_008" onclick="cart.addToCart(this, '008', priceList['008'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

                <!--  -->

                <div class="one-of-line-block-catalog three">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="three-box-bunchems-600.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/nine-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Трехъярусный чемодан<br>
                      с Bunchems 600 шт.
                    </span>

                    <span class="price-line">
                      1990
                    </span>

                    <span class="price-under">
                      399 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_009" onclick="cart.addToCart(this, '009', priceList['009'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

              </div>

            </div>

            <!-- end-line-block-catalog -->

            <div class="header-two-catalog-display-two">
              <span>
                Мега-пак
              </span>
            </div>

            <!-- line-block-catalog -->

            <div class="line-block-catalog">

              <div class="center-line-block-catalog two">

                <!--  -->

                <div class="one-of-line-block-catalog two">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="bunchems-700.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/ten-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Конструктор-липучка<br>
                      Bunchems 700 шт.
                    </span>

                    <span class="price-line">
                      2200
                    </span>

                    <span class="price-under">
                      149 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_010" onclick="cart.addToCart(this, '010', priceList['010'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

                <!--  -->

                <div class="one-of-line-block-catalog">

                  <div class="block-image-one-of-catalog">
                    <div class="null-block">
                      <div class="hover-block-catalog">
                        <a href="bunchems-800.php">
                          <span>
                            Подробнее
                          </span>
                        </a>
                      </div>
                    </div>

                    <div class="parent">
                      <div>
                        <img src="img/elewen-image-catalog.png" alt="not-image">
                      </div>
                    </div>
                  </div>

                  <div class="block-text-one-of-catalog">
                    <span class="text">
                      Конструктор-липучка<br>
                      Bunchems 800 шт.
                    </span>

                    <span class="price-line">
                      1500
                    </span>

                    <span class="price-under">
                      299 руб.
                    </span>
                  </div>

                  <div class="block-btn-catalog">
                    <button class="btn-catalog" id="wicartbutton_011" onclick="cart.addToCart(this, '011', priceList['011'])"><span>Купить</span></button>
                  </div>

                </div>

                <!--  -->

              </div>

            </div>

            <!-- end-line-block-catalog -->

          </div>

          <!-- end-catalog-display-two -->

        </div>
        
      </div>

      <!-- end-content-display-two -->

      <!-- end-display-two -->

      <!-- display-three -->

      <div class="display-three">
        
        <div class="center-all-block">
          
          <div class="left-center-all-block">

            <div class="null-block">
              <div class="block-around-display-three">
                <div class="null-block">
                  <div class="shadow-around">                    
                  </div>
                </div>

                <div class="icon-gift">                  
                </div>
              </div>
            </div>

            <div class="header-display-three">
              <span class="big">
                Выбираете конструктор <span>Bunchems</span> в подарок?
              </span>

              <span class="small">
                Теперь есть возможность заказать его в подарочной упаковке!
              </span>
            </div>

            <div class="arrow-display-three">
              <div class="left-arrow-display-three">                
              </div>

              <div class="border-display-three">
                <span>
                  Сделайте пометку в заказе или сообщите менеджеру,<br>
                  что хотите получить набор в подарочной упакове.
                </span>
              </div>

              <div class="right-arrow-display-three">                
              </div>
            </div>

            <div class="text-under-arrow-display-three">
              <span class="small">
                Подарите ребенку набор развивающего конструктора в
              </span>

              <span class="big">
                уникальной праздничной упаковке!
              </span>
            </div>

            <div class="coints-display-three">
              <span>
                +200 руб. к заказу
              </span>
            </div>

          </div>

        </div>

      </div>

      <!-- end-display-three -->

      <!-- display-four -->

      <div class="display-four">
        
        <div class="center-all-block">
          
          <div class="left-center-all-block">

            <div class="header-display-four">
              <span>
                Что вы найдете в наборе
              </span>
            </div>

            <!-- content-four -->

            <div class="content-display-four">

              <!-- left-content-four -->

              <div class="left-line-content-display-four">

                <div class="block-one-of-line-content">
                  <div class="one-of-line-content one">
                    <div class="null-block">
                      <div class="image-content-four one">                        
                      </div>
                    </div>

                    <span>
                      Коробка<br>
                      с удобной ручкой
                    </span>
                  </div>
                </div>

                <!--  -->

                <div class="block-two-of-line-content">
                  <div class="one-of-line-content two">
                    <div class="null-block">
                      <div class="image-content-four two">                        
                      </div>
                    </div>

                    <span>
                      Аксессуары:<br>
                      глазики, ручки,<br>
                      ножки, усы, крылья,<br>
                      шляпки и други
                    </span>
                  </div>
                </div>

                <!--  -->

              </div>

              <!-- end-left-content-four -->

              <!-- right-content-four -->

              <div class="right-line-content-display-four">

                <div class="block-one-of-line-content">
                  <div class="one-of-line-content three">
                    <div class="null-block">
                      <div class="image-content-four three">                        
                      </div>
                    </div>

                    <span>
                      Яркая инструкция<br>
                      с примерами<br>
                      создания фигурок
                    </span>
                  </div>
                </div>

                <!--  -->

                <div class="block-two-of-line-content">
                  <div class="one-of-line-content four">
                    <div class="null-block">
                      <div class="image-content-four four">                        
                      </div>
                    </div>

                    <span>
                      Разноцветные<br>
                      шарики-липучки
                    </span>
                  </div>
                </div>

                <!--  -->

              </div>

              <!-- end-right-content-four -->

            </div>

            <!-- end-content-four -->

            <div class="block-btn-display-four">
              <div class="btn-display-four-center">
                <div class="null-block">
                  <div class="shadow-btn-display-four">                  
                  </div>
                </div>

                <div class="btn-display-four">
                  <span>
                    Хочу набор bunchems!
                  </span>
                </div>
              </div>
            </div>

            <!--  -->

          </div>

        </div>

      </div>

      <!-- end-display-four -->

      <!-- display-five -->

      <div class="display-five">

        <div class="center-all-block">
          
          <div class="left-center-all-block">

            <div class="header-display-five">
              <span class="appetite">
                Возможно, Вашему ребенку также будет интересен Живой песок
              </span>

              <span class="big">
                Этот песок похож на массу для лепки, но при этом обладает сыпучестью и мягкостью.
              </span>
            </div>

            <!-- content-five -->

            <div class="right-block-content-five">
              <div class="null-block">
                <div class="one-box-right-content-five">                  
                </div>
              </div>

              <div class="null-block">
                <div class="two-box-right-content-five">                  
                </div>
              </div>
            </div>

            <div class="left-block-content-five">

              <!--  -->

              <div class="check-text-content-five one">
                <span>
                  Подходит и для постройки башенок,  и для лепки куличиков. 
                </span>
              </div>

              <div class="check-text-content-five two">
                <span>
                  Игры с этим видом песка очень полезны для развития мелкой моторики и воображения Вашего ребенка.
                </span>
              </div>

              <div class="check-text-content-five two">
                <span>
                  Живой песок нетоксичен и безопасен, не прилипает к коже или одежде и легко смывается водой.
                </span>
              </div>

              <!--  -->

              <div class="block-woman">
                <span>
                  Понравится даже Вам!
                </span>
              </div>

              <!--  -->

              <div class="block-btn-content-five">
                <div class="null-block">
                  <div class="shadow-btn-display-five">                      
                  </div>
                </div>

                <div class="btn-content-five">
                  <span>
                    Посмотреть больше наборов Живого песка
                  </span>
                </div>
              </div>

              <!--  -->

            </div>

            <!-- end-content-five -->            

          </div>

        </div>
        
      </div>

      <!-- end-display-five -->

      <!-- display-six -->

      <div class="display-six">

        <div class="center-all-block">
          
          <div class="left-center-all-block">            

            <div class="arond-display-six">

              <div class="null-block">
                <div class="null-block">
                  <div class="icon-car">                    
                  </div>
                </div>

                <div class="null-block">
                  <div class="icon-caterpillar">                    
                  </div>
                </div>

                <div class="null-block">
                  <div class="icon-godzi">                    
                  </div>
                </div>

                <div class="null-block">
                  <div class="icon-tower">                    
                  </div>
                </div>

                <div class="null-block">
                  <div class="icon-wolf">                    
                  </div>
                </div>

                <div class="null-block">
                  <div class="icon-giraffe">                    
                  </div>
                </div>

                <div class="null-block">
                  <div class="icon-fish">                    
                  </div>
                </div>

                <div class="null-block">
                  <div class="icon-horse">                    
                  </div>
                </div>
              </div>

              <span class="over-text-display-six">
                Из шариков-липучек конструктора<br>
                Bunchems можно сложить массу объемных<br>
                фигурок: обезьянки, собачки, машинки,<br>
                башни, роботы.
              </span>

              <span class="under-text-display-six">
                В этом году, в США, мальчик 6-ти лет<br>
                поставил рекорд Гиннесса и собрал<br>
                <span>10 тысяч разных фигурок</span> из этих<br>
                "волшебных" шариков.
              </span>
            </div>

          </div>

        </div>
              
      </div>

      <!-- end-display-six -->

      <!-- display-seven -->

      <div class="display-seven">

        <div class="center-all-block">
          
          <div class="left-center-all-block">

            <div class="header-display-seven">
              <span class="appetite">
                Посмотрите видео и узнайте, 
              </span>

              <span class="sans">
                как сложить самые классные фигуры из Bunchems!
              </span>
            </div>

            <div class="block-video-seven">
              <div class="video-seven">
                <div id="jp_container_1" class="jp-video jp-video-360p" role="application" aria-label="media player">
  <div class="jp-type-single">
    <div id="jquery_jplayer_1" class="jp-jplayer"></div>
    <div class="jp-gui">
      <div class="jp-video-play">
        <button class="jp-video-play-icon" role="button" tabindex="0">play</button>
      </div>
      <div class="jp-interface">

      
        <div class="jp-progress">
          <div class="jp-seek-bar">
            <div class="jp-play-bar"></div>
          </div>
        </div>
        <!--  -->
        <div class="all-progress">
        <!--  -->
      
        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
        <div class="jp-details">
        </div>
        <div class="jp-controls-holder">
          <div class="jp-volume-controls">
            <button class="jp-mute" role="button" tabindex="0">mute</button>
            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
            <div class="jp-volume-bar">
              <div class="jp-volume-bar-value"></div>
            </div>
          </div>
          <div class="jp-controls">
            <button class="jp-play" role="button" tabindex="0">play</button>
            <button class="jp-stop" role="button" tabindex="0">stop</button>
          </div>
          <div class="jp-toggles">
            <button class="jp-repeat" role="button" tabindex="0">repeat</button>
            <button class="jp-full-screen" role="button" tabindex="0">full screen</button>
          </div>
        </div>
        <!--  -->
        </div>
        <!--  -->
      </div>
    </div>
    <div class="jp-no-solution">
      <span>Update Required</span>
      To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
    </div>
  </div>
</div>
              </div>
            </div>

          </div>

        </div>
        
      </div>

      <!-- end-display-seven -->

      <!-- display-eight -->

      <div class="display-eight">

        <div class="center-all-block">
          
          <div class="left-center-all-block">

            <!-- slider-display-eight -->

            <div class="slider-display-eight">

              <div class="center slider">

                <!--  -->

                <div>
                  <div class="into-slider-eight">

                    <div class="block-foto-into-slider-eight">
                      <img src="img/one-image-display-eight.png" alt="not-image">
                    </div>

                    <div class="block-text-into-slider-eight">
                      <span class="comment">
                        "Купила сыну Арсению большой трехъярусный набор. Ребенок счастлив! Собирает часами. Ему 5-ть лет, но такие конструкции выдумывает - и башни, и роботов, и боевые автомобили. Заметно, 
                        как ребенок начал лучше соображать после покупки конструктора."
                      </span>

                      <span class="italic">
                        Татьяна Еременко, Москва
                      </span>
                    </div>
                  </div>              
                </div>

                <!--  -->

                <!--  -->

                <div>
                  <div class="into-slider-eight">

                    <div class="block-foto-into-slider-eight">
                      <img src="img/two-image-display-eight.png" alt="not-image">
                    </div>

                    <div class="block-text-into-slider-eight">
                      <span class="comment">
                        "Моей дочери 9 лет. Любила раньше сидеть у планшета или за компьютером. Было не оторвать! Потом крестные подарили этот конструктор. Планшет пылится на полке, компьютер никто не включал уже неделю. Дом завален фигурками, дочь требует еще 1 огромный набор на 800 штук. Будем оформлять заказ!"
                      </span>

                      <span class="italic">
                        Артем Куркчи, Краснодар
                      </span>
                    </div>
                  </div>              
                </div>

                <!--  -->

                <!--  -->

                <div>
                  <div class="into-slider-eight">

                    <div class="block-foto-into-slider-eight">
                      <img src="img/three-image-display-eight.png" alt="not-image">
                    </div>

                    <div class="block-text-into-slider-eight">
                      <span class="comment">
                        "Не хотела заказывать, потому что все эти "развивающие" игрушки в итоге - полная пустышка. Но сын уговорил. У них в садике у половины группы такой конструктор есть. Кароче беру свои слова обратно. Конструктор - бомба. Сама иногда с сыном увлекаюсь и сижу по пару часов играю. Жаль, что в нашем детстве таких игрушек не было!"
                      </span>

                      <span class="italic">
                        Евгения Тудачкова, Псков
                      </span>
                    </div>
                  </div>              
                </div>

                <!--  -->

              </div>

            </div>

            <!-- end-slider-display-eight -->            

          </div>

        </div>
        
      </div>

      <!-- end-display-eight -->

      <!-- display-nine -->

      <div class="display-nine">
        
        <div class="center-all-block">
          
          <div class="left-center-all-block"> 

            <div class="header-nine">
              <span>
                Хотите заказать конструктор <span>Bunchems</span><br>
                или задать нам вопросы? Просто напишите!
              </span>
            </div>

            <div class="block-form">
              <form name="dataGuestTwo" onSubmit="provDataGuestTwo(); return(false);" class="footer-form" action="send.php" method="post">
                <div class="bg-input">
                  <input type="text" id="user_name_footer_index" name="user_name_footer_index" placeholder="ваше имя">
                </div>

                <div class="bg-input two">
                  <input type="text" id="user_phone_footer_index" name="user_phone_footer_index" placeholder="ваш контактный телефон *">
                </div>

                <input name="form_footer_index" type="hidden" value='Формы "Заказать конструктор Bunchems или задать нам вопросы" (страница "Главная")'>

                <div class="block-btn-form">
                  <div class="null-block">
                    <div class="shadow-btn-form">                      
                    </div>
                  </div>

                  <input type="submit" name="submit" class="btn-form" value="Перезвоните мне">
                </div>
              </form>
            </div>

          </div>

        </div>

      </div>

      <!-- end-display-nine -->

      <!-- display-footer -->

      <?php
      include 'footer.php';
      ?>

      <!-- end-display-footer -->
      
    </div>

    <!-- end-all-block -->

    <!-- btn-up -->

    <div class="null-block">
      <a href="#scroll" onclick="return anchorScroller(this)">
        <div class="btn-up default">
          <span>
            наверх
          </span>       
        </div>
      </a>
    </div>


    <!-- end-btn-up -->

    <!-- popup -->

    <div class="block-popup-form">
      <div class="padding-top">
      </div>

        <div class="padding-left">        
        </div>

        <div class="block-form-popup"> 
          <div class="null-block">
            <div class="shadow-form-popup">              
            </div>
          </div>

          <form name="dataGuestOne" onSubmit="provDataGuestOne(); return(false);" class="form-popup" method="post" action="send.php">
            <div class="null-block">
              <div class="close-form-popup">                
              </div>
            </div>

            <div class="header-form-popup">
              <span>
                Заполните всего 2 поля
                и наш менеджер свяжется с Вами
                для оформления заказа
              </span>
            </div>

            <div class="bg-input-form-popup">
              <input type="text" id="user_name_call_back" name="user_name_call_back" placeholder="ваше имя">
            </div>

            <div class="bg-input-form-popup">
              <input type="text" id="user_phone_call_back" name="user_phone_call_back" placeholder="ваш контактный телефон *">
            </div>

            <input name="form_call_back" type="hidden" value='Формы обратный звонок (страница "Главная")'>

            <div class="block-btn-form-popup">
              <div class="null-block">
                <div class="shadow-btn-form popup">                      
                </div>
              </div>

              <input type="submit" name="submit" class="btn-form" value="Заказать">
            </div>

          </form>
        </div>
    </div>

    <!-- end-popup -->

    <!-- script -->

    <script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript">
        $(".regular").slick({
          dots: true,
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1
        });

        $(".center").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });
    </script>

    <script type="text/javascript">

      $(document).ready(function(){

        var $menu = $(".btn-up");

        $(window).scroll(function(){
            if ( $(this).scrollTop() > 250 && $menu.hasClass("default") ){
                $(".btn-up.default").fadeIn(500);
            } else if($(this).scrollTop() <= 250 && $menu.hasClass("default")) {
                $(".btn-up.default").fadeOut(500);
            }
        });
      });

    </script>

    <script type="text/javascript">
        $(function() {
          $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                  scrollTop: target.offset().top
                }, 1000);
                return false;
              }
            }
          });
        });
    </script>

    <script type="text/javascript">
      $(document).ready(function() {
          $('.btn-contact, .one-of-footer-menu.last').click(function () {
            $('.block-popup-form').fadeIn(300);
          });

          $('.close-form-popup').click(function () {
            $('.block-popup-form').fadeOut(300);
          });
      });
    </script>

    <script type="text/javascript">

      function provDataGuestOne() {
      
      obj_form=document.forms.dataGuestOne;
      obj_pole_user_phone_call_back=obj_form.user_phone_call_back;
      
        if (obj_pole_user_phone_call_back.value=="") {

          $('#user_phone_call_back').css({'background' : 'url(img/bg-input-form-popup-red.png) no-repeat', 'color' : 'black'});
          $('#user_phone_call_back').focus();

          return;
        }

        // если всё верно убирает красную подводку.
        else {
          $('#user_phone_call_back').css({'background' : 'url(img/bg-input-form-popup.png) no-repeat', 'color' : '#a0a0a0'});
        }

        obj_form.submit();
      }

      /////////////////////////////////////////////////////////////////////////////////////

      function provDataGuestTwo() {
      
      obj_form=document.forms.dataGuestTwo;
      obj_pole_user_phone_footer_index=obj_form.user_phone_footer_index;
      
        if (obj_pole_user_phone_footer_index.value=="") {

          $('#user_phone_footer_index').css({'background' : 'url(img/bg-input-red.png) no-repeat', 'color' : 'black', 'background-size' : '100%'});
          $('#user_phone_footer_index').focus();

          return;
        }

        // если всё верно убирает красную подводку.
        else {
          $('#user_phone_footer_index').css({'background' : 'url(img/bg-input.png) no-repeat', 'color' : '#a0a0a0'});
        }

        obj_form.submit();
      }

      /////////////////////////////////////////////////////////////////////////////////////

    </script>

    <script type="text/javascript">
    //<![CDATA[
    $(document).ready(function(){

      $("#jquery_jplayer_1").jPlayer({
        ready: function () {
          $(this).jPlayer("setMedia", {
            title: "Big Buck Bunny",
            m4v: "bu.m4v",
            poster: "img/video.png"
          });
        },
        swfPath: "../../dist/jplayer",
        supplied: "m4v",
        size: {
          width: "100%",
          height: "360px",
          cssClass: "jp-video-360p"
        },
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true,
        remainingDuration: true,
        toggleDuration: true
      });
    });
    //]]>
    </script>

    <!-- end-script -->

  </body>

</html>