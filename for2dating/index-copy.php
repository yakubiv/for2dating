<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity">
  		
  		<!-- baground-slider -->

  		<div class="null-block" style="display:none;">

  			<!-- slider-display-bg -->

	        <div class="slider-display-one">
	            <div class="regular slider">
		            <!--  -->
		            <div>
		                <div class="into-slider-one one">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one two">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one three">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one four">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one five">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one six">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one seven">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one eight">
		                </div>                  
		            </div>
		            <!--  -->
	        	</div>
	    	</div>

	    	<!-- end-slider-display-bg -->
  				
  		</div>

  		<!-- end-baground-slider -->

  		<!-- header -->

  		<div class="left-one-bg-header">
  			<div class="left-two-bg-header">
	  			<div class="center-all-block">
	  				<div class="left-center-all-block">

	  					<!--  -->

	  					<div class="over-header">

			  				<div class="block-logo-header">
			  					<a href="#">
			  						<div class="logo-header">		  							
			  						</div>
			  					</a>
			  				</div>

			  				<!--  -->

			  				<div class="block-soc-header">
			  					<span>
			  						Мы в
			  					</span>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-vk">		  								
			  							</div>
			  						</a>
			  					</div>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-face">		  								
			  							</div>
			  						</a>
			  					</div>
			  				</div>

			  				<!--  -->

			  				<div class="block-lang">
			  					<div class="one-of-lang">
			  						<div class="flag-one-of-lang">			  							
			  						</div>

			  						<span>
			  							Русский
			  						</span>

			  						<div class="arrow-lang">
			  							
			  						</div>
			  					</div>
			  				</div>

			  				<!--  -->

			  				<div class="block-reg-enter">
			  					<div class="btn-enter">
			  						<span>
			  							Войти
			  						</span>
			  					</div>

			  					<div class="btn-reg">
			  						<span>
			  							Регистрация
			  						</span>
			  					</div>
			  				</div>

			  				<!--  -->

		  				</div>

		  				<!--  -->

		  				<!--  -->

		  				<div class="under-header">

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span>
		  							Мой кабинет
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span>
		  							Уведомления
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span>
		  							Блог
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span>
		  							Карусель
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->
		  					
		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span>
		  							ТОП 100
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span>
		  							Поиск
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span>
		  							Идеальная пара
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  				</div>

		  				<!--  -->
	  					
	  				</div>
	  			</div>
	  		</div>
  		</div>

  		<!-- end-header -->



  		<!-- one-display -->

  		<div class="block-one-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="block-login-registration">
	  					
	  					<!--  -->

	  					<div class="block-btn-login-registration">
	  						<div class="all-block-btn-login">
	  							<div class="rotate-block-btn-login">
	  								<div class="btn-login">
	  									<span>
	  										Вход
	  									</span>
	  								</div>
	  							</div>
	  						</div>

	  						<!--  -->

	  						<div class="all-block-btn-registration">
	  							<div class="rotate-block-btn-registration">
	  								<div class="btn-registration">
	  									<span>
	  										Регистрация
	  									</span>
	  								</div>
	  							</div>
	  						</div>
	  					</div>

	  					<!-- login -->
	  					
	  					<div class="block-content-login">
	  						<div class="null-block">
		  						<div class="logo-form-login">	  							
		  						</div>
		  					</div>

	  						<form>

	  							<!--  -->

		  						<span class="info-input">
		  							Email адрес
		  						</span>

		  						<!--  -->

		  						<div class="bg-input-login-registration">
		  							<input class="input-login-registration" placeholder="Адрес электронной почты">
		  						</div>

		  						<!--  -->

		  						<span class="info-input">
		  							Пароль
		  						</span>

		  						<!--  -->

		  						<div class="bg-input-login-registration password">
		  							<input class="input-login-registration" placeholder="******">
		  						</div>

		  						<!--  -->

		  						<a href="#">
			  						<span class="forget-password">
			  							Забыли пароль?
			  						</span>
			  					</a>

		  						<div class="block-check-forget-password">
		  							<input id="check-forget-password" type="checkbox" name="check-forget-password">
                      				<label class="check-forget-password" for="check-forget-password"></label>

                      				<span class="forget-password">
                      					Запомнить меня
                      				</span>
		  						</div>

		  						<!--  -->

		  						<div class="block-enter-from-soc">
		  							<div class="btn-enter-from-soc">
		  								<span>
		  									Войти
		  								</span>
		  							</div>

		  							<div class="btn-enter-from-soc-two">
		  								<span>
		  									Присоединиться через
		  								</span>

		  								<div class="icon-face-btn">		  								
			  							</div>
		  							</div>
		  						</div>

	  						</form>
	  					</div>

	  					<!-- end-login -->

	  					<!-- registretion -->
	  					
	  					<div class="block-content-registretion">
	  						<form>

	  							<!--  -->

	  							<div class="header-registretion">
	  								<span>
	  									Сайт для двоих
	  								</span>
	  							</div>

	  							<!--  -->

	  							<span class="registretion">
	  								Регистрация
	  							</span>

	  							<span class="or">
	  								или
	  							</span>

	  							<div class="btn-enter-from-soc-two">
		  							<span>
		  								Присоединиться через
		  							</span>

		  							<div class="icon-face-btn">		  								
			  						</div>
		  						</div>

	  							<!--  -->

	  							<div class="line-block-input-registretion first">
	  								<div class="bg-input-registretion">
	  									<input class="input-registration" placeholder="Имя">
	  								</div>

	  								<div class="icon-must">	  									
	  								</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-i-am">
	  								<span class="i-am">
	  									Я
	  								</span>

	  								<div class="block-check-woman-man">
			  							<input id="check-woman" type="checkbox" name="check-woman">
	                      				<label class="check-woman" for="check-woman"></label>

	                      				<span class="text-check-woman">
	                      					жещина
	                      				</span>
			  						</div>

			  						<div class="block-check-woman-man">
			  							<input id="check-man" type="checkbox" name="check-man">
	                      				<label class="check-man" for="check-man"></label>

	                      				<span class="text-check-woman">
	                      					мужчина
	                      				</span>
			  						</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-i-am">
	  								<span class="i-am">
	  									Ищу
	  								</span>

	  								<div class="block-check-woman-man">
			  							<input id="check-icon-woman" type="checkbox" name="check-icon-woman">
	                      				<label class="check-icon-woman" for="check-icon-woman"></label>

	                      				<span class="text-check-woman">
	                      					жещину
	                      				</span>
			  						</div>

			  						<div class="block-check-woman-man">
			  							<input id="check-icon-man" type="checkbox" name="check-icon-man">
	                      				<label class="check-icon-man" for="check-icon-man"></label>

	                      				<span class="text-check-woman">
	                      					мужчину
	                      				</span>
			  						</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-date-registration">

	  								<span class="date-registration">
	  									Дата рождения:
	  								</span>
	  								
	  								<div class="block-select-registration">
	  									<select data-placeholder="День" class="chosen-select-no-single" tabindex="10">
								            <option value=""></option>
								            <option>1</option>
								            <option>2</option>
								            <option>3</option>
								            <option>4</option>
								            <option>5</option>
								            <option>6</option>
								            <option>7</option>
								            <option>8</option>
								            <option>9</option>
								            <option>10</option>
								            <option>11</option>
								            <option>12</option>
								            <option>13</option>
								            <option>14</option>
								            <option>15</option>
								            <option>16</option>
								            <option>17</option>
								            <option>18</option>
								            <option>19</option>
								            <option>20</option>
								        </select>
	  								</div>
	  							</div>

	  							<!--  -->

	  						</form>
	  					</div>

	  					<!-- end-registretion -->

	  				</div>

	  				<!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-one-display -->



  		<!-- two-display -->

  		<div class="block-two-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="header-two-display">
	  					<span>
	  						For two dating
	  					</span>
	  				</div>

	  				<!--  -->

	  				<!--  -->

	  				<div class="left-content-two-display">
	  					<span>
	  						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
	  					</span>

	  					<span>
	  						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
	  					</span>

	  					<span>
	  						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
	  					</span>
	  				</div>

	  				<!--  -->

	  				<!--  -->

	  				<div class="right-content-two-display">
		  				<div class="block-video-player">
		  					<div class="null-block">
		  						<div class="bg-all-btn-start-play">
		  							<div class="border-btn-start-play">
		  								<div class="btn-start-play">		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="block-navigate-player">
		  							<div class="navigate-player">

		  								<!--  -->

		  								<div class="block-btn-play-stop">
		  									<div class="btn-stop">		  									
		  									</div>
		  								</div>

		  								<!--  -->

		  								<div class="block-time-progress">
		  									<div class="bg-line-time-progress">
		  										<div class="load-line-time-progress">
		  											<div class="btn-circle-time-progress">
		  												
		  											</div>
		  										</div>
		  									</div>
		  								</div>

		  								<!--  -->

		  								<div class="block-volume">
		  									<div class="btn-volume">
		  										
		  									</div>
		  								</div>

		  								<!--  -->
		  							</div>		  							
		  						</div>
		  					</div>
		  				</div>
	  				</div>

	  				<!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-two-display -->



  		<!-- three-display -->

  		<!-- baground-slider-two -->

  		<div class="block-three-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="header-three-display">
	  					<span>
	  						Отзывы наших пользователей
	  					</span>
	  				</div>

	  				<!--  -->

		  			<!-- slider-display-three -->

			        <div class="slider-display-three">

			            <div class="regular-two slider">
				            
				            <!--  -->

			                <div>
			                  	<div class="into-slider-two">

				                    <div class="block-photo-user">
				                    	<div class="border-photo-user">
				                    		<div class="bg-photo-user">
				                    			<div class="around-photo-user">
				                    				<img src="img/one-photo-user.png" alt="not-image">
				                    			</div>
				                    		</div>
				                    	</div>			                      
				                    </div>

				                    <div class="block-text-into-slider-two">

				                    	<div class="header-text-into-slider-two">
				                    		<span class="name-user">
				                    			Генри и Ольга
				                    		</span>

				                    		<div class="block-number-comments-slide-two">
				                    			<a href="#">
					                    			<span class="text-number-comments">
					                    				Все отзывы
					                    			</span>
				                    			</a>
				                    			<span class="number-comments">
				                    				248
				                    			</span>
				                    		</div>
				                    	</div>

				                      	<span class="comment">
				                      		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				                      	</span>

				                      	<a href="#">
					                      	<span class="read-more">
					                      		Читать далее>>
					                      	</span>
				                      	</a>
				                    </div>

			                  	</div>              
			                </div>

			                <!--  -->

			                <!--  -->

			                <div>
			                  	<div class="into-slider-two">

				                    <div class="block-photo-user">
				                    	<div class="border-photo-user">
				                    		<div class="bg-photo-user">
				                    			<div class="around-photo-user">
				                    				<img src="img/one-photo-user.png" alt="not-image">
				                    			</div>
				                    		</div>
				                    	</div>			                      
				                    </div>

				                    <div class="block-text-into-slider-two">

				                    	<div class="header-text-into-slider-two">
				                    		<span class="name-user">
				                    			Генри и Ольга
				                    		</span>

				                    		<div class="block-number-comments-slide-two">
				                    			<a href="#">
					                    			<span class="text-number-comments">
					                    				Все отзывы
					                    			</span>
				                    			</a>
				                    			<span class="number-comments">
				                    				248
				                    			</span>
				                    		</div>
				                    	</div>

				                      	<span class="comment">
				                      		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				                      	</span>

				                      	<a href="#">
					                      	<span class="read-more">
					                      		Читать далее>>
					                      	</span>
				                      	</a>
				                    </div>

			                  	</div>              
			                </div>

			                <!--  -->

			                <!--  -->

			                <div>
			                  	<div class="into-slider-two">

				                    <div class="block-photo-user">
				                    	<div class="border-photo-user">
				                    		<div class="bg-photo-user">
				                    			<div class="around-photo-user">
				                    				<img src="img/one-photo-user.png" alt="not-image">
				                    			</div>
				                    		</div>
				                    	</div>			                      
				                    </div>

				                    <div class="block-text-into-slider-two">

				                    	<div class="header-text-into-slider-two">
				                    		<span class="name-user">
				                    			Генри и Ольга
				                    		</span>

				                    		<div class="block-number-comments-slide-two">
				                    			<a href="#">
					                    			<span class="text-number-comments">
					                    				Все отзывы
					                    			</span>
				                    			</a>
				                    			<span class="number-comments">
				                    				248
				                    			</span>
				                    		</div>
				                    	</div>

				                      	<span class="comment">
				                      		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				                      	</span>

				                      	<a href="#">
					                      	<span class="read-more">
					                      		Читать далее>>
					                      	</span>
				                      	</a>
				                    </div>

			                  	</div>              
			                </div>

			                <!--  -->

			        	</div>
			    	</div>

			    	<!-- end-slider-display-three -->

			    </div>
			</div>
		</div>

  		<!-- end-three-display -->

  		<!-- four-display -->

  		<div class="block-four-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="header-four-display">
	  					<span>
	  						Последние новости с блога
	  					</span>
	  				</div>

	  				<!--  -->

	  				<!--  -->

	  				<div class="left-block four-display">
	  					<div class="right-block">
				            <a href="#">
					           	<span class="all-article">
					           		Все статьи
					           	</span>
				            </a>
				        </div>
	  				</div>

	  				<!--  -->

	  				<!--  -->
			        
			        <div class="block-one-of-article">

				        <div class="block-photo-user">
				           	<div class="border-photo-user pink">
				           		<div class="bg-photo-user">
				           			<div class="around-photo-user">
				           				<img src="img/two-photo-user.png" alt="not-image">
				           			</div>
				           		</div>
				           	</div>			                      
				        </div>

				        <div class="block-text-one-of-article">

				           	<span class="name-user pink">
				           		Оля
				           	</span>

				           	<span class="question-user">
				           		Вопрос:
				           	</span>

				            <span class="comment">
				            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
				            </span>

				            <span class="question-user">
				           		Отвечает психолог Краснова А.
				           	</span>

				            <span class="comment last">
				            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				            </span>

				            <a href="#">
					          	<span class="read-more pink">
					          		Читать далее>>
					          	</span>
				          	</a>
				        </div>

			        </div>

			        <!--  -->

			        <div class="separator-betwen-article">
			        	
			        </div>

			        <!--  -->

			        <div class="block-one-of-article">

				        <div class="block-photo-user">
				           	<div class="border-photo-user blue">
				           		<div class="bg-photo-user">
				           			<div class="around-photo-user">
				           				<img src="img/three-photo-user.png" alt="not-image">
				           			</div>
				           		</div>
				           	</div>			                      
				        </div>

				        <div class="block-text-one-of-article">

				           	<span class="name-user blue">
				           		Gjerde
				           	</span>

				           	<span class="question-user">
				           		Вопрос:
				           	</span>

				            <span class="comment">
				            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
				            </span>

				            <span class="question-user">
				           		Отвечает психолог Краснова А.
				           	</span>

				            <span class="comment last">
				            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				            </span>

				            <a href="#">
					          	<span class="read-more blue">
					          		Читать далее>>
					          	</span>
				          	</a>
				        </div>

			        </div>

			        <!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-four-display -->

  		<!-- footer-display -->

  		<div class="footer-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

		  			<div class="footer-menu">

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					О нас
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Условия и правила
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Конфиденциальность
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Партнерам
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Отзывы
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Связаться с Нами
		  				</span>

		  				<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="block-soc-footer">

		  				<!--  -->

		  				<span class="header-soc-footer">
		  					Присоединяйся к <span>For two dating:</span>
		  				</span>

		  				<!--  -->

		  				<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-vk">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-face">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-twit">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-inst">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-yt">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-gp">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="footer-all-right">
		  				<span class="header-soc-footer">
		  					© 2016- 2016 For2date. Все права защищены. Разработка I-PR
		  				</span>
		  			</div>

		  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-footer-display -->

  	</div>

  	<!-- script -->

  	<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

  	<script type="text/javascript">
        $(".regular").slick({
          	dots: false,
          	autoplay: true,
           	prevArrow: false,
            nextArrow: false,
            centerMode: true,
          	infinite: true,
          	slidesToShow: 1,
          	slidesToScroll: 1,
          	autoplaySpeed: 10000
        });

        $(".regular-two").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });
    </script>

    <!-- =============================================================== -->

    <script src="js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
	    var config = {
	      '.chosen-select'           : {},
	      '.chosen-select-deselect'  : {allow_single_deselect:true},
	      '.chosen-select-no-single' : {disable_search_threshold:1},
	      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	      '.chosen-select-width'     : {width:"95%"}
	    }
	    for (var selector in config) {
	      $(selector).chosen(config[selector]);
	    }
	</script>

	<!-- =============================================================== -->

	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>

	<script type="text/javascript" src="js/jquery.jscrollpane.js"></script>

	<script type="text/javascript">
			
			$('.block-select-registration, .active-result, .chosen-single').click(function()
			{
				$('.scroll-pane').jScrollPane({showArrows: true});
			});
	</script>



	<style type="text/css">
			/* Styles specific to this particular page */
			.scroll-pane
			{
				width: 100%;
				height: 240px;
				overflow: scroll;
			}
		</style>

	<!-- =============================================================== -->

  	<!-- end-script -->

  </body>
</html>