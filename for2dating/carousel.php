<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity perfect-pair-step-first">
  		
  		<!-- baground-slider -->

  		<div class="null-block">

  			<!-- slider-display-bg -->

	        <div class="slider-display-one" style="display:none;">
	            <div class="regular-first slider">
		            <!--  -->
		            <div>
		                <div class="into-slider-one one">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one two">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one three">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one four">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one five">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one six">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one seven">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one eight">
		                </div>                  
		            </div>
		            <!--  -->
	        	</div>
	    	</div>

	    	<!-- end-slider-display-bg -->
  				
  		</div>

  		<!-- end-baground-slider -->

  		<!-- header -->

  		<div class="left-one-bg-header">
  			<div class="left-two-bg-header">
	  			<div class="center-all-block">
	  				<div class="left-center-all-block">

	  					<!--  -->

	  					<div class="over-header">

			  				<div class="block-logo-header">
			  					<a href="#">
			  						<div class="logo-header">		  							
			  						</div>
			  					</a>
			  				</div>

			  				<!--  -->

			  				<div class="block-soc-header">
			  					<span>
			  						Мы в
			  					</span>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-vk">		  								
			  							</div>
			  						</a>
			  					</div>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-face">		  								
			  							</div>
			  						</a>
			  					</div>
			  				</div>

			  				<!-- lang-select -->
	  								
	  						<div class="block-select-registration-lang">
	  							<div class="select-registration-lang">
	  								<div id="total-lang">
	  									<div class="flag-one-of-lang">			  							
						  				</div>

						  				<span>
						  					Русский
						  				</span>
	  								</div>

	  								<div class="btn-select-lang">	  											
	  								</div>
	  							</div>

	  							<div class="all-block-option-lang">
									<div class="scroll-pane-lang">
										<div class="content-select-lang">
										    <div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Русский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Английский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Норвежский
								  				</span>
										   	</div>
									    </div>
									</div>
								</div>
	  						</div>
	  								
	  						<!-- end-lang-select -->

			  				<!-- account -->

			  				<div class="block-account">
			  					<div class="block-coints">
			  						<span class="coints">
			  							0
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-user-header">
			  						<div class="border-photo-user-header">
			  							<img src="img/photo-user-header.png" alt="not-image">
			  						</div>

			  						<span>
			  							Александр
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-setting">
			  						<div class="icon-setting">			  							
			  						</div>
			  					</div>

			  					<!--  -->

			  					<div class="block-icon-enter-header">
			  						<div class="icon-enter-header">			  							
			  						</div>
			  					</div>

			  					<!--  -->

			  				</div>

			  				<!-- end-account -->

		  				</div>

		  				<!--  -->

		  				<!-- mobile-menu -->

		  				<div class="null-block">
		  					<div class="null-block">
		  						<div class="btn-mobile-open">
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="btn-mobile-close">
		  						</div>
		  					</div>
		  				</div>

		  				<!-- end-mobile-menu -->

		  				<!--  -->

		  				<div class="under-header">

		  					<!--  -->

		  					<div class="one-of-menu office">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Мой кабинет
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu office">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои альбомы
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Моя статистика
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой пакет услуг
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой счет
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои статьи
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои подарки
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Пригласи друга
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои настройки
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu message">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Уведомления
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="block-number-message">
		  								<span>
		  									648
		  								</span>
		  							</div>
		  						</div>
		  						<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu message">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои сообщения <span>(9)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подмигиваний <span>(315)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Просмотры <span>(49)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Избранные <span>(5)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подарки <span>(3)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Симпатии <span>(5)</span>
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">
		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Блог
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Карусель
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->
		  					
		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							ТОП 100
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Поиск
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Идеальная пара
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  				</div>

		  				<!--  -->
	  					
	  				</div>
	  			</div>
	  		</div>
  		</div>

  		<!-- end-header -->

  		<!-- slider-under-header -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  			<!--  -->

	  			<div class="block-slider-under-header">
	  				<div class="all-slider-under-header">

	  					<div class="block-trap-photo">
	  						<span class="bold">
	  							Ловушка:
	  						</span>

	  						<span class="regular">
	  							Попасть<br>
	  							в ленту
	  						</span>
	  					</div>

	  					<!--  -->

				        <div class="slider-display-under-header">
				            <div class="regular-under-header slider-under-header">
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/one-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/two-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/three-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/four-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/five-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
				        	</div>
				    	</div>

				    	<!--  -->

	  				</div>
	  			</div>

	  			<!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-slider-under-header -->

  		<!-- content -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<div class="content-gift-page">

	  					<!-- left-sidedar -->

	  					<div class="left-sidedar">
	  						
	  						<!--  -->

	  						<span class="header-left-sidebar">
	  							Лидер
	  						</span>

	  						<!--  -->

	  						<div class="block-one-of-banner">
	  							<div class="null-block">
	  								<div class="block-message-banner">
	  									<span>
	  										Сообщение
	  									</span>
	  								</div>	
	  							</div>

	  							<img src="img/one-image-banner.png" alt="not-image">	  							
	  						</div>

	  						<!--  -->

	  						<div class="block-coints-banner">
	  							<span>
	  								Заплати одну монету и попади в лидеры
	  							</span>
	  						</div>

	  						<!--  -->

	  						<div class="block-one-of-banner pink">
	  							<img src="img/two-image-banner.png" alt="not-image">	  							
	  						</div>

	  						<!--  -->

	  					</div>

	  					<!-- end-left-sidedar -->

	  					<!-- center-content -->

	  					<!-- header-center-content -->

	  						<span class="header-center-content">
	  							Карусель
	  						</span>

	  						<div class="text-under-header-center-content">
		  						<span>
		  							Голосуйте за понравившихся вам девушек и ваша фотография будет предложена им для голосования.
								</span>

	  							<span>
									Если ваши симпатии совпадут - вы получите сообщение об этом и сможете продолжить знакомство уже в переписке.
								</span>

								<span>
									Больше симпатий - больше шансов познакомиться!
	  							</span>
	  						</div>

	  					<!-- end-header-center-content -->

	  					<div class="center-content-carousel">

	  						<!-- block-carousel -->

	  						<div class="block-carousel">
	  							<div class="null-block">
	  								<div class="bg-carousel-up">	  									
	  								</div>
	  							</div>

	  							<div class="all-carousel">
	  								<div class="center-carousel">
	  									<!--  -->

								        <div class="slider-display-carousel">
								            <div class="regular-carousel slider-carousel">

									            <!--  -->

									            <div>
									                <div class="into-slider-carousel">

									                	<div class="one-of-like-user">
										                	<div class="left-block">
										                		<div class="null-block">
										                			<div class="block-hover-like-user">
										                				<div class="around-gift">									                					
										                				</div>

										                				<div class="block-icon-hover-like-user">
										                					<div class="icon-send-message">									                						
										                					</div>

										                					<div class="icon-like-user">									                						
										                					</div>

										                					<div class="icon-add-favorite">									                						
										                					</div>

										                					<div class="icon-off-line">									                						
										                					</div>
										                				</div>
										                			</div>
										                		</div>

										                		<div class="block-photo-one-of-like-user">
										                			<img src="img/one-image-like-user.png" alt="not-image">
										                		</div>

										                		<span class="bold">
										                			Инна, 26 лет
										                		</span>

										                		<span class="regular">
										                			Украина, г.Днепр
										                		</span>
									                		</div>

									                		<div class="btn-like-user">
									                			<span>
									                				Нравится
									                			</span>
									                		</div>									                		
									                	</div>

									                	<!--  -->

									                	<div class="one-of-like-user right">
										                	<div class="left-block">
										                		<div class="null-block">
										                			<div class="block-hover-like-user">
										                				<div class="around-gift">									                					
										                				</div>

										                				<div class="block-icon-hover-like-user">
										                					<div class="icon-send-message">									                						
										                					</div>

										                					<div class="icon-like-user">									                						
										                					</div>

										                					<div class="icon-add-favorite">									                						
										                					</div>

										                					<div class="icon-on-line">									                						
										                					</div>
										                				</div>
										                			</div>
										                		</div>

										                		<div class="block-photo-one-of-like-user">
										                			<img src="img/two-image-like-user.png" alt="not-image">
										                		</div>

										                		<span class="bold">
										                			Викория, 25 лет
										                		</span>

										                		<span class="regular">
										                			Украина, г.Днепр
										                		</span>
									                		</div>

									                		<div class="btn-like-user">
									                			<span>
									                				Нравится
									                			</span>
									                		</div>									                		
									                	</div>
									                	
									                </div>                
									            </div>

									            <!--  -->

									            <!--  -->

									            <div>
									                <div class="into-slider-carousel">

									                	<div class="one-of-like-user">
										                	<div class="left-block">
										                		<div class="null-block">
										                			<div class="block-hover-like-user">
										                				<div class="around-gift">									                					
										                				</div>

										                				<div class="block-icon-hover-like-user">
										                					<div class="icon-send-message">									                						
										                					</div>

										                					<div class="icon-like-user">									                						
										                					</div>

										                					<div class="icon-add-favorite">									                						
										                					</div>

										                					<div class="icon-on-line">									                						
										                					</div>
										                				</div>
										                			</div>
										                		</div>

										                		<div class="block-photo-one-of-like-user">
										                			<img src="img/two-image-like-user.png" alt="not-image">
										                		</div>

										                		<span class="bold">
										                			Инна, 26 лет
										                		</span>

										                		<span class="regular">
										                			Украина, г.Днепр
										                		</span>
									                		</div>

									                		<div class="btn-like-user">
									                			<span>
									                				Нравится
									                			</span>
									                		</div>									                		
									                	</div>

									                	<!--  -->

									                	<div class="one-of-like-user right">
										                	<div class="left-block">
										                		<div class="null-block">
										                			<div class="block-hover-like-user">
										                				<div class="around-gift">									                					
										                				</div>

										                				<div class="block-icon-hover-like-user">
										                					<div class="icon-send-message">									                						
										                					</div>

										                					<div class="icon-like-user">									                						
										                					</div>

										                					<div class="icon-add-favorite">									                						
										                					</div>

										                					<div class="icon-off-line">									                						
										                					</div>
										                				</div>
										                			</div>
										                		</div>

										                		<div class="block-photo-one-of-like-user">
										                			<img src="img/one-image-like-user.png" alt="not-image">
										                		</div>

										                		<span class="bold">
										                			Викория, 25 лет
										                		</span>

										                		<span class="regular">
										                			Украина, г.Днепр
										                		</span>
									                		</div>

									                		<div class="btn-like-user">
									                			<span>
									                				Нравится
									                			</span>
									                		</div>									                		
									                	</div>
									                	
									                </div>                
									            </div>

									            <!--  -->

								        	</div>
								    	</div>

								    	<!--  -->
	  								</div>
	  							</div>

	  							<div class="null-block">
	  								<div class="bg-carousel-down">	  									
	  								</div>
	  							</div>
	  						</div>

	  						<!-- end-block-carousel -->

	  						<!-- block-chose -->

	  						<div class="block-chose">
	  							<div class="center-block-chose">
	  								<span class="header-center-chose">
	  									Мало показов?
	  								</span>

	  								<span class="regular">
	  									Покупай больше показов - <span>получай больше симпатий!</span>
	  								</span>

	  								<div class="one-of-chose">
	  									<span class="bold">
	  										+50
	  									</span>

	  									<span class="pink">
	  										показов
	  									</span>

	  									<div class="left-block">
		  									<div class="btn-chose">
		  										<span>
		  											Купить
		  										</span>
		  									</div>
		  								</div>

		  								<span class="coints-chose">
		  									5 монет
		  								</span>
	  								</div>

	  								<!--  -->

	  								<div class="one-of-chose center">
	  									<span class="bold">
	  										+200
	  									</span>

	  									<span class="pink">
	  										показов
	  									</span>

	  									<div class="left-block">
		  									<div class="btn-chose">
		  										<div class="null-block">
		  											<div class="bg-akciy">
		  												<span>
		  													-5%
		  												</span>
		  											</div>
		  										</div>

		  										<span>
		  											купить
		  										</span>
		  									</div>
		  								</div>

		  								<span class="coints-chose">
		  									19 монет
		  								</span>
	  								</div>

	  								<!--  -->

	  								<div class="one-of-chose">
	  									<span class="bold">
	  										+500
	  									</span>

	  									<span class="pink">
	  										показов
	  									</span>

	  									<div class="left-block">
		  									<div class="btn-chose">
		  										<div class="null-block">
		  											<div class="bg-akciy">
		  												<span>
		  													-10%
		  												</span>
		  											</div>
		  										</div>

		  										<span>
		  											купить
		  										</span>
		  									</div>
		  								</div>

		  								<span class="coints-chose">
		  									450 монет
		  								</span>
	  								</div>

	  								<!--  -->

	  							</div>

	  							<div class="null-block">
	  								<div class="bg-chose-down">	  									
	  								</div>
	  							</div>
	  						</div>

	  						<!-- end-block-chose -->

	  						<!-- block-sympathy -->

	  						<div class="block-sympathy">
	  							<span class="header-sympathy">
	  								Симпатии
	  							</span>

	  							<div class="line-block-sympathy-user">

	  								<!--  -->

	  								<div class="one-of-sympathy-user first">
										<div class="null-block">
											<div class="block-hover-sympathy-user">
												<div class="around-gift-sympathy">									                					
												</div>

				                				<div class="bg-hover-sympathy-user">
				                					<div class="icon-send-message-sympathy">				                						
				                					</div>

						     						<div class="icon-like-user-sympathy">									                						
										            </div>

										            <div class="icon-add-favorite-sympathy">									                						
										            </div>

										            <div class="icon-off-line-sympathy">									                						
										            </div>
										        </div>
										    </div>
										</div>

					             		<div class="block-photo-one-of-sympathy-user">
										    <img src="img/one-image-sympathy-user.png" alt="not-image">
					            		</div>

				                		<span class="bold">
				                			Инна, 26 лет
				                		</span>

										<span class="regular">
											Украина, г.Днепр
										</span>
									</div>

									<!--  -->

									<!--  -->

	  								<div class="one-of-sympathy-user">
										<div class="null-block">
											<div class="block-hover-sympathy-user">
												<div class="around-gift-sympathy">									                					
												</div>

				                				<div class="bg-hover-sympathy-user">
				                					<div class="icon-send-message-sympathy">				                						
				                					</div>

						     						<div class="icon-like-user-sympathy">									                						
										            </div>

										            <div class="icon-add-favorite-sympathy">									                						
										            </div>

										            <div class="icon-on-line-sympathy">									                						
										            </div>
										        </div>
										    </div>
										</div>

					             		<div class="block-photo-one-of-sympathy-user">
										    <img src="img/two-image-sympathy-user.png" alt="not-image">
					            		</div>

				                		<span class="bold">
				                			Викория, 25 лет
				                		</span>

										<span class="regular">
											Украина, г.Днепр
										</span>
									</div>

									<!--  -->

									<!--  -->

	  								<div class="one-of-sympathy-user">
										<div class="null-block">
											<div class="block-hover-sympathy-user">
												<div class="around-gift-sympathy">									                					
												</div>

				                				<div class="bg-hover-sympathy-user">
				                					<div class="icon-send-message-sympathy">				                						
				                					</div>

						     						<div class="icon-like-user-sympathy">									                						
										            </div>

										            <div class="icon-add-favorite-sympathy">									                						
										            </div>

										            <div class="icon-off-line-sympathy">									                						
										            </div>
										        </div>
										    </div>
										</div>

					             		<div class="block-photo-one-of-sympathy-user">
										    <img src="img/three-image-sympathy-user.png" alt="not-image">
					            		</div>

				                		<span class="bold">
				                			Ангелина, 23 года
				                		</span>

										<span class="regular">
											Украина, г.Днепр
										</span>
									</div>

									<!--  -->

									<!--  -->

	  								<div class="one-of-sympathy-user">
										<div class="null-block">
											<div class="block-hover-sympathy-user">
												<div class="around-gift-sympathy">									                					
												</div>

				                				<div class="bg-hover-sympathy-user">
				                					<div class="icon-send-message-sympathy">				                						
				                					</div>

						     						<div class="icon-like-user-sympathy">									                						
										            </div>

										            <div class="icon-add-favorite-sympathy">									                						
										            </div>

										            <div class="icon-on-line-sympathy">									                						
										            </div>
										        </div>
										    </div>
										</div>

					             		<div class="block-photo-one-of-sympathy-user">
										    <img src="img/four-image-sympathy-user.png" alt="not-image">
					            		</div>

				                		<span class="bold">
				                			Татьяна, 25 лет
				                		</span>

										<span class="regular">
											Украина, г.Днепр
										</span>
									</div>

									<!--  -->

									<!--  -->

	  								<div class="one-of-sympathy-user">
										<div class="null-block">
											<div class="block-hover-sympathy-user">
												<div class="around-gift-sympathy">									                					
												</div>

				                				<div class="bg-hover-sympathy-user">
				                					<div class="icon-send-message-sympathy">				                						
				                					</div>

						     						<div class="icon-like-user-sympathy">									                						
										            </div>

										            <div class="icon-add-favorite-sympathy">									                						
										            </div>

										            <div class="icon-off-line-sympathy">									                						
										            </div>
										        </div>
										    </div>
										</div>

					             		<div class="block-photo-one-of-sympathy-user">
										    <img src="img/five-image-sympathy-user.png" alt="not-image">
					            		</div>

				                		<span class="bold">
				                			Викория, 25 лет
				                		</span>

										<span class="regular">
											Украина, г.Днепр
										</span>
									</div>

									<!--  -->
	  							</div>
	  						</div>

	  						<!-- end-block-sympathy -->

	  						<!-- look-more -->

	  						<span class="look-more">
	  							Смотреть все
	  						</span>

	  						<!-- end-look-more -->

	  					</div>

	  					<!-- end-center-content -->
	  					
	  				</div>

	  			</div>
	  		</div>
  		</div>

  		<!-- end-content -->

  		<!-- footer-display -->

  		<div class="footer-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

		  			<div class="footer-menu">

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					О нас
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Условия и правила
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Конфиденциальность
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Партнерам
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Отзывы
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Связаться с Нами
		  				</span>

		  				<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="block-soc-footer">

		  				<!--  -->

		  				<span class="header-soc-footer">
		  					Присоединяйся к <span>For two dating:</span>
		  				</span>

		  				<!--  -->

		  				<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-vk">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-face">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-twit">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-inst">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-yt">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-gp">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="footer-all-right">
		  				<span class="header-soc-footer">
		  					© 2016- 2016 For2date. Все права защищены. Разработка I-PR
		  				</span>
		  			</div>

		  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-footer-display -->

  	</div>

  	<!-- script -->

  	<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

  	<script type="text/javascript">
        $(".regular-first").slick({
          	dots: false,
          	autoplay: true,
           	prevArrow: false,
            nextArrow: false,
            centerMode: true,
          	infinite: true,
          	slidesToShow: 1,
          	slidesToScroll: 1,
          	autoplaySpeed: 10000
        });

        $(".regular-two").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });

        $(".regular-gift-for-user").slick({
          infinite: true,
          // centerMode: true,
          slidesToShow: 3,
          slidesToScroll: 3
        });

        $(".regular-carousel").slick({
          	dots: false,
          	// autoplay: true,
            // centerMode: true,
          	// autoplaySpeed: 5000,
          	infinite: true,
          	variableWidth: true,
          	slidesToShow: 1,
          	slidesToScroll: 1
        });
        
    </script>

    <!-- =============================================================== -->

    <script src="js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
	    var config = {
	      '.chosen-select'           : {},
	      '.chosen-select-deselect'  : {allow_single_deselect:true},
	      '.chosen-select-no-single' : {disable_search_threshold:1},
	      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	      '.chosen-select-width'     : {width:"95%"}
	    }
	    for (var selector in config) {
	      $(selector).chosen(config[selector]);
	    }
	</script>

	<!-- =============================================================== -->

	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>

	<script type="text/javascript" src="js/jquery.jscrollpane.js"></script>

	<script type="text/javascript">

		$('.one-of-menu.message').click(function () {
            $('.all-block-dropdown-menu.message').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.message').length ) 
            	return;
          	$('.all-block-dropdown-menu.message').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.message').mouseleave (function() {
          $('.all-block-dropdown-menu.message').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

        $('.one-of-menu.office').click(function () {
            $('.all-block-dropdown-menu.office').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.office').length ) 
            	return;
          	$('.all-block-dropdown-menu.office').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.office').mouseleave (function() {
          $('.all-block-dropdown-menu.office').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

		$('.btn-login').click(function () {
            $('.block-content-registretion').fadeOut(0);
            $('.block-content-login').fadeIn(0);
            $('.btn-login').addClass('active');
            $('.btn-registration').removeClass('active');
        });

        $('.btn-registration').click(function () {
            $('.block-content-login').fadeOut(0);
            $('.block-content-registretion').fadeIn(0);
            $('.btn-registration').addClass('active');
            $('.btn-login').removeClass('active');
        });

        // //////////////////////////////////////////////////////////

        $('.into-gift-center-content').find(".one-of-tab-content").click(function () {
		  	$('.one-of-tab-content').removeClass('active');
	        $(this).addClass('active');
		});

		$('.one-of-tab-content.all-gift').click(function () {
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.all-gift').fadeIn(200);
        });

		$('.one-of-tab-content.new-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeIn(200);
            $(".regular-gift-for-user-two").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        $('.one-of-tab-content.my-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeIn(200);
            $(".regular-gift-for-user-three").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        // //////////////////////////////////////////////////////////

		$('.select-registration-one').click(function () {
            $('.all-block-option').fadeIn(200);
            $('.scroll-pane').jScrollPane({showArrows: true});
            $('.btn-select').addClass('active');
        });

		$('.points-count.focus').click(function () {
            $('.all-block-option').fadeOut(200);
            $('.btn-select').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-one').length ) 
            	return;
          	$('.btn-select').removeClass('active');
          	$('.all-block-option').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-one').find(".points-count").click(function () {
		  	$('.points-count').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count.focus').click(function () {
          	var total = document.querySelector('.content-select-one .points-count.focus').innerHTML;
          	document.getElementById('total').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-two').click(function () {
            $('.all-block-option-two').fadeIn(200);
            $('.scroll-pane-two').jScrollPane({showArrows: true});
            $('.btn-select-two').addClass('active');
        });

		$('.points-count-two.focus').click(function () {
            $('.all-block-option-two').fadeOut(200);
            $('.btn-select-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-two').length ) 
            	return;
          	$('.btn-select-two').removeClass('active');
          	$('.all-block-option-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-two').find(".points-count-two").click(function () {
		  	$('.points-count-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-two.focus').click(function () {
          	var total = document.querySelector('.content-select-two .points-count-two.focus').innerHTML;
          	document.getElementById('total-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-three').click(function () {
            $('.all-block-option-three').fadeIn(200);
            $('.scroll-pane-three').jScrollPane({showArrows: true});
            $('.btn-select-three').addClass('active');
        });

		$('.points-count-three.focus').click(function () {
            $('.all-block-option-three').fadeOut(200);
            $('.btn-select-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-three').length ) 
            	return;
          	$('.btn-select-three').removeClass('active');
          	$('.all-block-option-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-three').find(".points-count-three").click(function () {
		  	$('.points-count-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-three.focus').click(function () {
          	var total = document.querySelector('.content-select-three .points-count-three.focus').innerHTML;
          	document.getElementById('total-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-lang').click(function () {
            $('.all-block-option-lang').fadeIn(200);
            // $('.scroll-pane-lang').jScrollPane({showArrows: true});
            $('.btn-select-lang').addClass('active');
        });

		$('.points-count-lang.focus').click(function () {
            $('.all-block-option-lang').fadeOut(200);
            $('.btn-select-lang').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-lang').length ) 
            	return;
          	$('.btn-select-lang').removeClass('active');
          	$('.all-block-option-lang').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-lang').find(".points-count-lang").click(function () {
		  	$('.points-count-lang').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-lang.focus').click(function () {
          	var total = document.querySelector('.content-select-lang .points-count-lang.focus').innerHTML;
          	document.getElementById('total-lang').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        $('.into-slider-carousel').find(".one-of-like-user .left-block").hover(function() {
        	$(this).find('.block-hover-like-user').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.block-hover-like-user').fadeOut(200);
        	});
		});

		// ///////////////////////////////////////////////////////////////////

		$('.line-block-sympathy-user').find(".one-of-sympathy-user").hover(function() {
        	$(this).find('.block-hover-sympathy-user').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.block-hover-sympathy-user').fadeOut(200);
        	});
		});


        // ///////////////////////////////////////////////////////////////////



    </script>

    <script>

			  if(document.documentElement.clientWidth > 1340) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 10,
		          	slidesToScroll: 10
		        });
			  }

			  else if(document.documentElement.clientWidth > 900) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 7,
		          	slidesToScroll: 7
		        });
			  }

			 else if(document.documentElement.clientWidth > 600) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 5,
		          	slidesToScroll: 5
		        });
			  }

			  else if(document.documentElement.clientWidth > 0) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 2,
		          	slidesToScroll: 2
		        });
			  }

			  else {

			  }
        </script>

        <script type="text/javascript">

            $(document).ready(function(){

            $(".btn-mobile-open").click(function(){
                $(".btn-mobile-open").fadeOut(0);
                $(".under-header").animate({'width': "+=250px"}, 500);
                $(".under-header").css({'border': "2px solid white"});
                $(".btn-mobile-close").fadeIn(200);
            });

            $(".btn-mobile-close").click(function(){
                $(".under-header").animate({'width': "-=250px"}, 500);
                $(".under-header").css({'border': "0px"});
                $(".btn-mobile-close").fadeOut(0);
                $(".btn-mobile-open").fadeIn(200);
            });

            });
    </script>



	<!-- =============================================================== -->

  	<!-- end-script -->

  </body>
</html>