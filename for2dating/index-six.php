<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity">
  		
  		<!-- baground-slider -->

  		<div class="null-block">

  			<!-- slider-display-bg -->

	        <div class="slider-display-one">
	            <div class="left-block">
		            <!--  -->
		            <div>
		                <div class="into-slider-one six">
		                </div>                  
		            </div>
		            <!--  -->
	        	</div>
	    	</div>

	    	<!-- end-slider-display-bg -->
  				
  		</div>

  		<!-- end-baground-slider -->

  		<!-- header -->

  		<div class="left-one-bg-header">
  			<div class="left-two-bg-header">
	  			<div class="center-all-block">
	  				<div class="left-center-all-block">

	  					<!--  -->

	  					<div class="over-header">

			  				<div class="block-logo-header">
			  					<a href="#">
			  						<div class="logo-header">		  							
			  						</div>
			  					</a>
			  				</div>

			  				<!--  -->

			  				<div class="block-soc-header">
			  					<span>
			  						Мы в
			  					</span>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-vk">		  								
			  							</div>
			  						</a>
			  					</div>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-face">		  								
			  							</div>
			  						</a>
			  					</div>
			  				</div>

			  				<!-- lang-select -->
	  								
	  						<div class="block-select-registration-lang">
	  							<div class="select-registration-lang">
	  								<div id="total-lang">
	  									<div class="flag-one-of-lang">			  							
						  				</div>

						  				<span>
						  					Русский
						  				</span>
	  								</div>

	  								<div class="btn-select-lang">	  											
	  								</div>
	  							</div>

	  							<div class="all-block-option-lang">
									<div class="scroll-pane-lang">
										<div class="content-select-lang">
										    <div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Русский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Английский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Норвежский
								  				</span>
										   	</div>
									    </div>
									</div>
								</div>
	  						</div>
	  								
	  						<!-- end-lang-select -->

			  				<!--  -->

			  				<div class="block-reg-enter">
			  					<div class="btn-enter">
			  						<span>
			  							Войти
			  						</span>
			  					</div>

			  					<div class="btn-reg">
			  						<span>
			  							Регистрация
			  						</span>
			  					</div>
			  				</div>

			  				<!--  -->

		  				</div>

		  				<!--  -->

		  				<!-- mobile-menu -->

		  				<div class="null-block">
		  					<div class="null-block">
		  						<div class="btn-mobile-open">
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="btn-mobile-close">
		  						</div>
		  					</div>
		  				</div>

		  				<!-- end-mobile-menu -->

		  				<!--  -->

		  				<div class="under-header">

		  					<!--  -->

		  					<div class="one-of-menu office">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Мой кабинет
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu office">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои альбомы
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Моя статистика
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой пакет услуг
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой счет
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои статьи
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои подарки
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Пригласи друга
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои настройки
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu message">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Уведомления
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="block-number-message">
		  								<span>
		  									648
		  								</span>
		  							</div>
		  						</div>
		  						<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu message">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои сообщения <span>(9)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подмигиваний <span>(315)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Просмотры <span>(49)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Избранные <span>(5)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подарки <span>(3)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Симпатии <span>(5)</span>
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">
		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Блог
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Карусель
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->
		  					
		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							ТОП 100
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Поиск
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Идеальная пара
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  				</div>

		  				<!--  -->
	  					
	  				</div>
	  			</div>
	  		</div>
  		</div>

  		<!-- end-header -->



  		<!-- one-display -->

  		<div class="block-one-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="block-login-registration">
	  					
	  					<!--  -->

	  					<div class="block-btn-login-registration">
	  						<div class="all-block-btn-login">
	  							<div class="rotate-block-btn-login">
	  								<div class="btn-login active">
	  									<span>
	  										Вход
	  									</span>
	  								</div>
	  							</div>
	  						</div>

	  						<!--  -->

	  						<div class="all-block-btn-registration">
	  							<div class="rotate-block-btn-registration">
	  								<div class="btn-registration">
	  									<span>
	  										Регистрация
	  									</span>
	  								</div>
	  							</div>
	  						</div>
	  					</div>

	  					<!-- login -->
	  					
	  					<div class="block-content-login">
	  						<div class="null-block">
		  						<div class="logo-form-login">	  							
		  						</div>
		  					</div>

	  						<form>

	  							<!--  -->

		  						<span class="info-input">
		  							Email адрес
		  						</span>

		  						<!--  -->

		  						<div class="bg-input-login-registration">
		  							<input class="input-login-registration" placeholder="Адрес электронной почты">
		  						</div>

		  						<!--  -->

		  						<span class="info-input">
		  							Пароль
		  						</span>

		  						<!--  -->

		  						<div class="bg-input-login-registration password">
		  							<input class="input-login-registration" placeholder="******">
		  						</div>

		  						<!--  -->

		  						<a href="#">
			  						<span class="forget-password">
			  							Забыли пароль?
			  						</span>
			  					</a>

		  						<div class="block-check-forget-password">
		  							<input id="check-forget-password" type="checkbox" name="check-forget-password">
                      				<label class="check-forget-password" for="check-forget-password"></label>

                      				<span class="forget-password">
                      					Запомнить меня
                      				</span>
		  						</div>

		  						<!--  -->

		  						<div class="block-enter-from-soc">
		  							<div class="btn-enter-from-soc">
		  								<span>
		  									Войти
		  								</span>
		  							</div>

		  							<div class="btn-enter-from-soc-two">
		  								<span>
		  									Присоединиться через
		  								</span>

		  								<div class="icon-face-btn">		  								
			  							</div>
		  							</div>
		  						</div>

	  						</form>
	  					</div>

	  					<!-- end-login -->

	  					<!-- registretion -->
	  					
	  					<div class="block-content-registretion">
	  						<form>

	  							<!--  -->

	  							<div class="header-registretion">
	  								<span>
	  									Сайт для двоих
	  								</span>
	  							</div>

	  							<!--  -->

	  							<span class="registretion">
	  								Регистрация
	  							</span>

	  							<span class="or">
	  								или
	  							</span>

	  							<div class="btn-enter-from-soc-two">
		  							<span>
		  								Присоединиться через
		  							</span>

		  							<div class="icon-face-btn">		  								
			  						</div>
		  						</div>

	  							<!--  -->

	  							<div class="line-block-input-registretion first">
	  								<div class="bg-input-registretion">
	  									<input class="input-registration" placeholder="Имя">
	  								</div>

	  								<div class="icon-must">	  									
	  								</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-i-am">
	  								<span class="i-am">
	  									Я
	  								</span>

	  								<div class="block-check-woman-man">
			  							<input id="check-woman" type="checkbox" name="check-woman">
	                      				<label class="check-woman" for="check-woman"></label>

	                      				<span class="text-check-woman">
	                      					жещина
	                      				</span>
			  						</div>

			  						<div class="block-check-woman-man">
			  							<input id="check-man" type="checkbox" name="check-man">
	                      				<label class="check-man" for="check-man"></label>

	                      				<span class="text-check-woman">
	                      					мужчина
	                      				</span>
			  						</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-i-am">
	  								<span class="i-am">
	  									Ищу
	  								</span>

	  								<div class="block-check-woman-man">
			  							<input id="check-icon-woman" type="checkbox" name="check-icon-woman">
	                      				<label class="check-icon-woman" for="check-icon-woman"></label>

	                      				<span class="text-check-woman">
	                      					жещину
	                      				</span>
			  						</div>

			  						<div class="block-check-woman-man">
			  							<input id="check-icon-man" type="checkbox" name="check-icon-man">
	                      				<label class="check-icon-man" for="check-icon-man"></label>

	                      				<span class="text-check-woman">
	                      					мужчину
	                      				</span>
			  						</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-date-registration">

	  								<span class="date-registration">
	  									Дата рождения:
	  								</span>

	  								<!-- one-select -->
	  								
	  								<div class="block-select-registration-one">
	  									<div class="select-registration-one">
	  										<div id="total">
	  											День
	  										</div>

	  										<div class="btn-select">
	  											
	  										</div>
	  									</div>

	  									<div class="all-block-option">
											<div class="scroll-pane">
												<div class="content-select-one">
												    <div class="points-count focus">
												    	1
												    </div>

												    <div class="points-count focus">
												    	2
												    </div>

												    <div class="points-count focus">
												    	3
												    </div>

												    <div class="points-count focus">
												    	4
												    </div>

												    <div class="points-count focus">
												    	5
												    </div>

												    <div class="points-count focus">
												    	6
												    </div>

												    <div class="points-count focus">
												    	7
												    </div>

												    <div class="points-count focus">
												    	8
												    </div>

												    <div class="points-count focus">
												    	9
												    </div>

												    <div class="points-count focus">
												    	10
												    </div>

												    <div class="points-count focus">
												    	11
												    </div>

												    <div class="points-count focus">
												    	12
												    </div>

												    <div class="points-count focus">
												    	13
												    </div>

												    <div class="points-count focus">
												    	14
												    </div>

												    <div class="points-count focus">
												    	15
												    </div>

												    <div class="points-count focus">
												    	16
												    </div>

												    <div class="points-count focus">
												    	17
												    </div>

												    <div class="points-count focus">
												    	18
												    </div>

												    <div class="points-count focus">
												    	19
												    </div>

												    <div class="points-count focus">
												    	20
												    </div>

												    <div class="points-count focus">
												    	21
												    </div>

												    <div class="points-count focus">
												    	22
												    </div>

												    <div class="points-count focus">
												    	23
												    </div>

												    <div class="points-count focus">
												    	24
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-one-select -->

	  								<!-- two-select -->
	  								
	  								<div class="block-select-registration-two">
	  									<div class="select-registration-two">
	  										<div id="total-two">
	  											Месяц
	  										</div>

	  										<div class="btn-select-two">	  											
	  										</div>
	  									</div>

	  									<div class="all-block-option-two">
											<div class="scroll-pane-two">
												<div class="content-select-two">
												    <div class="points-count-two focus">
												    	Январь
												    </div>

												    <div class="points-count-two focus">
												    	Февраль
												    </div>

												    <div class="points-count-two focus">
												    	Март
												    </div>

												    <div class="points-count-two focus">
												    	Апрель
												    </div>

												    <div class="points-count-two focus">
												    	Май
												    </div>

												    <div class="points-count-two focus">
												    	Июнь
												    </div>

												    <div class="points-count-two focus">
												    	Июль
												    </div>

												    <div class="points-count-two focus">
												    	Август
												    </div>

												    <div class="points-count-two focus">
												    	Сентябрь
												    </div>

												    <div class="points-count-two focus">
												    	Октябрь
												    </div>

												    <div class="points-count-two focus">
												    	Ноябрь
												    </div>

												    <div class="points-count-two focus">
												    	Декабрь
												    </div>
											    </div>
											</div>
										</div>
	  								</div>
	  								
	  								<!-- end-two-select -->

	  								<!-- three-select -->
	  								
	  								<div class="block-select-registration-three">
	  									<div class="select-registration-three">
	  										<div id="total-three">
	  											Год
	  										</div>

	  										<div class="btn-select-three">	  											
	  										</div>
	  									</div>

	  									<div class="all-block-option-three">
											<div class="scroll-pane-three">
												<div class="content-select-three">
												    <div class="points-count-three focus">
												    	2016
												    </div>

												    <div class="points-count-three focus">
												    	2015
												    </div>

												    <div class="points-count-three focus">
												    	2014
												    </div>

												    <div class="points-count-three focus">
												    	2013
												    </div>

												    <div class="points-count-three focus">
												    	2012
												    </div>

												    <div class="points-count-three focus">
												    	2011
												    </div>

												    <div class="points-count-three focus">
												    	2010
												    </div>

												    <div class="points-count-three focus">
												    	2009
												    </div>

												    <div class="points-count-three focus">
												    	2008
												    </div>

												    <div class="points-count-three focus">
												    	2007
												    </div>

												    <div class="points-count-three focus">
												    	2006
												    </div>

												    <div class="points-count-three focus">
												    	2005
												    </div>
											    </div>
											</div>
										</div>
	  								</div>
	  								
	  								<!-- end-three-select -->

	  							</div>

	  							<!--  -->



	  								<div class="line-block-input-registretion">
		  								<div class="bg-input-registretion">
		  									<input class="input-registration" placeholder="Страна">
		  								</div>

		  								<div class="icon-must">	  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-block-input-registretion">
		  								<div class="bg-input-registretion">
		  									<input class="input-registration" placeholder="Адрес электронной почты">
		  								</div>

		  								<div class="icon-must">	  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-block-input-registretion">
		  								<div class="bg-input-registretion">
		  									<input class="input-registration" placeholder="Пароль">
		  								</div>

		  								<div class="icon-must">	  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-block-input-registretion last">
		  								<div class="bg-input-registretion">
		  									<input class="input-registration" placeholder="Повторите пароль">
		  								</div>

		  								<div class="icon-must">	  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="block-btn-all-registration">
		  								<div class="btn-all-registration">
		  									<span>
		  										Регистрация
		  									</span>
		  								</div>		  								
		  							</div>

		  							<!--  -->

		  							<div class="text-under-btn-registration">
		  								<span>
		  									Нажимая <span class="bold">Регистрация,</span> ты принимаешь наши <a href="#"><span class="pink">Условия и правила</span></a> и наше <a href="#"><span class="pink">Положение о конфиденциальности</span></a>, в том числе использование нами Куки, ты также соглашаешься получать электронные уведомления о своем аккаунте, от которых ты можешь отписаться в любое время.
		  								</span>
		  							</div>

		  							<!--  -->

	  						</form>
	  					</div>

	  					<!-- end-registretion -->

	  				</div>

	  				<!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-one-display -->



  		<!-- two-display -->

  		<div class="block-two-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="header-two-display">
	  					<span>
	  						For two dating
	  					</span>
	  				</div>

	  				<!--  -->

	  				<!--  -->

	  				<div class="left-content-two-display">
	  					<span>
	  						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
	  					</span>

	  					<span>
	  						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
	  					</span>

	  					<span>
	  						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
	  					</span>
	  				</div>

	  				<!--  -->

	  				<!--  -->

	  				<div class="right-content-two-display">
		  				<div class="block-video-player">
		  					<div class="null-block">
		  						<div class="bg-all-btn-start-play">
		  							<div class="border-btn-start-play">
		  								<div class="btn-start-play">		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="block-navigate-player">
		  							<div class="navigate-player">

		  								<!--  -->

		  								<div class="block-btn-play-stop">
		  									<div class="btn-stop">		  									
		  									</div>
		  								</div>

		  								<!--  -->

		  								<div class="block-time-progress">
		  									<div class="bg-line-time-progress">
		  										<div class="load-line-time-progress">
		  											<div class="btn-circle-time-progress">
		  												
		  											</div>
		  										</div>
		  									</div>
		  								</div>

		  								<!--  -->

		  								<div class="block-volume">
		  									<div class="btn-volume">
		  										
		  									</div>
		  								</div>

		  								<!--  -->
		  							</div>		  							
		  						</div>
		  					</div>
		  				</div>
	  				</div>

	  				<!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-two-display -->



  		<!-- three-display -->

  		<!-- baground-slider-two -->

  		<div class="block-three-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="header-three-display">
	  					<span>
	  						Отзывы наших пользователей
	  					</span>
	  				</div>

	  				<!--  -->

		  			<!-- slider-display-three -->

			        <div class="slider-display-three">

			            <div class="regular-two slider">
				            
				            <!--  -->

			                <div>
			                  	<div class="into-slider-two">

				                    <div class="block-photo-user">
				                    	<div class="border-photo-user">
				                    		<div class="bg-photo-user">
				                    			<div class="around-photo-user">
				                    				<img src="img/one-photo-user.png" alt="not-image">
				                    			</div>
				                    		</div>
				                    	</div>			                      
				                    </div>

				                    <div class="block-text-into-slider-two">

				                    	<div class="header-text-into-slider-two">
				                    		<span class="name-user">
				                    			Генри и Ольга
				                    		</span>

				                    		<div class="block-number-comments-slide-two">
				                    			<a href="#">
					                    			<span class="text-number-comments">
					                    				Все отзывы
					                    			</span>
				                    			</a>
				                    			<span class="number-comments">
				                    				248
				                    			</span>
				                    		</div>
				                    	</div>

				                      	<span class="comment">
				                      		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				                      	</span>

				                      	<a href="#">
					                      	<span class="read-more">
					                      		Читать далее>>
					                      	</span>
				                      	</a>
				                    </div>

			                  	</div>              
			                </div>

			                <!--  -->

			                <!--  -->

			                <div>
			                  	<div class="into-slider-two">

				                    <div class="block-photo-user">
				                    	<div class="border-photo-user">
				                    		<div class="bg-photo-user">
				                    			<div class="around-photo-user">
				                    				<img src="img/one-photo-user.png" alt="not-image">
				                    			</div>
				                    		</div>
				                    	</div>			                      
				                    </div>

				                    <div class="block-text-into-slider-two">

				                    	<div class="header-text-into-slider-two">
				                    		<span class="name-user">
				                    			Генри и Ольга
				                    		</span>

				                    		<div class="block-number-comments-slide-two">
				                    			<a href="#">
					                    			<span class="text-number-comments">
					                    				Все отзывы
					                    			</span>
				                    			</a>
				                    			<span class="number-comments">
				                    				248
				                    			</span>
				                    		</div>
				                    	</div>

				                      	<span class="comment">
				                      		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				                      	</span>

				                      	<a href="#">
					                      	<span class="read-more">
					                      		Читать далее>>
					                      	</span>
				                      	</a>
				                    </div>

			                  	</div>              
			                </div>

			                <!--  -->

			                <!--  -->

			                <div>
			                  	<div class="into-slider-two">

				                    <div class="block-photo-user">
				                    	<div class="border-photo-user">
				                    		<div class="bg-photo-user">
				                    			<div class="around-photo-user">
				                    				<img src="img/one-photo-user.png" alt="not-image">
				                    			</div>
				                    		</div>
				                    	</div>			                      
				                    </div>

				                    <div class="block-text-into-slider-two">

				                    	<div class="header-text-into-slider-two">
				                    		<span class="name-user">
				                    			Генри и Ольга
				                    		</span>

				                    		<div class="block-number-comments-slide-two">
				                    			<a href="#">
					                    			<span class="text-number-comments">
					                    				Все отзывы
					                    			</span>
				                    			</a>
				                    			<span class="number-comments">
				                    				248
				                    			</span>
				                    		</div>
				                    	</div>

				                      	<span class="comment">
				                      		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				                      	</span>

				                      	<a href="#">
					                      	<span class="read-more">
					                      		Читать далее>>
					                      	</span>
				                      	</a>
				                    </div>

			                  	</div>              
			                </div>

			                <!--  -->

			        	</div>
			    	</div>

			    	<!-- end-slider-display-three -->

			    </div>
			</div>
		</div>

  		<!-- end-three-display -->

  		<!-- four-display -->

  		<div class="block-four-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="header-four-display">
	  					<span>
	  						Последние новости с блога
	  					</span>
	  				</div>

	  				<!--  -->

	  				<!--  -->

	  				<div class="left-block four-display">
	  					<div class="right-block">
				            <a href="#">
					           	<span class="all-article">
					           		Все статьи
					           	</span>
				            </a>
				        </div>
	  				</div>

	  				<!--  -->

	  				<!--  -->
			        
			        <div class="block-one-of-article">

				        <div class="block-photo-user">
				           	<div class="border-photo-user pink">
				           		<div class="bg-photo-user">
				           			<div class="around-photo-user">
				           				<img src="img/two-photo-user.png" alt="not-image">
				           			</div>
				           		</div>
				           	</div>			                      
				        </div>

				        <div class="block-text-one-of-article">

				           	<span class="name-user pink">
				           		Оля
				           	</span>

				           	<span class="question-user">
				           		Вопрос:
				           	</span>

				            <span class="comment">
				            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
				            </span>

				            <span class="question-user">
				           		Отвечает психолог Краснова А.
				           	</span>

				            <span class="comment last">
				            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				            </span>

				            <a href="#">
					          	<span class="read-more pink">
					          		Читать далее>>
					          	</span>
				          	</a>
				        </div>

			        </div>

			        <!--  -->

			        <div class="separator-betwen-article">
			        	
			        </div>

			        <!--  -->

			        <div class="block-one-of-article">

				        <div class="block-photo-user">
				           	<div class="border-photo-user blue">
				           		<div class="bg-photo-user">
				           			<div class="around-photo-user">
				           				<img src="img/three-photo-user.png" alt="not-image">
				           			</div>
				           		</div>
				           	</div>			                      
				        </div>

				        <div class="block-text-one-of-article">

				           	<span class="name-user blue">
				           		Gjerde
				           	</span>

				           	<span class="question-user">
				           		Вопрос:
				           	</span>

				            <span class="comment">
				            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
				            </span>

				            <span class="question-user">
				           		Отвечает психолог Краснова А.
				           	</span>

				            <span class="comment last">
				            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio...
				            </span>

				            <a href="#">
					          	<span class="read-more blue">
					          		Читать далее>>
					          	</span>
				          	</a>
				        </div>

			        </div>

			        <!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-four-display -->

  		<!-- footer-display -->

  		<div class="footer-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

		  			<div class="footer-menu">

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					О нас
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Условия и правила
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Конфиденциальность
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Партнерам
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Отзывы
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Связаться с Нами
		  				</span>

		  				<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="block-soc-footer">

		  				<!--  -->

		  				<span class="header-soc-footer">
		  					Присоединяйся к <span>For two dating:</span>
		  				</span>

		  				<!--  -->

		  				<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-vk">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-face">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-twit">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-inst">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-yt">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-gp">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="footer-all-right">
		  				<span class="header-soc-footer">
		  					© 2016- 2016 For2date. Все права защищены. Разработка I-PR
		  				</span>
		  			</div>

		  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-footer-display -->

  	</div>

  	<!-- script -->

  	<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

  	<script type="text/javascript">
        $(".regular-first").slick({
          	dots: false,
          	autoplay: true,
           	prevArrow: false,
            nextArrow: false,
            centerMode: true,
          	infinite: true,
          	slidesToShow: 1,
          	slidesToScroll: 1,
          	autoplaySpeed: 10000
        });

        $(".regular-two").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });
    </script>

    <!-- =============================================================== -->

    <script src="js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
	    var config = {
	      '.chosen-select'           : {},
	      '.chosen-select-deselect'  : {allow_single_deselect:true},
	      '.chosen-select-no-single' : {disable_search_threshold:1},
	      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	      '.chosen-select-width'     : {width:"95%"}
	    }
	    for (var selector in config) {
	      $(selector).chosen(config[selector]);
	    }
	</script>

	<!-- =============================================================== -->

	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>

	<script type="text/javascript" src="js/jquery.jscrollpane.js"></script>

	<script type="text/javascript">

		$('.one-of-menu.message').click(function () {
            $('.all-block-dropdown-menu.message').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.message').length ) 
            	return;
          	$('.all-block-dropdown-menu.message').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.message').mouseleave (function() {
          $('.all-block-dropdown-menu.message').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

        $('.one-of-menu.office').click(function () {
            $('.all-block-dropdown-menu.office').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.office').length ) 
            	return;
          	$('.all-block-dropdown-menu.office').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.office').mouseleave (function() {
          $('.all-block-dropdown-menu.office').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

		$('.btn-login').click(function () {
            $('.block-content-registretion').fadeOut(0);
            $('.block-content-login').fadeIn(0);
            $('.btn-login').addClass('active');
            $('.btn-registration').removeClass('active');
        });

        $('.btn-registration').click(function () {
            $('.block-content-login').fadeOut(0);
            $('.block-content-registretion').fadeIn(0);
            $('.btn-registration').addClass('active');
            $('.btn-login').removeClass('active');
        });

        // //////////////////////////////////////////////////////////

		$('.select-registration-one').click(function () {
            $('.all-block-option').fadeIn(200);
            $('.scroll-pane').jScrollPane({showArrows: true});
            $('.btn-select').addClass('active');
        });

		$('.points-count.focus').click(function () {
            $('.all-block-option').fadeOut(200);
            $('.btn-select').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-one').length ) 
            	return;
          	$('.btn-select').removeClass('active');
          	$('.all-block-option').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-one').find(".points-count").click(function () {
		  	$('.points-count').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count.focus').click(function () {
          	var total = document.querySelector('.content-select-one .points-count.focus').innerHTML;
          	document.getElementById('total').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-two').click(function () {
            $('.all-block-option-two').fadeIn(200);
            $('.scroll-pane-two').jScrollPane({showArrows: true});
            $('.btn-select-two').addClass('active');
        });

		$('.points-count-two.focus').click(function () {
            $('.all-block-option-two').fadeOut(200);
            $('.btn-select-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-two').length ) 
            	return;
          	$('.btn-select-two').removeClass('active');
          	$('.all-block-option-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-two').find(".points-count-two").click(function () {
		  	$('.points-count-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-two.focus').click(function () {
          	var total = document.querySelector('.content-select-two .points-count-two.focus').innerHTML;
          	document.getElementById('total-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-three').click(function () {
            $('.all-block-option-three').fadeIn(200);
            $('.scroll-pane-three').jScrollPane({showArrows: true});
            $('.btn-select-three').addClass('active');
        });

		$('.points-count-three.focus').click(function () {
            $('.all-block-option-three').fadeOut(200);
            $('.btn-select-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-three').length ) 
            	return;
          	$('.btn-select-three').removeClass('active');
          	$('.all-block-option-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-three').find(".points-count-three").click(function () {
		  	$('.points-count-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-three.focus').click(function () {
          	var total = document.querySelector('.content-select-three .points-count-three.focus').innerHTML;
          	document.getElementById('total-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-lang').click(function () {
            $('.all-block-option-lang').fadeIn(200);
            // $('.scroll-pane-lang').jScrollPane({showArrows: true});
            $('.btn-select-lang').addClass('active');
        });

		$('.points-count-lang.focus').click(function () {
            $('.all-block-option-lang').fadeOut(200);
            $('.btn-select-lang').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-lang').length ) 
            	return;
          	$('.btn-select-lang').removeClass('active');
          	$('.all-block-option-lang').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-lang').find(".points-count-lang").click(function () {
		  	$('.points-count-lang').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-lang.focus').click(function () {
          	var total = document.querySelector('.content-select-lang .points-count-lang.focus').innerHTML;
          	document.getElementById('total-lang').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////



    </script>

    <script type="text/javascript">

            $(document).ready(function(){

            $(".btn-mobile-open").click(function(){
                $(".btn-mobile-open").fadeOut(0);
                $(".under-header").animate({'width': "+=250px"}, 500);
                $(".under-header").css({'border': "2px solid white"});
                $(".btn-mobile-close").fadeIn(200);
            });

            $(".btn-mobile-close").click(function(){
                $(".under-header").animate({'width': "-=250px"}, 500);
                $(".under-header").css({'border': "0px"});
                $(".btn-mobile-close").fadeOut(0);
                $(".btn-mobile-open").fadeIn(200);
            });

            });
    </script>



	<!-- =============================================================== -->

  	<!-- end-script -->

  </body>
</html>