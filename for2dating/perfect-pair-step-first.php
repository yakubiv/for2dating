<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />
    <link rel="stylesheet" href="css/form-validation.css">

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity perfect-pair-step-first">

  		<!-- baground-slider -->

  		<div class="null-block">

  			<!-- slider-display-bg -->

	        <div class="slider-display-one" style="display:none;">
	            <div class="regular-first slider">
		            <!--  -->
		            <div>
		                <div class="into-slider-one one">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one two">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one three">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one four">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one five">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one six">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one seven">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one eight">
		                </div>
		            </div>
		            <!--  -->
	        	</div>
	    	</div>

	    	<!-- end-slider-display-bg -->

  		</div>

  		<!-- end-baground-slider -->

  		<!-- header -->

  		<div class="left-one-bg-header">
  			<div class="left-two-bg-header">
	  			<div class="center-all-block">
	  				<div class="left-center-all-block">

	  					<!--  -->

	  					<div class="over-header">

			  				<div class="block-logo-header">
			  					<a href="#">
			  						<div class="logo-header">
			  						</div>
			  					</a>
			  				</div>

			  				<!--  -->

			  				<div class="block-soc-header">
			  					<span>
			  						Мы в
			  					</span>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-vk">
			  							</div>
			  						</a>
			  					</div>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-face">
			  							</div>
			  						</a>
			  					</div>
			  				</div>

			  				<!-- lang-select -->

	  						<div class="block-select-registration-lang">
	  							<div class="select-registration-lang">
	  								<div id="total-lang">
	  									<div class="flag-one-of-lang">
						  				</div>

						  				<span>
						  					Русский
						  				</span>
	  								</div>

	  								<div class="btn-select-lang">
	  								</div>
	  							</div>

	  							<div class="all-block-option-lang">
									<div class="scroll-pane-lang">
										<div class="content-select-lang">
										    <div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">
								  				</div>

								  				<span>
								  					Русский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">
								  				</div>

								  				<span>
								  					Английский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">
								  				</div>

								  				<span>
								  					Норвежский
								  				</span>
										   	</div>
									    </div>
									</div>
								</div>
	  						</div>

	  						<!-- end-lang-select -->

			  				<!--  -->

			  				<div class="block-reg-enter">
			  					<div class="btn-enter">
			  						<span>
			  							Войти
			  						</span>
			  					</div>

			  					<div class="btn-reg">
			  						<span>
			  							Регистрация
			  						</span>
			  					</div>
			  				</div>

			  				<!--  -->

		  				</div>

		  				<!--  -->

		  				<!-- mobile-menu -->

		  				<div class="null-block">
		  					<div class="null-block">
		  						<div class="btn-mobile-open">
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="btn-mobile-close">
		  						</div>
		  					</div>
		  				</div>

		  				<!-- end-mobile-menu -->

		  				<!--  -->

		  				<div class="under-header">

		  					<!--  -->

		  					<div class="one-of-menu office">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Мой кабинет
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu office">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои альбомы
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Моя статистика
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой пакет услуг
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой счет
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои статьи
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои подарки
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Пригласи друга
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои настройки
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu message">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Уведомления
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="block-number-message">
		  								<span>
		  									648
		  								</span>
		  							</div>
		  						</div>
		  						<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu message">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои сообщения <span>(9)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подмигиваний <span>(315)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Просмотры <span>(49)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Избранные <span>(5)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подарки <span>(3)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Симпатии <span>(5)</span>
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">

		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Блог
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Карусель
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							ТОП 100
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Поиск
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Идеальная пара
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  				</div>

		  				<!--  -->

	  				</div>
	  			</div>
	  		</div>
  		</div>

  		<!-- end-header -->

  		<!-- line-block-under-header -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="line-block-under-header">
	  					<div class="one-step">
	  						<span class="bold">
	  							Шаг
	  						</span>

	  						<span class="bg-around">
	  							1
	  						</span>

	  						<span class="regular">
	  							Фото
	  						</span>
	  					</div>

	  					<!--  -->

	  					<div class="two-step">
	  						<span class="bold">
	  							Шаг
	  						</span>

	  						<span class="bg-around">
	  							2
	  						</span>

	  						<span class="regular">
	  							Персональные данные
	  						</span>
	  					</div>

	  					<!--  -->

	  					<div class="three-step">
	  						<span class="bold">
	  							Шаг
	  						</span>

	  						<span class="bg-around">
	  							3
	  						</span>

	  						<span class="regular">
	  							О себе
	  						</span>
	  					</div>

	  					<!--  -->

	  					<div class="four-step">
	  						<span class="bold">
	  							Шаг
	  						</span>

	  						<span class="bg-around">
	  							4
	  						</span>

	  						<span class="regular">
	  							Я ищу
	  						</span>
	  					</div>

	  					<!--  -->

	  				</div>

	  				<!--  -->

	  				<!--  -->

	  				<span class="line-block-under-step">
	  					Заполните обязательные поля, чтобы вас находили чаще
	  				</span>

	  				<!--  -->

	  				<!--  -->

	  				<span class="heaedr-block-one-step-registration">
	  					Загрузить фото
	  				</span>

	  				<div class="block-one-step-registration">

	  					<!--  -->

	  					<div class="right-block-one-step-registration">

	  						<span class="one-of-header">
	  							С Facebook
	  						</span>

	  						<span class="under-one-of-header">
	  							Скопируйте фото с вашего профиля в вашей учетной записи на Face
	  						</span>

	  						<!--  -->

	  						<div class="block-btn-face-step">
	  							<div class="btn-face-step">
	  								<div class="icon-face-btn">
			  						</div>

		  							<span>
		  								Соединиться
		  							</span>
		  						</div>
	  						</div>


	  					</div>

	  					<!--  -->

	  					<div class="left-block-one-step-registration">
	  						<div class="null-block">
	  							<span class="around-or">
	  								или
	  							</span>
	  						</div>

	  						<span class="one-of-header">
	  							С вашего компьютера
	  						</span>

	  						<span class="under-one-of-header">
	  							1. Просматривать фото.
	  						</span>

	  						<span class="under-one-of-header">
	  							2. Подтвердить, что это ваше фото.
	  						</span>

	  						<span class="under-one-of-header">
	  							3. Щелкнуть для загруски.
	  						</span>

	  						<!--  -->

	  						<div class="block-form-search">
	  							<span class="around-step">
	  								1
	  							</span>

	  							<!--  -->

	  							<form class="form-search">
		  							<input type="button" class="btn-search" value="Поиск">

		  							<div class="bg-input-search">
		  								<input type="text" class="input-search" disabled>
		  							</div>

                    <input type="file" id="form-search-file">
		  						</form>

	  							<!--  -->
	  						</div>

	  						<!--  -->

	  						<!--  -->

	  						<div class="block-photo-one-step">
	  							<div class="border-photo-one-step">
	  								<img src="img/photo-one-step.png" alt="not-image" id="form-search-image">
	  							</div>
	  						</div>

	  						<!--  -->

	  						<!--  -->

	  						<div class="step-under-photo">
	  							<span class="around-step disable">
	  								2
	  							</span>

	  							<!--  -->

	  							<div class="block-check-step">
	  								<input id="check-step" type="checkbox" name="check-step">
                      				<label class="check-step" for="check-step"></label>
	  							</div>

	  							<!--  -->

	  							<span class="text-step-two">
	  								Я подтверждаю, что у меня есть все<br>
									права на это изображение
	  							</span>

	  							<!--  -->

	  						</div>

	  						<!--  -->

	  						<!--  -->

	  						<div class="step-under-photo">
	  							<span class="around-step disable">
	  								3
	  							</span>

	  							<!--  -->

	  							<div class="btn-add-step">
	  								<span>
	  									Добавить
	  								</span>
	  							</div>

	  							<!--  -->

	  						</div>

	  						<!--  -->

	  					</div>

	  					<!--  -->

	  				</div>

	  				<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-line-block-under-header -->

  		<!-- display-chose-photo -->

  		<div class="display-chose-photo">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<div class="block-chose-photo-step">
	  					<div class="border-chose-photo-step">
	  						<img src="img/photo-chose-step.png" alt="not-image">
	  					</div>
	  				</div>

	  				<!--  -->

	  				<div class="block-info-chose-photo">
	  					<span class="one-of-header">
	  						Как выбрать правильное основное фото:
	  					</span>

	  					<span class="under-one-of-header">
	  						Вы улыбаетесь :)
	  					</span>

	  					<span class="under-one-of-header">
	  						На фото только вы
	  					</span>

	  					<span class="under-one-of-header">
	  						Недавнее фото
	  					</span>

	  					<span class="under-one-of-header">
	  						Четко показывает ваше лицо
	  					</span>

	  					<span class="under-one-of-header">
	  						Хорошего качества и освещения
	  					</span>

	  					<span class="under-one-of-header">
	  						НЕ содержит наготы
	  					</span>

	  					<span class="under-one-of-header">
	  						Размер фото ХХХ*YYY
	  					</span>
	  				</div>

	  				<!--  -->

	  				<span class="btn-next-step">
	  					Далее
	  				</span>

	  				<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-display-chose-photo -->

  		<!-- footer-display -->

  		<div class="footer-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

		  			<div class="footer-menu">

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					О нас
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Условия и правила
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Конфиденциальность
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Партнерам
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Отзывы
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Связаться с Нами
		  				</span>

		  				<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="block-soc-footer">

		  				<!--  -->

		  				<span class="header-soc-footer">
		  					Присоединяйся к <span>For two dating:</span>
		  				</span>

		  				<!--  -->

		  				<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-vk">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-face">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-twit">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-inst">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-yt">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-gp">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="footer-all-right">
		  				<span class="header-soc-footer">
		  					© 2016- 2016 For2date. Все права защищены. Разработка I-PR
		  				</span>
		  			</div>

		  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-footer-display -->

  	</div>

  	<!-- script -->

  	<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

  	<script type="text/javascript">
        $(".regular-first").slick({
          	dots: false,
          	autoplay: true,
           	prevArrow: false,
            nextArrow: false,
            centerMode: true,
          	infinite: true,
          	slidesToShow: 1,
          	slidesToScroll: 1,
          	autoplaySpeed: 10000
        });

        $(".regular-two").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });
    </script>

    <!-- =============================================================== -->

    <script src="js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
	    var config = {
	      '.chosen-select'           : {},
	      '.chosen-select-deselect'  : {allow_single_deselect:true},
	      '.chosen-select-no-single' : {disable_search_threshold:1},
	      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	      '.chosen-select-width'     : {width:"95%"}
	    }
	    for (var selector in config) {
	      $(selector).chosen(config[selector]);
	    }
	</script>

	<!-- =============================================================== -->

	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>

	<script type="text/javascript" src="js/jquery.jscrollpane.js"></script>

	<script type="text/javascript">

		$('.one-of-menu.message').click(function () {
            $('.all-block-dropdown-menu.message').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.message').length )
            	return;
          	$('.all-block-dropdown-menu.message').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.message').mouseleave (function() {
          $('.all-block-dropdown-menu.message').fadeOut(200);
        });

        // //////////////////////////////////////////////////////////

        $('.one-of-menu.office').click(function () {
            $('.all-block-dropdown-menu.office').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.office').length )
            	return;
          	$('.all-block-dropdown-menu.office').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.office').mouseleave (function() {
          $('.all-block-dropdown-menu.office').fadeOut(200);
        });

        // //////////////////////////////////////////////////////////

		$('.btn-login').click(function () {
            $('.block-content-registretion').fadeOut(0);
            $('.block-content-login').fadeIn(0);
            $('.btn-login').addClass('active');
            $('.btn-registration').removeClass('active');
        });

        $('.btn-registration').click(function () {
            $('.block-content-login').fadeOut(0);
            $('.block-content-registretion').fadeIn(0);
            $('.btn-registration').addClass('active');
            $('.btn-login').removeClass('active');
        });

        // //////////////////////////////////////////////////////////

		$('.select-registration-one').click(function () {
            $('.all-block-option').fadeIn(200);
            $('.scroll-pane').jScrollPane({showArrows: true});
            $('.btn-select').addClass('active');
        });

		$('.points-count.focus').click(function () {
            $('.all-block-option').fadeOut(200);
            $('.btn-select').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-one').length )
            	return;
          	$('.btn-select').removeClass('active');
          	$('.all-block-option').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-one').find(".points-count").click(function () {
		  	$('.points-count').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count.focus').click(function () {
          	var total = document.querySelector('.content-select-one .points-count.focus').innerHTML;
          	document.getElementById('total').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-two').click(function () {
            $('.all-block-option-two').fadeIn(200);
            $('.scroll-pane-two').jScrollPane({showArrows: true});
            $('.btn-select-two').addClass('active');
        });

		$('.points-count-two.focus').click(function () {
            $('.all-block-option-two').fadeOut(200);
            $('.btn-select-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-two').length )
            	return;
          	$('.btn-select-two').removeClass('active');
          	$('.all-block-option-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-two').find(".points-count-two").click(function () {
		  	$('.points-count-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-two.focus').click(function () {
          	var total = document.querySelector('.content-select-two .points-count-two.focus').innerHTML;
          	document.getElementById('total-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-three').click(function () {
            $('.all-block-option-three').fadeIn(200);
            $('.scroll-pane-three').jScrollPane({showArrows: true});
            $('.btn-select-three').addClass('active');
        });

		$('.points-count-three.focus').click(function () {
            $('.all-block-option-three').fadeOut(200);
            $('.btn-select-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-three').length )
            	return;
          	$('.btn-select-three').removeClass('active');
          	$('.all-block-option-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-three').find(".points-count-three").click(function () {
		  	$('.points-count-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-three.focus').click(function () {
          	var total = document.querySelector('.content-select-three .points-count-three.focus').innerHTML;
          	document.getElementById('total-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-lang').click(function () {
            $('.all-block-option-lang').fadeIn(200);
            // $('.scroll-pane-lang').jScrollPane({showArrows: true});
            $('.btn-select-lang').addClass('active');
        });

		$('.points-count-lang.focus').click(function () {
            $('.all-block-option-lang').fadeOut(200);
            $('.btn-select-lang').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-lang').length )
            	return;
          	$('.btn-select-lang').removeClass('active');
          	$('.all-block-option-lang').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-lang').find(".points-count-lang").click(function () {
		  	$('.points-count-lang').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-lang.focus').click(function () {
          	var total = document.querySelector('.content-select-lang .points-count-lang.focus').innerHTML;
          	document.getElementById('total-lang').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////



    </script>

    <script type="text/javascript">

            $(document).ready(function(){

            $(".btn-mobile-open").click(function(){
                $(".btn-mobile-open").fadeOut(0);
                $(".under-header").animate({'width': "+=250px"}, 500);
                $(".under-header").css({'border': "2px solid white"});
                $(".btn-mobile-close").fadeIn(200);
            });

            $(".btn-mobile-close").click(function(){
                $(".under-header").animate({'width': "-=250px"}, 500);
                $(".under-header").css({'border': "0px"});
                $(".btn-mobile-close").fadeOut(0);
                $(".btn-mobile-open").fadeIn(200);
            });

            });
    </script>



	<!-- =============================================================== -->

  <script src="js/form-validation.js"></script>

  	<!-- end-script -->

  </body>
</html>
