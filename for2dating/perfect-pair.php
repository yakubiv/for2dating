<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity bg-perfect-pair">
  		
  		<!-- baground-slider -->

  		<div class="null-block">

  			<!-- slider-display-bg -->

	        <div class="slider-display-one" style="display:none;">
	            <div class="regular-first slider">
		            <!--  -->
		            <div>
		                <div class="into-slider-one one">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one two">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one three">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one four">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one five">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one six">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one seven">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one eight">
		                </div>                  
		            </div>
		            <!--  -->
	        	</div>
	    	</div>

	    	<!-- end-slider-display-bg -->
  				
  		</div>

  		<!-- end-baground-slider -->

  		<!-- header -->

  		<div class="left-one-bg-header">
  			<div class="left-two-bg-header">
	  			<div class="center-all-block">
	  				<div class="left-center-all-block">

	  					<!--  -->

	  					<div class="over-header">

			  				<div class="block-logo-header">
			  					<a href="#">
			  						<div class="logo-header">		  							
			  						</div>
			  					</a>
			  				</div>

			  				<!--  -->

			  				<div class="block-soc-header">
			  					<span>
			  						Мы в
			  					</span>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-vk">		  								
			  							</div>
			  						</a>
			  					</div>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-face">		  								
			  							</div>
			  						</a>
			  					</div>
			  				</div>

			  				<!-- lang-select -->
	  								
	  						<div class="block-select-registration-lang">
	  							<div class="select-registration-lang">
	  								<div id="total-lang">
	  									<div class="flag-one-of-lang">			  							
						  				</div>

						  				<span>
						  					Русский
						  				</span>
	  								</div>

	  								<div class="btn-select-lang">	  											
	  								</div>
	  							</div>

	  							<div class="all-block-option-lang">
									<div class="scroll-pane-lang">
										<div class="content-select-lang">
										    <div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Русский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Английский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Норвежский
								  				</span>
										   	</div>
									    </div>
									</div>
								</div>
	  						</div>
	  								
	  						<!-- end-lang-select -->

			  				<!-- account -->

			  				<div class="block-account">
			  					<div class="block-coints">
			  						<span class="coints">
			  							0
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-user-header">
			  						<div class="border-photo-user-header">
			  							<img src="img/photo-user-header.png" alt="not-image">
			  						</div>

			  						<span>
			  							Александр
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-setting">
			  						<div class="icon-setting">			  							
			  						</div>
			  					</div>

			  					<!--  -->

			  					<div class="block-icon-enter-header">
			  						<div class="icon-enter-header">			  							
			  						</div>
			  					</div>

			  					<!--  -->

			  				</div>

			  				<!-- end-account -->

		  				</div>

		  				<!--  -->

		  				<!-- mobile-menu -->

		  				<div class="null-block">
		  					<div class="null-block">
		  						<div class="btn-mobile-open">
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="btn-mobile-close">
		  						</div>
		  					</div>
		  				</div>

		  				<!-- end-mobile-menu -->

		  				<!--  -->

		  				<div class="under-header">

		  					<!--  -->

		  					<div class="one-of-menu office">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Мой кабинет
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu office">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои альбомы
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Моя статистика
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой пакет услуг
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой счет
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои статьи
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои подарки
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Пригласи друга
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои настройки
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu message">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Уведомления
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="block-number-message">
		  								<span>
		  									648
		  								</span>
		  							</div>
		  						</div>
		  						<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu message">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои сообщения <span>(9)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подмигиваний <span>(315)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Просмотры <span>(49)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Избранные <span>(5)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подарки <span>(3)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Симпатии <span>(5)</span>
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">
		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Блог
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Карусель
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->
		  					
		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							ТОП 100
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Поиск
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Идеальная пара
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  				</div>

		  				<!--  -->
	  					
	  				</div>
	  			</div>
	  		</div>
  		</div>

  		<!-- end-header -->

  		<!-- slider-under-header -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  			<!--  -->

	  			<div class="block-slider-under-header">
	  				<div class="all-slider-under-header">

	  					<div class="block-trap-photo">
	  						<span class="bold">
	  							Ловушка:
	  						</span>

	  						<span class="regular">
	  							Попасть<br>
	  							в ленту
	  						</span>
	  					</div>

	  					<!--  -->

				        <div class="slider-display-under-header">
				            <div class="regular-under-header slider-under-header">
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/one-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/two-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/three-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/four-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/five-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
				        	</div>
				    	</div>

				    	<!--  -->

	  				</div>
	  			</div>

	  			<!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-slider-under-header -->

  		<!-- content -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<div class="content-gift-page">

	  					<!-- left-sidedar -->

	  					<div class="left-sidedar">
	  						
	  						<!--  -->

	  						<span class="header-left-sidebar">
	  							Лидер
	  						</span>

	  						<!--  -->

	  						<div class="block-one-of-banner">
	  							<div class="null-block">
	  								<div class="block-message-banner">
	  									<span>
	  										Сообщение
	  									</span>
	  								</div>	
	  							</div>

	  							<img src="img/one-image-banner.png" alt="not-image">	  							
	  						</div>

	  						<!--  -->

	  						<div class="block-coints-banner">
	  							<span>
	  								Заплати одну монету и попади в лидеры
	  							</span>
	  						</div>

	  						<!--  -->

	  					</div>

	  					<!-- end-left-sidedar -->

	  					<!-- center-content -->

	  					<div class="center-content-perfect-pair">
	  						<span class="header-content-perfect-pair">
		  						Поиск
		  					</span>

		  					<div class="content-perfect-pair">
		  						<div class="header-perfect-pair">
		  							<span class="btn-one-search active">
		  								Блондинки Днепр
		  							</span>

		  							<span class="btn-two-search">
		  								Поиск 2
		  							</span>

		  							<span class="btn-three-search">
		  								Поиск 3
		  							</span>
		  						</div>

		  						<!-- form-search -->
		  						<div class="left-block">
		  							<!-- registretion -->

				  					<form>
					  					
					  					<div class="all-form-search">

				  							<div class="block-i-am">
				  								<span class="i-am">
				  									Ищу
				  								</span>

				  								<div class="block-check-woman-man">
						  							<input id="check-icon-woman" type="checkbox" name="check-icon-woman">
				                      				<label class="check-icon-woman" for="check-icon-woman"></label>

				                      				<span class="text-check-woman">
				                      					жещину
				                      				</span>
						  						</div>

						  						<div class="block-check-woman-man">
						  							<input id="check-icon-man" type="checkbox" name="check-icon-man">
				                      				<label class="check-icon-man" for="check-icon-man"></label>

				                      				<span class="text-check-woman">
				                      					мужчину
				                      				</span>
						  						</div>
				  							</div>

				  							<!--  -->

				  							<!--  -->

				  							<div class="block-change-country">
				  								<span class="change-country">
				  									Имя/Ник:
				  								</span>

				  								<div class="bg-input-change-country">
				  									<input class="input-registration" placeholder="введите имя">
				  								</div>
				  							</div>

				  							<!--  -->

				  							<div class="block-date-registration">

				  								<span class="date-registration">
				  									Возраст:
				  								</span>

				  								<span class="regular">
				  									от
				  								</span>

				  								<!-- one-select -->
				  								
				  								<div class="block-select-search-one">
				  									<div class="select-search-one">
				  										<div id="search">
				  											23
				  										</div>

				  										<div class="btn-select-search">
				  											
				  										</div>
				  									</div>

				  									<div class="all-block-option-search">
														<div class="scroll-pane-search">
															<div class="content-select-search-one">
															    <div class="points-count-search focus">
															    	18
															    </div>

															    <div class="points-count-search focus">
															    	20
															    </div>

															    <div class="points-count-search focus">
															    	22
															    </div>

															    <div class="points-count-search focus">
															    	24
															    </div>

															    <div class="points-count-search focus">
															    	26
															    </div>

															    <div class="points-count-search focus">
															    	28
															    </div>

															    <div class="points-count-search focus">
															    	30
															    </div>

															    <div class="points-count-search focus">
															    	32
															    </div>

															    <div class="points-count-search focus">
															    	34
															    </div>

															    <div class="points-count-search focus">
															    	36
															    </div>

															    <div class="points-count-search focus">
															    	38
															    </div>

															    <div class="points-count-search focus">
															    	40
															    </div>

															    <div class="points-count-search focus">
															    	42
															    </div>
														    </div>
														</div>
													</div>
				  								</div>

				  								<!-- end-one-select -->

				  								<span class="regular">
				  									до
				  								</span>

				  								<!-- two-select -->
				  								
				  								<div class="block-select-search-two">
				  									<div class="select-search-two">
				  										<div id="search-two">
				  											44
				  										</div>

				  										<div class="btn-select-search-two">	  											
				  										</div>
				  									</div>

				  									<div class="all-block-option-search-two">
														<div class="scroll-pane-search-two">
															<div class="content-select-search-two">
															    <div class="points-count-search-two focus">
															    	40
															    </div>

															    <div class="points-count-search-two focus">
															    	42
															    </div>

															    <div class="points-count-search-two focus">
															    	44
															    </div>

															    <div class="points-count-search-two focus">
															    	46
															    </div>

															    <div class="points-count-search-two focus">
															    	48
															    </div>

															    <div class="points-count-search-two focus">
															    	50
															    </div>

															    <div class="points-count-search-two focus">
															    	52
															    </div>

															    <div class="points-count-search-two focus">
															    	54
															    </div>
														    </div>
														</div>
													</div>
				  								</div>
				  								
				  								<!-- end-two-select -->

				  								<!--  -->

					  							<div class="block-change-country-two">
					  								<span class="change-country">
					  									Из:
					  								</span>

					  								<div class="bg-input-change-country">
					  									<input class="input-registration" placeholder="страна/город">
					  								</div>
					  							</div>

					  							<!--  -->

				  								<!--  -->

					  							<div class="block-change-country-two right">
					  								<span class="change-country">
					  									Удаленность:
					  								</span>

					  								<span class="regular">
					  									от
					  								</span>

					  								<!-- three-select -->
					  								
					  								<div class="block-select-search-three">
					  									<div class="select-search-three">
					  										<div id="search-three">
					  											1 км
					  										</div>

					  										<div class="btn-select-search-three">	  											
					  										</div>
					  									</div>

					  									<div class="all-block-option-search-three">
															<div class="scroll-pane-search-three">
																<div class="content-select-search-three">
																    <div class="points-count-search-three focus">
																    	1 км
																    </div>

																    <div class="points-count-search-three focus">
																    	5 км
																    </div>

																    <div class="points-count-search-three focus">
																    	10 км
																    </div>

																    <div class="points-count-search-three focus">
																    	15 км
																    </div>

																    <div class="points-count-search-three focus">
																    	20 км
																    </div>

																    <div class="points-count-search-three focus">
																    	25 км
																    </div>

																    <div class="points-count-search-three focus">
																    	30 км
																    </div>

																    <div class="points-count-search-three focus">
																    	35 км
																    </div>
															    </div>
															</div>
														</div>
					  								</div>
					  								
					  								<!-- end-three-select -->

					  								<span class="regular">
					  									до
					  								</span>

					  								<!-- four-select -->
					  								
					  								<div class="block-select-search-four">
					  									<div class="select-search-four">
					  										<div id="search-four">
					  											50 км
					  										</div>

					  										<div class="btn-select-search-four">	  											
					  										</div>
					  									</div>

					  									<div class="all-block-option-search-four">
															<div class="scroll-pane-search-four">
																<div class="content-select-search-four">
																    <div class="points-count-search-four focus">
																    	50 км
																    </div>

																    <div class="points-count-search-four focus">
																    	75 км
																    </div>

																    <div class="points-count-search-four focus">
																    	100 км
																    </div>

																    <div class="points-count-search-four focus">
																    	125 км
																    </div>

																    <div class="points-count-search-four focus">
																    	150 км
																    </div>

																    <div class="points-count-search-four focus">
																    	175 км
																    </div>

																    <div class="points-count-search-four focus">
																    	200 км
																    </div>

																    <div class="points-count-search-four focus">
																    	400 км
																    </div>
															    </div>
															</div>
														</div>
					  								</div>
					  								
					  								<!-- end-four-select -->
					  							</div>

					  							<!--  -->

				  							</div>

				  							<!--  -->

				  							<!--  -->
				  							<div class="block-date-registration">

				  								<span class="date-registration">
				  									Семейное положение:
				  								</span>

				  								<!-- five-select -->
					  								
					  							<div class="block-select-search-five">
					  								<div class="select-search-five">
				  										<div id="search-five">
				  											Не важно
				  										</div>

				  										<div class="btn-select-search-five">	  											
														</div>
					  								</div>

					  								<div class="all-block-option-search-five">
														<div class="scroll-pane-search-five">
															<div class="content-select-search-five">
															    <div class="points-count-search-five focus">
															    	Не важно
															    </div>

															    <div class="points-count-search-five focus">
															    	Женат/Замужем
															    </div>

															    <div class="points-count-search-five focus">
																   	В отношениях
																</div>

															    <div class="points-count-search-five focus">
															    	В поисках
															    </div>
														    </div>
														</div>
													</div>
					  							</div>
					  								
					  							<!-- end-five-select -->

				  								<!--  -->

					  							<div class="block-change-country-two right">
					  								<span class="change-country">
					  									Ориентация:
					  								</span>

					  								<!-- six-select -->
					  								
					  								<div class="block-select-search-six">
					  									<div class="select-search-six">
					  										<div id="search-six">
					  											Не хочу отвечать
					  										</div>

					  										<div class="btn-select-search-six">	  											
					  										</div>
					  									</div>

					  									<div class="all-block-option-search-six">
															<div class="scroll-pane-search-six">
																<div class="content-select-search-six">
																    <div class="points-count-search-six focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-six focus">
																    	Не хочу отвечать
																    </div>

																    <div class="points-count-search-six focus">
																    	Гетеро
																    </div>

																    <div class="points-count-search-six focus">
																    	Би
																    </div>
															    </div>
															</div>
														</div>
					  								</div>
					  								
					  								<!-- end-six-select -->
					  								
					  							</div>

					  							<!--  -->

				  							</div>
				  							<!--  -->

				  							<!--  -->
				  							<div class="block-date-registration two">

				  								<span class="date-registration">
				  									Цель знакомства:
				  								</span>

				  								<!-- seven-select -->
					  								
					  							<div class="block-select-search-seven">
					  								<div class="select-search-seven">
				  										<div id="search-seven">
				  											Романтические отношения/Свидания
				  										</div>

				  										<div class="btn-select-search-seven">	  											
														</div>
					  								</div>

					  								<div class="all-block-option-search-seven">
														<div class="scroll-pane-search-seven">
															<div class="content-select-search-seven">
															    <div class="points-count-search-seven focus">
															    	Не важно
															    </div>

															    <div class="points-count-search-seven focus">
															    	Друзья по переписке, общение
															    </div>

															    <div class="points-count-search-seven focus">
																   	Романтические отношения/Свидания
																</div>

															    <div class="points-count-search-seven focus">
															    	Брак
															    </div>

															    <div class="points-count-search-seven focus">
															    	Рождение детей
															    </div>

															    <div class="points-count-search-seven focus">
																   	Путешествия
																</div>

															    <div class="points-count-search-seven focus">
															    	Совместный досуг
															    </div>
														    </div>
														</div>
													</div>
					  							</div>
					  								
					  							<!-- end-seven-select -->

				  								<!--  -->

					  							<div class="block-change-country-two right">
					  								<span class="btn-find-search">
					  									Найти
					  								</span>
					  							</div>

					  							<!--  -->

				  							</div>
				  							<!--  -->			  							
				  						
				  						</div>

				  						<!--  -->

				  						<!-- block-more-search -->

				  						<!--  -->
				  						<div class="block-btn-more-search">
				  							<div class="btn-more-search">
				  								<span class="text-btn-more-search">
				  									Расширенный поиск
				  								</span>
				  							</div>
				  						</div>
				  						<!--  -->

				  						<!--  -->

				  						<div class="null-block">

					  						<div class="block-show-more-search">
					  							<!-- left-block-show-more-search -->
					  							<div class="left-block-show-more-search">
					  								<span class="header-left-block-show-more-search">
					  									Внешность
					  								</span>

					  								<!-- eight-select -->					  								
						  							<div class="block-select-search-eight">
						  								<div class="select-search-eight">
					  										<div id="search-eight">
					  											Телосложение
					  										</div>

					  										<div class="btn-select-search-eight">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-eight">
															<div class="scroll-pane-search-eight">
																<div class="content-select-search-eight">
																    <div class="points-count-search-eight focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-eight focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-eight focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-eight focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-eight-select -->

						  							<!-- nine-select -->					  								
						  							<div class="block-select-search-nine">
						  								<div class="select-search-nine">
					  										<div id="search-nine">
					  											Этническая принадлежность
					  										</div>

					  										<div class="btn-select-search-nine">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-nine">
															<div class="scroll-pane-search-nine">
																<div class="content-select-search-nine">
																    <div class="points-count-search-nine focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-nine focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-nine focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-nine focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-nine-select -->

						  							<!-- ten-select -->					  								
						  							<div class="block-select-search-ten">
						  								<div class="select-search-ten">
					  										<div id="search-ten">
					  											Цвет волос
					  										</div>

					  										<div class="btn-select-search-ten">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-ten">
															<div class="scroll-pane-search-ten">
																<div class="content-select-search-ten">
																    <div class="points-count-search-ten focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-ten focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-ten focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-ten focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-ten-select -->

						  							<!-- elewen-select -->					  								
						  							<div class="block-select-search-elewen">
						  								<div class="select-search-elewen">
					  										<div id="search-elewen">
					  											Длина волос
					  										</div>

					  										<div class="btn-select-search-elewen">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-elewen">
															<div class="scroll-pane-search-elewen">
																<div class="content-select-search-elewen">
																    <div class="points-count-search-elewen focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-elewen focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-elewen focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-elewen focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-elewen-select -->

						  							<!-- tvelwe-select -->					  								
						  							<div class="block-select-search-tvelwe">
						  								<div class="select-search-tvelwe">
					  										<div id="search-tvelwe">
					  											Тип волос
					  										</div>

					  										<div class="btn-select-search-tvelwe">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-tvelwe">
															<div class="scroll-pane-search-tvelwe">
																<div class="content-select-search-tvelwe">
																    <div class="points-count-search-tvelwe focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-tvelwe focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-tvelwe focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-tvelwe focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-tvelwe-select -->

						  							<!-- thirting-select -->					  								
						  							<div class="block-select-search-thirting">
						  								<div class="select-search-thirting">
					  										<div id="search-thirting">
					  											Цвет глаз
					  										</div>

					  										<div class="btn-select-search-thirting">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-thirting">
															<div class="scroll-pane-search-thirting">
																<div class="content-select-search-thirting">
																    <div class="points-count-search-thirting focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-thirting focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-thirting focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-thirting focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-thirting-select -->

						  							<!-- fourting-select -->					  								
						  							<div class="block-select-search-fourting">
						  								<div class="select-search-fourting">
					  										<div id="search-fourting">
					  											Самое привлекательное
					  										</div>

					  										<div class="btn-select-search-fourting">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-fourting">
															<div class="scroll-pane-search-fourting">
																<div class="content-select-search-fourting">
																    <div class="points-count-search-fourting focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-fourting focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-fourting focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-fourting focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-fourting-select -->

						  							<!-- fiveting-select -->					  								
						  							<div class="block-select-search-fiveting">
						  								<div class="select-search-fiveting">
					  										<div id="search-fiveting">
					  											Боди-арт
					  										</div>

					  										<div class="btn-select-search-fiveting">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-fiveting">
															<div class="scroll-pane-search-fiveting">
																<div class="content-select-search-fiveting">
																    <div class="points-count-search-fiveting focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-fiveting focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-fiveting focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-fiveting focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-fiveting-select -->

					  							</div>
					  							<!-- end-left-block-show-more-search -->

					  							<!-- center-block-show-more-search -->
					  							<div class="center-block-show-more-search">
					  								<span class="header-left-block-show-more-search">
					  									Образ жизни
					  								</span>

					  								<!-- sixting-select -->					  								
						  							<div class="block-select-search-sixting">
						  								<div class="select-search-sixting">
					  										<div id="search-sixting">
					  											Отношение к курению
					  										</div>

					  										<div class="btn-select-search-sixting">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-sixting">
															<div class="scroll-pane-search-sixting">
																<div class="content-select-search-sixting">
																    <div class="points-count-search-sixting focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-sixting focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-sixting focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-sixting focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-sixting-select -->

						  							<!-- seventing-select -->					  								
						  							<div class="block-select-search-seventing">
						  								<div class="select-search-seventing">
					  										<div id="search-seventing">
					  											Отношение к алкоголю
					  										</div>

					  										<div class="btn-select-search-seventing">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-seventing">
															<div class="scroll-pane-search-seventing">
																<div class="content-select-search-seventing">
																    <div class="points-count-search-seventing focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-seventing focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-seventing focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-seventing focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-seventing-select -->

						  							<!-- eighting-select -->					  								
						  							<div class="block-select-search-eighting">
						  								<div class="select-search-eighting">
					  										<div id="search-eighting">
					  											Смена места жительства на
					  										</div>

					  										<div class="btn-select-search-eighting">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-eighting">
															<div class="scroll-pane-search-eighting">
																<div class="content-select-search-eighting">
																    <div class="points-count-search-eighting focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-eighting focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-eighting focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-eighting focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-eighting-select -->

						  							<!-- nineting-select -->					  								
						  							<div class="block-select-search-nineting">
						  								<div class="select-search-nineting">
					  										<div id="search-nineting">
					  											Есть дети
					  										</div>

					  										<div class="btn-select-search-nineting">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-nineting">
															<div class="scroll-pane-search-nineting">
																<div class="content-select-search-nineting">
																    <div class="points-count-search-nineting focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-nineting focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-nineting focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-nineting focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-nineting-select -->

						  							<!-- twenty-select -->					  								
						  							<div class="block-select-search-twenty">
						  								<div class="select-search-twenty">
					  										<div id="search-twenty">
					  											Домашние животные
					  										</div>

					  										<div class="btn-select-search-twenty">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty">
															<div class="scroll-pane-search-twenty">
																<div class="content-select-search-twenty">
																    <div class="points-count-search-twenty focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-select -->

						  							<!-- twenty-one-select -->					  								
						  							<div class="block-select-search-twenty-one">
						  								<div class="select-search-twenty-one">
					  										<div id="search-twenty-one">
					  											Специальность
					  										</div>

					  										<div class="btn-select-search-twenty-one">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty-one">
															<div class="scroll-pane-search-twenty-one">
																<div class="content-select-search-twenty-one">
																    <div class="points-count-search-twenty-one focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty-one focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty-one focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty-one focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-one-select -->

						  							<!-- twenty-two-select -->					  								
						  							<div class="block-select-search-twenty-two">
						  								<div class="select-search-twenty-two">
					  										<div id="search-twenty-two">
					  											Робочий статус
					  										</div>

					  										<div class="btn-select-search-twenty-two">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty-two">
															<div class="scroll-pane-search-twenty-two">
																<div class="content-select-search-twenty-two">
																    <div class="points-count-search-twenty-two focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty-two focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty-two focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty-two focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-two-select -->

						  							<!-- twenty-three-select -->					  								
						  							<div class="block-select-search-twenty-three">
						  								<div class="select-search-twenty-three">
					  										<div id="search-twenty-three">
					  											Ежигодный доход
					  										</div>

					  										<div class="btn-select-search-twenty-three">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty-three">
															<div class="scroll-pane-search-twenty-three">
																<div class="content-select-search-twenty-three">
																    <div class="points-count-search-twenty-three focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty-three focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty-three focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty-three focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-three-select -->

						  							<!-- twenty-four-select -->					  								
						  							<div class="block-select-search-twenty-four">
						  								<div class="select-search-twenty-four">
					  										<div id="search-twenty-four">
					  											Ситуация с жильем
					  										</div>

					  										<div class="btn-select-search-twenty-four">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty-four">
															<div class="scroll-pane-search-twenty-four">
																<div class="content-select-search-twenty-four">
																    <div class="points-count-search-twenty-four focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty-four focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty-four focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty-four focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-four-select -->

						  							<!-- twenty-five-select -->					  								
						  							<div class="block-select-search-twenty-five">
						  								<div class="select-search-twenty-five">
					  										<div id="search-twenty-five">
					  											Этническая принадлежность
					  										</div>

					  										<div class="btn-select-search-twenty-five">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty-five">
															<div class="scroll-pane-search-twenty-five">
																<div class="content-select-search-twenty-five">
																    <div class="points-count-search-twenty-five focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty-five focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty-five focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty-five focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-five-select -->

						  							<!-- twenty-six-select -->					  								
						  							<div class="block-select-search-twenty-six">
						  								<div class="select-search-twenty-six">
					  										<div id="search-twenty-six">
					  											Основной язык
					  										</div>

					  										<div class="btn-select-search-twenty-six">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty-six">
															<div class="scroll-pane-search-twenty-six">
																<div class="content-select-search-twenty-six">
																    <div class="points-count-search-twenty-six focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty-six focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty-six focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty-six focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-six-select -->

					  							</div>
					  							<!-- end-center-block-show-more-search -->

					  							<!-- left-block-show-more-search -->
					  							<div class="left-block-show-more-search">
					  								<span class="header-left-block-show-more-search">
					  									Личность
					  								</span>

					  								<!-- twenty-seven-select -->					  								
						  							<div class="block-select-search-twenty-seven">
						  								<div class="select-search-twenty-seven">
					  										<div id="search-twenty-seven">
					  											Образование
					  										</div>

					  										<div class="btn-select-search-twenty-seven">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty-seven">
															<div class="scroll-pane-search-twenty-seven">
																<div class="content-select-search-twenty-seven">
																    <div class="points-count-search-twenty-seven focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty-seven focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty-seven focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty-seven focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-seven-select -->

						  							<!-- twenty-eight-select -->					  								
						  							<div class="block-select-search-twenty-eight">
						  								<div class="select-search-twenty-eight">
					  										<div id="search-twenty-eight">
					  											Религия
					  										</div>

					  										<div class="btn-select-search-twenty-eight">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty-eight">
															<div class="scroll-pane-search-twenty-eight">
																<div class="content-select-search-twenty-eight">
																    <div class="points-count-search-twenty-eight focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty-eight focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty-eight focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty-eight focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-eight-select -->

						  							<!-- twenty-nine-select -->					  								
						  							<div class="block-select-search-twenty-nine">
						  								<div class="select-search-twenty-nine">
					  										<div id="search-twenty-nine">
					  											Какую музыку предпочитает
					  										</div>

					  										<div class="btn-select-search-twenty-nine">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-twenty-nine">
															<div class="scroll-pane-search-twenty-nine">
																<div class="content-select-search-twenty-nine">
																    <div class="points-count-search-twenty-nine focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-twenty-nine focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-twenty-nine focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-twenty-nine focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-twenty-nine-select -->

						  							<!-- thirty-select -->					  								
						  							<div class="block-select-search-thirty">
						  								<div class="select-search-thirty">
					  										<div id="search-thirty">
					  											Знак зодиака
					  										</div>

					  										<div class="btn-select-search-thirty">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-thirty">
															<div class="scroll-pane-search-thirty">
																<div class="content-select-search-thirty">
																    <div class="points-count-search-thirty focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-thirty focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-thirty focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-thirty focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-thirty-select -->

						  							<!-- thirty-one-select -->					  								
						  							<div class="block-select-search-thirty-one">
						  								<div class="select-search-thirty-one">
					  										<div id="search-thirty-one">
					  											Знак зодиака
					  										</div>

					  										<div class="btn-select-search-thirty-one">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-thirty-one">
															<div class="scroll-pane-search-thirty-one">
																<div class="content-select-search-thirty-one">
																    <div class="points-count-search-thirty-one focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-thirty-one focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-thirty-one focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-thirty-one focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-thirty-one-select -->

						  							<!-- thirty-two-select -->					  								
						  							<div class="block-select-search-thirty-two">
						  								<div class="select-search-thirty-two">
					  										<div id="search-thirty-two">
					  											Какую кухню предпочитает
					  										</div>

					  										<div class="btn-select-search-thirty-two">	  											
															</div>
						  								</div>

						  								<div class="all-block-option-search-thirty-two">
															<div class="scroll-pane-search-thirty-two">
																<div class="content-select-search-thirty-two">
																    <div class="points-count-search-thirty-two focus">
																    	Не важно
																    </div>

																    <div class="points-count-search-thirty-two focus">
																    	текст 1
																    </div>

																    <div class="points-count-search-thirty-two focus">
																	   	текст 2
																	</div>

																    <div class="points-count-search-thirty-two focus">
																    	текст 3
																    </div>
															    </div>
															</div>
														</div>
						  							</div>					  								
						  							<!-- end-thirty-two-select -->
					  							</div>
					  							<!-- end-left-block-show-more-search -->

					  							<!-- under-block-more-search -->

						  						<div class="under-block-more-search">
						  							<span class="fade-out-block-more-search">
						  								Свернуть
						  							</span>

						  							<div class="block-check-more-search">
					                      				<span class="text-check-econom">
					                      					Сохранить как:
					                      				</span>

							  							<input id="check-econom" type="checkbox" name="check-econom">
					                      				<label class="check-econom" for="check-econom"></label>
							  						</div>

							  						<div class="block-bg-input-more-search">
							  							<input type="text" class="input-more-search" placeholder="введите название поиска">
							  						</div>

							  						<span class="bg-btn-find-more-search">
							  							Найти
							  						</span>

							  						<span class="bg-btn-cancel-more-search">
							  							Отменить
							  						</span>
						  						</div>

						  						<!-- end-under-block-more-search -->
					  						</div>

				  						</div>

				  						<!-- end-block-more-search -->

				  					</form>

				  					<!-- end-registretion -->
		  						</div>
		  						<!-- end-form-search -->

		  						<!--  -->

		  						<div class="left-block">

		  							<!-- one-line-result -->
			  						<!--  -->
									<div class="one-of-like-user">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/one-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Инна, 26 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/two-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Виктория, 25 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/three-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Ангелина, 23 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/four-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Татьяна, 25 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->
									<!-- end-one-line-result -->

									<!-- one-line-result -->
			  						<!--  -->
									<div class="one-of-like-user">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/one-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Инна, 26 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/two-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Виктория, 25 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/three-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Ангелина, 23 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/four-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Татьяна, 25 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->
									<!-- end-one-line-result -->

									<!-- one-line-result -->
			  						<!--  -->
									<div class="one-of-like-user">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/one-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Инна, 26 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/two-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Виктория, 25 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/three-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Ангелина, 23 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/four-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Татьяна, 25 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->
									<!-- end-one-line-result -->

									<!-- one-line-result -->
			  						<!--  -->
									<div class="one-of-like-user">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/one-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Инна, 26 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/two-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Виктория, 25 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/three-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Ангелина, 23 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->

									<!--  -->
									<div class="one-of-like-user right">
								    	<div class="left-block">
								    		<div class="null-block">
											    <div class="block-hover-like-user">
											    	<div class="around-gift">									                					
											    	</div>

											    	<div class="block-icon-hover-like-user">
											    		<div class="icon-send-message">									                						
											    		</div>

											    		<div class="icon-like-user">									                						
											    		</div>

											    		<div class="icon-add-favorite">									                						
											    		</div>

											    		<div class="icon-on-line">									                						
											    		</div>
											    	</div>
											    </div>
									   		</div>

									   		<div class="block-photo-one-of-like-user">
									   			<img src="img/four-image-like-user.png" alt="not-image">
									   		</div>

									   		<span class="bold">
									   			Татьяна, 25 лет
									   		</span>

									   		<span class="regular">
									   			Украина, г.Днепр
									   		</span>
										</div>								                		
									</div>
									<!--  -->
									<!-- end-one-line-result -->

								</div>
								<!--  -->

								<div class="block-btn-show-more-search">
									<div class="btn-show-more-search">
										<span>
											Показать больше
										</span>
									</div>
								</div>

		  					</div>
	  					</div>

	  					<!-- end-center-content -->

	  					<!-- right-sidebar -->

	  					<!-- end-right-sidebar -->
	  					
	  				</div>

	  			</div>
	  		</div>
  		</div>

  		<!-- end-content -->

  		<!-- footer-display -->

  		<div class="footer-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

		  			<div class="footer-menu">

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					О нас
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Условия и правила
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Конфиденциальность
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Партнерам
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Отзывы
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Связаться с Нами
		  				</span>

		  				<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="block-soc-footer">

		  				<!--  -->

		  				<span class="header-soc-footer">
		  					Присоединяйся к <span>For two dating:</span>
		  				</span>

		  				<!--  -->

		  				<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-vk">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-face">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-twit">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-inst">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-yt">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-gp">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="footer-all-right">
		  				<span class="header-soc-footer">
		  					© 2016- 2016 For2date. Все права защищены. Разработка I-PR
		  				</span>
		  			</div>

		  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-footer-display -->

  	</div>

  	<!-- script -->

  	<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

  	<script type="text/javascript">
        $(".regular-first").slick({
          	dots: false,
          	autoplay: true,
           	prevArrow: false,
            nextArrow: false,
            centerMode: true,
          	infinite: true,
          	slidesToShow: 1,
          	slidesToScroll: 1,
          	autoplaySpeed: 10000
        });

        $(".regular-two").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });

        $(".regular-gift-for-user").slick({
          infinite: true,
          // centerMode: true,
          slidesToShow: 3,
          slidesToScroll: 3
        });
        
    </script>

    <!-- =============================================================== -->

    <script src="js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
	    var config = {
	      '.chosen-select'           : {},
	      '.chosen-select-deselect'  : {allow_single_deselect:true},
	      '.chosen-select-no-single' : {disable_search_threshold:1},
	      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	      '.chosen-select-width'     : {width:"95%"}
	    }
	    for (var selector in config) {
	      $(selector).chosen(config[selector]);
	    }
	</script>

	<!-- =============================================================== -->

	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>

	<script type="text/javascript" src="js/jquery.jscrollpane.js"></script>

	<script type="text/javascript">

		$('.one-of-menu.message').click(function () {
            $('.all-block-dropdown-menu.message').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.message').length ) 
            	return;
          	$('.all-block-dropdown-menu.message').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.message').mouseleave (function() {
          $('.all-block-dropdown-menu.message').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

        $('.one-of-menu.office').click(function () {
            $('.all-block-dropdown-menu.office').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.office').length ) 
            	return;
          	$('.all-block-dropdown-menu.office').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.office').mouseleave (function() {
          $('.all-block-dropdown-menu.office').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

		$('.btn-login').click(function () {
            $('.block-content-registretion').fadeOut(0);
            $('.block-content-login').fadeIn(0);
            $('.btn-login').addClass('active');
            $('.btn-registration').removeClass('active');
        });

        $('.btn-registration').click(function () {
            $('.block-content-login').fadeOut(0);
            $('.block-content-registretion').fadeIn(0);
            $('.btn-registration').addClass('active');
            $('.btn-login').removeClass('active');
        });

        // //////////////////////////////////////////////////////////

        $('.into-gift-center-content').find(".one-of-tab-content").click(function () {
		  	$('.one-of-tab-content').removeClass('active');
	        $(this).addClass('active');
		});

		$('.one-of-tab-content.all-gift').click(function () {
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.all-gift').fadeIn(200);
        });

		$('.one-of-tab-content.new-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeIn(200);
            $(".regular-gift-for-user-two").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        $('.one-of-tab-content.my-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeIn(200);
            $(".regular-gift-for-user-three").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        // //////////////////////////////////////////////////////////

		$('.select-registration-one').click(function () {
            $('.all-block-option').fadeIn(200);
            $('.scroll-pane').jScrollPane({showArrows: true});
            $('.btn-select').addClass('active');
        });

		$('.points-count.focus').click(function () {
            $('.all-block-option').fadeOut(200);
            $('.btn-select').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-one').length ) 
            	return;
          	$('.btn-select').removeClass('active');
          	$('.all-block-option').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-one').find(".points-count").click(function () {
		  	$('.points-count').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count.focus').click(function () {
          	var total = document.querySelector('.content-select-one .points-count.focus').innerHTML;
          	document.getElementById('total').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-two').click(function () {
            $('.all-block-option-two').fadeIn(200);
            $('.scroll-pane-two').jScrollPane({showArrows: true});
            $('.btn-select-two').addClass('active');
        });

		$('.points-count-two.focus').click(function () {
            $('.all-block-option-two').fadeOut(200);
            $('.btn-select-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-two').length ) 
            	return;
          	$('.btn-select-two').removeClass('active');
          	$('.all-block-option-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-two').find(".points-count-two").click(function () {
		  	$('.points-count-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-two.focus').click(function () {
          	var total = document.querySelector('.content-select-two .points-count-two.focus').innerHTML;
          	document.getElementById('total-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-three').click(function () {
            $('.all-block-option-three').fadeIn(200);
            $('.scroll-pane-three').jScrollPane({showArrows: true});
            $('.btn-select-three').addClass('active');
        });

		$('.points-count-three.focus').click(function () {
            $('.all-block-option-three').fadeOut(200);
            $('.btn-select-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-three').length ) 
            	return;
          	$('.btn-select-three').removeClass('active');
          	$('.all-block-option-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-three').find(".points-count-three").click(function () {
		  	$('.points-count-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-three.focus').click(function () {
          	var total = document.querySelector('.content-select-three .points-count-three.focus').innerHTML;
          	document.getElementById('total-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-lang').click(function () {
            $('.all-block-option-lang').fadeIn(200);
            // $('.scroll-pane-lang').jScrollPane({showArrows: true});
            $('.btn-select-lang').addClass('active');
        });

		$('.points-count-lang.focus').click(function () {
            $('.all-block-option-lang').fadeOut(200);
            $('.btn-select-lang').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-lang').length ) 
            	return;
          	$('.btn-select-lang').removeClass('active');
          	$('.all-block-option-lang').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-lang').find(".points-count-lang").click(function () {
		  	$('.points-count-lang').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-lang.focus').click(function () {
          	var total = document.querySelector('.content-select-lang .points-count-lang.focus').innerHTML;
          	document.getElementById('total-lang').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-one').click(function () {
            $('.all-block-option-search').fadeIn(200);
            $('.scroll-pane-search').jScrollPane({showArrows: true});
            $('.btn-select-search').addClass('active');
        });

		$('.points-count-search.focus').click(function () {
            $('.all-block-option-search').fadeOut(200);
            $('.btn-select-search').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-one').length ) 
            	return;
          	$('.btn-select-search').removeClass('active');
          	$('.all-block-option-search').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-one').find(".points-count-search").click(function () {
		  	$('.points-count-search').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search.focus').click(function () {
          	var total = document.querySelector('.content-select-search-one .points-count-search.focus').innerHTML;
          	document.getElementById('search').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

         // //////////////////////////////////////////////////////////

		$('.select-search-two').click(function () {
            $('.all-block-option-search-two').fadeIn(200);
            $('.scroll-pane-search-two').jScrollPane({showArrows: true});
            $('.btn-select-search-two').addClass('active');
        });

		$('.points-count-search-two.focus').click(function () {
            $('.all-block-option-search-two').fadeOut(200);
            $('.btn-select-search-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-two').length ) 
            	return;
          	$('.btn-select-search-two').removeClass('active');
          	$('.all-block-option-search-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-two').find(".points-count-search-two").click(function () {
		  	$('.points-count-search-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-two.focus').click(function () {
          	var total = document.querySelector('.content-select-search-two .points-count-search-two.focus').innerHTML;
          	document.getElementById('search-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-three').click(function () {
            $('.all-block-option-search-three').fadeIn(200);
            $('.scroll-pane-search-three').jScrollPane({showArrows: true});
            $('.btn-select-search-three').addClass('active');
        });

		$('.points-count-search-three.focus').click(function () {
            $('.all-block-option-search-three').fadeOut(200);
            $('.btn-select-search-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-three').length ) 
            	return;
          	$('.btn-select-search-three').removeClass('active');
          	$('.all-block-option-search-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-three').find(".points-count-search-three").click(function () {
		  	$('.points-count-search-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-three.focus').click(function () {
          	var total = document.querySelector('.content-select-search-three .points-count-search-three.focus').innerHTML;
          	document.getElementById('search-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-four').click(function () {
            $('.all-block-option-search-four').fadeIn(200);
            $('.scroll-pane-search-four').jScrollPane({showArrows: true});
            $('.btn-select-search-four').addClass('active');
        });

		$('.points-count-search-four.focus').click(function () {
            $('.all-block-option-search-four').fadeOut(200);
            $('.btn-select-search-four').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-four').length ) 
            	return;
          	$('.btn-select-search-four').removeClass('active');
          	$('.all-block-option-search-four').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-four').find(".points-count-search-four").click(function () {
		  	$('.points-count-search-four').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-four.focus').click(function () {
          	var total = document.querySelector('.content-select-search-four .points-count-search-four.focus').innerHTML;
          	document.getElementById('search-four').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-six').click(function () {
            $('.all-block-option-search-six').fadeIn(200);
            $('.scroll-pane-search-six').jScrollPane({showArrows: true});
            $('.btn-select-search-six').addClass('active');
        });

		$('.points-count-search-six.focus').click(function () {
            $('.all-block-option-search-six').fadeOut(200);
            $('.btn-select-search-six').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-six').length ) 
            	return;
          	$('.btn-select-search-six').removeClass('active');
          	$('.all-block-option-search-six').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-six').find(".points-count-search-six").click(function () {
		  	$('.points-count-search-six').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-six.focus').click(function () {
          	var total = document.querySelector('.content-select-search-six .points-count-search-six.focus').innerHTML;
          	document.getElementById('search-six').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-five').click(function () {
            $('.all-block-option-search-five').fadeIn(200);
            $('.scroll-pane-search-five').jScrollPane({showArrows: true});
            $('.btn-select-search-five').addClass('active');
        });

		$('.points-count-search-five.focus').click(function () {
            $('.all-block-option-search-five').fadeOut(200);
            $('.btn-select-search-five').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-five').length ) 
            	return;
          	$('.btn-select-search-five').removeClass('active');
          	$('.all-block-option-search-five').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-five').find(".points-count-search-five").click(function () {
		  	$('.points-count-search-five').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-five.focus').click(function () {
          	var total = document.querySelector('.content-select-search-five .points-count-search-five.focus').innerHTML;
          	document.getElementById('search-five').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

         // //////////////////////////////////////////////////////////

		$('.select-search-seven').click(function () {
            $('.all-block-option-search-seven').fadeIn(200);
            $('.scroll-pane-search-seven').jScrollPane({showArrows: true});
            $('.btn-select-search-seven').addClass('active');
        });

		$('.points-count-search-seven.focus').click(function () {
            $('.all-block-option-search-seven').fadeOut(200);
            $('.btn-select-search-seven').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-seven').length ) 
            	return;
          	$('.btn-select-search-seven').removeClass('active');
          	$('.all-block-option-search-seven').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-seven').find(".points-count-search-seven").click(function () {
		  	$('.points-count-search-seven').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-seven.focus').click(function () {
          	var total = document.querySelector('.content-select-search-seven .points-count-search-seven.focus').innerHTML;
          	document.getElementById('search-seven').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////

		$('.select-search-eight').click(function () {
            $('.all-block-option-search-eight').fadeIn(200);
            $('.scroll-pane-search-eight').jScrollPane({showArrows: true});
            $('.btn-select-search-eight').addClass('active');
        });

		$('.points-count-search-eight.focus').click(function () {
            $('.all-block-option-search-eight').fadeOut(200);
            $('.btn-select-search-eight').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-eight').length ) 
            	return;
          	$('.btn-select-search-eight').removeClass('active');
          	$('.all-block-option-search-eight').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-eight').find(".points-count-search-eight").click(function () {
		  	$('.points-count-search-eight').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-eight.focus').click(function () {
          	var total = document.querySelector('.content-select-search-eight .points-count-search-eight.focus').innerHTML;
          	document.getElementById('search-eight').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-nine').click(function () {
            $('.all-block-option-search-nine').fadeIn(200);
            $('.scroll-pane-search-nine').jScrollPane({showArrows: true});
            $('.btn-select-search-nine').addClass('active');
        });

		$('.points-count-search-nine.focus').click(function () {
            $('.all-block-option-search-nine').fadeOut(200);
            $('.btn-select-search-nine').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-nine').length ) 
            	return;
          	$('.btn-select-search-nine').removeClass('active');
          	$('.all-block-option-search-nine').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-nine').find(".points-count-search-nine").click(function () {
		  	$('.points-count-search-nine').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-nine.focus').click(function () {
          	var total = document.querySelector('.content-select-search-nine .points-count-search-nine.focus').innerHTML;
          	document.getElementById('search-nine').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-ten').click(function () {
            $('.all-block-option-search-ten').fadeIn(200);
            $('.scroll-pane-search-ten').jScrollPane({showArrows: true});
            $('.btn-select-search-ten').addClass('active');
        });

		$('.points-count-search-ten.focus').click(function () {
            $('.all-block-option-search-ten').fadeOut(200);
            $('.btn-select-search-ten').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-ten').length ) 
            	return;
          	$('.btn-select-search-ten').removeClass('active');
          	$('.all-block-option-search-ten').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-ten').find(".points-count-search-ten").click(function () {
		  	$('.points-count-search-ten').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-ten.focus').click(function () {
          	var total = document.querySelector('.content-select-search-ten .points-count-search-ten.focus').innerHTML;
          	document.getElementById('search-ten').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-elewen').click(function () {
            $('.all-block-option-search-elewen').fadeIn(200);
            $('.scroll-pane-search-elewen').jScrollPane({showArrows: true});
            $('.btn-select-search-elewen').addClass('active');
        });

		$('.points-count-search-elewen.focus').click(function () {
            $('.all-block-option-search-elewen').fadeOut(200);
            $('.btn-select-search-elewen').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-elewen').length ) 
            	return;
          	$('.btn-select-search-elewen').removeClass('active');
          	$('.all-block-option-search-elewen').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-elewen').find(".points-count-search-elewen").click(function () {
		  	$('.points-count-search-elewen').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-elewen.focus').click(function () {
          	var total = document.querySelector('.content-select-search-elewen .points-count-search-elewen.focus').innerHTML;
          	document.getElementById('search-elewen').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-tvelwe').click(function () {
            $('.all-block-option-search-tvelwe').fadeIn(200);
            $('.scroll-pane-search-tvelwe').jScrollPane({showArrows: true});
            $('.btn-select-search-tvelwe').addClass('active');
        });

		$('.points-count-search-tvelwe.focus').click(function () {
            $('.all-block-option-search-tvelwe').fadeOut(200);
            $('.btn-select-search-tvelwe').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-tvelwe').length ) 
            	return;
          	$('.btn-select-search-tvelwe').removeClass('active');
          	$('.all-block-option-search-tvelwe').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-tvelwe').find(".points-count-search-tvelwe").click(function () {
		  	$('.points-count-search-tvelwe').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-tvelwe.focus').click(function () {
          	var total = document.querySelector('.content-select-search-tvelwe .points-count-search-tvelwe.focus').innerHTML;
          	document.getElementById('search-tvelwe').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-thirting').click(function () {
            $('.all-block-option-search-thirting').fadeIn(200);
            $('.scroll-pane-search-thirting').jScrollPane({showArrows: true});
            $('.btn-select-search-thirting').addClass('active');
        });

		$('.points-count-search-thirting.focus').click(function () {
            $('.all-block-option-search-thirting').fadeOut(200);
            $('.btn-select-search-thirting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-thirting').length ) 
            	return;
          	$('.btn-select-search-thirting').removeClass('active');
          	$('.all-block-option-search-thirting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-thirting').find(".points-count-search-thirting").click(function () {
		  	$('.points-count-search-thirting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-thirting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-thirting .points-count-search-thirting.focus').innerHTML;
          	document.getElementById('search-thirting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-fourting').click(function () {
            $('.all-block-option-search-fourting').fadeIn(200);
            $('.scroll-pane-search-fourting').jScrollPane({showArrows: true});
            $('.btn-select-search-fourting').addClass('active');
        });

		$('.points-count-search-fourting.focus').click(function () {
            $('.all-block-option-search-fourting').fadeOut(200);
            $('.btn-select-search-fourting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-fourting').length ) 
            	return;
          	$('.btn-select-search-fourting').removeClass('active');
          	$('.all-block-option-search-fourting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-fourting').find(".points-count-search-fourting").click(function () {
		  	$('.points-count-search-fourting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-fourting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-fourting .points-count-search-fourting.focus').innerHTML;
          	document.getElementById('search-fourting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-fiveting').click(function () {
            $('.all-block-option-search-fiveting').fadeIn(200);
            $('.scroll-pane-search-fiveting').jScrollPane({showArrows: true});
            $('.btn-select-search-fiveting').addClass('active');
        });

		$('.points-count-search-fiveting.focus').click(function () {
            $('.all-block-option-search-fiveting').fadeOut(200);
            $('.btn-select-search-fiveting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-fiveting').length ) 
            	return;
          	$('.btn-select-search-fiveting').removeClass('active');
          	$('.all-block-option-search-fiveting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-fiveting').find(".points-count-search-fiveting").click(function () {
		  	$('.points-count-search-fiveting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-fiveting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-fiveting .points-count-search-fiveting.focus').innerHTML;
          	document.getElementById('search-fiveting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////

        $('.select-search-sixting').click(function () {
            $('.all-block-option-search-sixting').fadeIn(200);
            $('.scroll-pane-search-sixting').jScrollPane({showArrows: true});
            $('.btn-select-search-sixting').addClass('active');
        });

		$('.points-count-search-sixting.focus').click(function () {
            $('.all-block-option-search-sixting').fadeOut(200);
            $('.btn-select-search-sixting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-sixting').length ) 
            	return;
          	$('.btn-select-search-sixting').removeClass('active');
          	$('.all-block-option-search-sixting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-sixting').find(".points-count-search-sixting").click(function () {
		  	$('.points-count-search-sixting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-sixting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-sixting .points-count-search-sixting.focus').innerHTML;
          	document.getElementById('search-sixting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-seventing').click(function () {
            $('.all-block-option-search-seventing').fadeIn(200);
            $('.scroll-pane-search-seventing').jScrollPane({showArrows: true});
            $('.btn-select-search-seventing').addClass('active');
        });

		$('.points-count-search-seventing.focus').click(function () {
            $('.all-block-option-search-seventing').fadeOut(200);
            $('.btn-select-search-seventing').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-seventing').length ) 
            	return;
          	$('.btn-select-search-seventing').removeClass('active');
          	$('.all-block-option-search-seventing').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-seventing').find(".points-count-search-seventing").click(function () {
		  	$('.points-count-search-seventing').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-seventing.focus').click(function () {
          	var total = document.querySelector('.content-select-search-seventing .points-count-search-seventing.focus').innerHTML;
          	document.getElementById('search-seventing').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-eighting').click(function () {
            $('.all-block-option-search-eighting').fadeIn(200);
            $('.scroll-pane-search-eighting').jScrollPane({showArrows: true});
            $('.btn-select-search-eighting').addClass('active');
        });

		$('.points-count-search-eighting.focus').click(function () {
            $('.all-block-option-search-eighting').fadeOut(200);
            $('.btn-select-search-eighting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-eighting').length ) 
            	return;
          	$('.btn-select-search-eighting').removeClass('active');
          	$('.all-block-option-search-eighting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-eighting').find(".points-count-search-eighting").click(function () {
		  	$('.points-count-search-eighting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-eighting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-eighting .points-count-search-eighting.focus').innerHTML;
          	document.getElementById('search-eighting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-nineting').click(function () {
            $('.all-block-option-search-nineting').fadeIn(200);
            $('.scroll-pane-search-nineting').jScrollPane({showArrows: true});
            $('.btn-select-search-nineting').addClass('active');
        });

		$('.points-count-search-nineting.focus').click(function () {
            $('.all-block-option-search-nineting').fadeOut(200);
            $('.btn-select-search-nineting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-nineting').length ) 
            	return;
          	$('.btn-select-search-nineting').removeClass('active');
          	$('.all-block-option-search-nineting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-nineting').find(".points-count-search-nineting").click(function () {
		  	$('.points-count-search-nineting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-nineting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-nineting .points-count-search-nineting.focus').innerHTML;
          	document.getElementById('search-nineting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty').click(function () {
            $('.all-block-option-search-twenty').fadeIn(200);
            $('.scroll-pane-search-twenty').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty').addClass('active');
        });

		$('.points-count-search-twenty.focus').click(function () {
            $('.all-block-option-search-twenty').fadeOut(200);
            $('.btn-select-search-twenty').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty').length ) 
            	return;
          	$('.btn-select-search-twenty').removeClass('active');
          	$('.all-block-option-search-twenty').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty').find(".points-count-search-twenty").click(function () {
		  	$('.points-count-search-twenty').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty .points-count-search-twenty.focus').innerHTML;
          	document.getElementById('search-twenty').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-one').click(function () {
            $('.all-block-option-search-twenty-one').fadeIn(200);
            $('.scroll-pane-search-twenty-one').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-one').addClass('active');
        });

		$('.points-count-search-twenty-one.focus').click(function () {
            $('.all-block-option-search-twenty-one').fadeOut(200);
            $('.btn-select-search-twenty-one').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-one').length ) 
            	return;
          	$('.btn-select-search-twenty-one').removeClass('active');
          	$('.all-block-option-search-twenty-one').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-one').find(".points-count-search-twenty-one").click(function () {
		  	$('.points-count-search-twenty-one').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-one.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-one .points-count-search-twenty-one.focus').innerHTML;
          	document.getElementById('search-twenty-one').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-two').click(function () {
            $('.all-block-option-search-twenty-two').fadeIn(200);
            $('.scroll-pane-search-twenty-two').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-two').addClass('active');
        });

		$('.points-count-search-twenty-two.focus').click(function () {
            $('.all-block-option-search-twenty-two').fadeOut(200);
            $('.btn-select-search-twenty-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-two').length ) 
            	return;
          	$('.btn-select-search-twenty-two').removeClass('active');
          	$('.all-block-option-search-twenty-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-two').find(".points-count-search-twenty-two").click(function () {
		  	$('.points-count-search-twenty-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-two.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-two .points-count-search-twenty-two.focus').innerHTML;
          	document.getElementById('search-twenty-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-three').click(function () {
            $('.all-block-option-search-twenty-three').fadeIn(200);
            $('.scroll-pane-search-twenty-three').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-three').addClass('active');
        });

		$('.points-count-search-twenty-three.focus').click(function () {
            $('.all-block-option-search-twenty-three').fadeOut(200);
            $('.btn-select-search-twenty-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-three').length ) 
            	return;
          	$('.btn-select-search-twenty-three').removeClass('active');
          	$('.all-block-option-search-twenty-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-three').find(".points-count-search-twenty-three").click(function () {
		  	$('.points-count-search-twenty-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-three.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-three .points-count-search-twenty-three.focus').innerHTML;
          	document.getElementById('search-twenty-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-four').click(function () {
            $('.all-block-option-search-twenty-four').fadeIn(200);
            $('.scroll-pane-search-twenty-four').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-four').addClass('active');
        });

		$('.points-count-search-twenty-four.focus').click(function () {
            $('.all-block-option-search-twenty-four').fadeOut(200);
            $('.btn-select-search-twenty-four').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-four').length ) 
            	return;
          	$('.btn-select-search-twenty-four').removeClass('active');
          	$('.all-block-option-search-twenty-four').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-four').find(".points-count-search-twenty-four").click(function () {
		  	$('.points-count-search-twenty-four').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-four.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-four .points-count-search-twenty-four.focus').innerHTML;
          	document.getElementById('search-twenty-four').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-five').click(function () {
            $('.all-block-option-search-twenty-five').fadeIn(200);
            $('.scroll-pane-search-twenty-five').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-five').addClass('active');
        });

		$('.points-count-search-twenty-five.focus').click(function () {
            $('.all-block-option-search-twenty-five').fadeOut(200);
            $('.btn-select-search-twenty-five').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-five').length ) 
            	return;
          	$('.btn-select-search-twenty-five').removeClass('active');
          	$('.all-block-option-search-twenty-five').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-five').find(".points-count-search-twenty-five").click(function () {
		  	$('.points-count-search-twenty-five').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-five.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-five .points-count-search-twenty-five.focus').innerHTML;
          	document.getElementById('search-twenty-five').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-six').click(function () {
            $('.all-block-option-search-twenty-six').fadeIn(200);
            $('.scroll-pane-search-twenty-six').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-six').addClass('active');
        });

		$('.points-count-search-twenty-six.focus').click(function () {
            $('.all-block-option-search-twenty-six').fadeOut(200);
            $('.btn-select-search-twenty-six').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-six').length ) 
            	return;
          	$('.btn-select-search-twenty-six').removeClass('active');
          	$('.all-block-option-search-twenty-six').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-six').find(".points-count-search-twenty-six").click(function () {
		  	$('.points-count-search-twenty-six').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-six.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-six .points-count-search-twenty-six.focus').innerHTML;
          	document.getElementById('search-twenty-six').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-seven').click(function () {
            $('.all-block-option-search-twenty-seven').fadeIn(200);
            $('.scroll-pane-search-twenty-seven').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-seven').addClass('active');
        });

		$('.points-count-search-twenty-seven.focus').click(function () {
            $('.all-block-option-search-twenty-seven').fadeOut(200);
            $('.btn-select-search-twenty-seven').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-seven').length ) 
            	return;
          	$('.btn-select-search-twenty-seven').removeClass('active');
          	$('.all-block-option-search-twenty-seven').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-seven').find(".points-count-search-twenty-seven").click(function () {
		  	$('.points-count-search-twenty-seven').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-seven.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-seven .points-count-search-twenty-seven.focus').innerHTML;
          	document.getElementById('search-twenty-seven').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-eight').click(function () {
            $('.all-block-option-search-twenty-eight').fadeIn(200);
            $('.scroll-pane-search-twenty-eight').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-eight').addClass('active');
        });

		$('.points-count-search-twenty-eight.focus').click(function () {
            $('.all-block-option-search-twenty-eight').fadeOut(200);
            $('.btn-select-search-twenty-eight').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-eight').length ) 
            	return;
          	$('.btn-select-search-twenty-eight').removeClass('active');
          	$('.all-block-option-search-twenty-eight').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-eight').find(".points-count-search-twenty-eight").click(function () {
		  	$('.points-count-search-twenty-eight').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-eight.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-eight .points-count-search-twenty-eight.focus').innerHTML;
          	document.getElementById('search-twenty-eight').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-nine').click(function () {
            $('.all-block-option-search-twenty-nine').fadeIn(200);
            $('.scroll-pane-search-twenty-nine').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-nine').addClass('active');
        });

		$('.points-count-search-twenty-nine.focus').click(function () {
            $('.all-block-option-search-twenty-nine').fadeOut(200);
            $('.btn-select-search-twenty-nine').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-nine').length ) 
            	return;
          	$('.btn-select-search-twenty-nine').removeClass('active');
          	$('.all-block-option-search-twenty-nine').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-nine').find(".points-count-search-twenty-nine").click(function () {
		  	$('.points-count-search-twenty-nine').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-nine.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-nine .points-count-search-twenty-nine.focus').innerHTML;
          	document.getElementById('search-twenty-nine').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-thirty').click(function () {
            $('.all-block-option-search-thirty').fadeIn(200);
            $('.scroll-pane-search-thirty').jScrollPane({showArrows: true});
            $('.btn-select-search-thirty').addClass('active');
        });

		$('.points-count-search-thirty.focus').click(function () {
            $('.all-block-option-search-thirty').fadeOut(200);
            $('.btn-select-search-thirty').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-thirty').length ) 
            	return;
          	$('.btn-select-search-thirty').removeClass('active');
          	$('.all-block-option-search-thirty').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-thirty').find(".points-count-search-thirty").click(function () {
		  	$('.points-count-search-thirty').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-thirty.focus').click(function () {
          	var total = document.querySelector('.content-select-search-thirty .points-count-search-thirty.focus').innerHTML;
          	document.getElementById('search-thirty').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-thirty-one').click(function () {
            $('.all-block-option-search-thirty-one').fadeIn(200);
            $('.scroll-pane-search-thirty-one').jScrollPane({showArrows: true});
            $('.btn-select-search-thirty-one').addClass('active');
        });

		$('.points-count-search-thirty-one.focus').click(function () {
            $('.all-block-option-search-thirty-one').fadeOut(200);
            $('.btn-select-search-thirty-one').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-thirty-one').length ) 
            	return;
          	$('.btn-select-search-thirty-one').removeClass('active');
          	$('.all-block-option-search-thirty-one').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-thirty-one').find(".points-count-search-thirty-one").click(function () {
		  	$('.points-count-search-thirty-one').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-thirty-one.focus').click(function () {
          	var total = document.querySelector('.content-select-search-thirty-one .points-count-search-thirty-one.focus').innerHTML;
          	document.getElementById('search-thirty-one').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-thirty-two').click(function () {
            $('.all-block-option-search-thirty-two').fadeIn(200);
            $('.scroll-pane-search-thirty-two').jScrollPane({showArrows: true});
            $('.btn-select-search-thirty-two').addClass('active');
        });

		$('.points-count-search-thirty-two.focus').click(function () {
            $('.all-block-option-search-thirty-two').fadeOut(200);
            $('.btn-select-search-thirty-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-thirty-two').length ) 
            	return;
          	$('.btn-select-search-thirty-two').removeClass('active');
          	$('.all-block-option-search-thirty-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-thirty-two').find(".points-count-search-thirty-two").click(function () {
		  	$('.points-count-search-thirty-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-thirty-two.focus').click(function () {
          	var total = document.querySelector('.content-select-search-thirty-two .points-count-search-thirty-two.focus').innerHTML;
          	document.getElementById('search-thirty-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.content-perfect-pair').find(".one-of-like-user .left-block").hover(function() {
        	$(this).find('.block-hover-like-user').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.block-hover-like-user').fadeOut(200);
        	});
		});

		// ///////////////////////////////////////////////////////////////////

		$('.text-btn-more-search').click(function() {
        	$('.block-show-more-search').fadeIn(200);
		});

		$('.fade-out-block-more-search').click(function() {
        	$('.block-show-more-search').fadeOut(200);
		});

		// ///////////////////////////////////////////////////////////////////



    </script>

    <script>

			  if(document.documentElement.clientWidth > 1340) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 10,
		          	slidesToScroll: 10
		        });
			  }

			  else if(document.documentElement.clientWidth > 900) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 7,
		          	slidesToScroll: 7
		        });
			  }

			 else if(document.documentElement.clientWidth > 600) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 5,
		          	slidesToScroll: 5
		        });
			  }

			  else if(document.documentElement.clientWidth > 0) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 2,
		          	slidesToScroll: 2
		        });
			  }

			  else {

			  }
        </script>

        <script type="text/javascript">

            $(document).ready(function(){

            $(".btn-mobile-open").click(function(){
                $(".btn-mobile-open").fadeOut(0);
                $(".under-header").animate({'width': "+=250px"}, 500);
                $(".under-header").css({'border': "2px solid white"});
                $(".btn-mobile-close").fadeIn(200);
            });

            $(".btn-mobile-close").click(function(){
                $(".under-header").animate({'width': "-=250px"}, 500);
                $(".under-header").css({'border': "0px"});
                $(".btn-mobile-close").fadeOut(0);
                $(".btn-mobile-open").fadeIn(200);
            });

            });
    </script>



	<!-- =============================================================== -->

  	<!-- end-script -->

  </body>
</html>