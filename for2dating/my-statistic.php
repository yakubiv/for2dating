<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity bg-perfect-pair">
  		
  		<!-- baground-slider -->

  		<div class="null-block">

  			<!-- slider-display-bg -->

	        <div class="slider-display-one" style="display:none;">
	            <div class="regular-first slider">
		            <!--  -->
		            <div>
		                <div class="into-slider-one one">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one two">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one three">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one four">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one five">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one six">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one seven">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one eight">
		                </div>                  
		            </div>
		            <!--  -->
	        	</div>
	    	</div>

	    	<!-- end-slider-display-bg -->
  				
  		</div>

  		<!-- end-baground-slider -->

  		<!-- header -->

  		<div class="left-one-bg-header">
  			<div class="left-two-bg-header">
	  			<div class="center-all-block">
	  				<div class="left-center-all-block">

	  					<!--  -->

	  					<div class="over-header">

			  				<div class="block-logo-header">
			  					<a href="#">
			  						<div class="logo-header">		  							
			  						</div>
			  					</a>
			  				</div>

			  				<!--  -->

			  				<div class="block-soc-header">
			  					<span>
			  						Мы в
			  					</span>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-vk">		  								
			  							</div>
			  						</a>
			  					</div>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-face">		  								
			  							</div>
			  						</a>
			  					</div>
			  				</div>

			  				<!-- lang-select -->
	  								
	  						<div class="block-select-registration-lang">
	  							<div class="select-registration-lang">
	  								<div id="total-lang">
	  									<div class="flag-one-of-lang">			  							
						  				</div>

						  				<span>
						  					Русский
						  				</span>
	  								</div>

	  								<div class="btn-select-lang">	  											
	  								</div>
	  							</div>

	  							<div class="all-block-option-lang">
									<div class="scroll-pane-lang">
										<div class="content-select-lang">
										    <div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Русский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Английский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Норвежский
								  				</span>
										   	</div>
									    </div>
									</div>
								</div>
	  						</div>
	  								
	  						<!-- end-lang-select -->

			  				<!-- account -->

			  				<div class="block-account">
			  					<div class="block-coints">
			  						<span class="coints">
			  							0
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-user-header">
			  						<div class="border-photo-user-header">
			  							<img src="img/photo-user-header.png" alt="not-image">
			  						</div>

			  						<span>
			  							Александр
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-setting">
			  						<div class="icon-setting">			  							
			  						</div>
			  					</div>

			  					<!--  -->

			  					<div class="block-icon-enter-header">
			  						<div class="icon-enter-header">			  							
			  						</div>
			  					</div>

			  					<!--  -->

			  				</div>

			  				<!-- end-account -->

		  				</div>

		  				<!--  -->

		  				<!-- mobile-menu -->

		  				<div class="null-block">
		  					<div class="null-block">
		  						<div class="btn-mobile-open">
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="btn-mobile-close">
		  						</div>
		  					</div>
		  				</div>

		  				<!-- end-mobile-menu -->

		  				<!--  -->

		  				<div class="under-header">

		  					<!--  -->

		  					<div class="one-of-menu office">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Мой кабинет
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu office">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои альбомы
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Моя статистика
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой пакет услуг
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой счет
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои статьи
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои подарки
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Пригласи друга
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои настройки
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu message">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Уведомления
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="block-number-message">
		  								<span>
		  									648
		  								</span>
		  							</div>
		  						</div>
		  						<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu message">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои сообщения <span>(9)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подмигиваний <span>(315)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Просмотры <span>(49)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Избранные <span>(5)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подарки <span>(3)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Симпатии <span>(5)</span>
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">
		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Блог
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Карусель
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->
		  					
		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							ТОП 100
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Поиск
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Идеальная пара
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  				</div>

		  				<!--  -->
	  					
	  				</div>
	  			</div>
	  		</div>
  		</div>

  		<!-- end-header -->

  		<!-- slider-under-header -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  			<!--  -->

	  			<div class="block-slider-under-header">
	  				<div class="all-slider-under-header">

	  					<div class="block-trap-photo">
	  						<span class="bold">
	  							Ловушка:
	  						</span>

	  						<span class="regular">
	  							Попасть<br>
	  							в ленту
	  						</span>
	  					</div>

	  					<!--  -->

				        <div class="slider-display-under-header">
				            <div class="regular-under-header slider-under-header">
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/one-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/two-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/three-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/four-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/five-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
				        	</div>
				    	</div>

				    	<!--  -->

	  				</div>
	  			</div>

	  			<!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-slider-under-header -->

  		<!-- content -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<div class="content-gift-page">

	  					<!-- left-sidedar -->

	  					<div class="left-sidedar">
	  						
	  						<!--  -->

	  						<span class="header-left-sidebar">
	  							Лидер
	  						</span>

	  						<!--  -->

	  						<div class="block-one-of-banner">
	  							<div class="null-block">
	  								<div class="block-message-banner">
	  									<span>
	  										Сообщение
	  									</span>
	  								</div>	
	  							</div>

	  							<img src="img/one-image-banner.png" alt="not-image">	  							
	  						</div>

	  						<!--  -->

	  						<div class="block-coints-banner">
	  							<span>
	  								Заплати одну монету и попади в лидеры
	  							</span>
	  						</div>

	  						<!--  -->

	  						<div class="block-one-of-banner pink">
	  							<img src="img/two-image-banner.png" alt="not-image">	  							
	  						</div>

	  						<!--  -->

	  					</div>

	  					<!-- end-left-sidedar -->

	  					<!-- content-my-statistic -->

	  					<div class="content-my-statistic">
	  						<span class="header-content-my-statistic">
	  							Моя статистика
	  						</span>

	  						<span class="under-header-content-my-statistic">
	  							На данной странице мы свели общую статистику - рейтинг Вашей анкеты, кого Вы больше предпочитаете и кто просматривает Вашу анкету. Полученные данные помогут Вам понять как лучше подобрать себе пару, супруга или просто друга для общения!
	  						</span>

	  						<span class="header-reting">
	  							Рейтинг Вашей анкеты
	  						</span>

	  						<div class="all-block-reting">
	  							<div class="block-reting">
	  								<div class="progress-reting">
	  									<span class="regular">
	  										Ваша анкета на <span class="bold">368 месте</span>
	  									</span>
	  								</div>
	  							</div>

	  							<span class="btn-reting">
	  								Хочешь в ТОП 100?
	  							</span>

	  							<span class="under-header-content-my-statistic right">
	  								Заполни профиль и твоя анкета будет выше в рейтинге!
	  							</span>
	  						</div>

	  						<!-- block-tab-statistic -->
	  						<div class="block-tab-statistic">
	  							<div class="one-of-tab-reting active">
	  								<span>
	  									Какие женщины предпочитают меня?
	  								</span>
	  							</div>

	  							<div class="one-of-tab-reting center">
	  								<span>
	  									Кого предпочитаю я?
	  								</span>
	  							</div>

	  							<div class="one-of-tab-reting right">
	  								<span>
	  									Рейтинг моей анкеты
	  								</span>
	  							</div>
	  						</div>

	  						<!-- end-block-tab-statistic -->

	  						<!-- block-table -->

	  						<div class="header-around-pink-big">
	  							<span class="line-header-around-pink-big">
	  								Какие женщины предпочитают меня?
	  							</span>
	  						</div>

	  						<span class="under-header-content-my-statistic under-header-around">
	  							Мы обратили внимание на женщин, которые просматривают Вашу анкету:
	  						</span>

	  						<span class="one-of-label-tabel-one">
	  							Средний возраст женьщин 32 года
	  						</span>

	  						<span class="one-of-label-tabel-left">
	  							<span class="bold">Образование:</span> Высшее
	  						</span>

	  						<span class="one-of-label-tabel-right">
	  							<span class="bold">Отношение к курению:</span> Не курю
	  						</span>

	  						<span class="one-of-label-tabel-left">
	  							Для серьезных отношений
	  						</span>

	  						<span class="one-of-label-tabel-right top">
	  							<span class="bold">Отношение к алкоголю:</span> По праздникам и в хорошей компании
	  						</span>

	  						<span class="one-of-label-tabel-left small">
	  							<span class="bold">Религия:</span> Православная
	  						</span>

	  						<span class="one-of-label-tabel-center">
	  							<span class="bold">Рабочий статус:</span><br>
	  							На постоянной основе
	  						</span>

	  						<span class="one-of-label-tabel-left small">
	  							Для серьезных отношений
	  						</span>
	  						<!-- end-block-table -->

	  						<!--  -->
	  						<div class="header-zodiac">
	  							<span class="bold">
	  								Астрология никак не влияет на наш выбор потенциальных партнеров,
									но многие из интересных вам людей родились под одним и тем же знаком.
	  							</span>

	  							<div class="block-icon-zodiac">
		  							<div class="icon-zodiac">
		  								<!--  -->
						  				<div class="parent-icon-zodiac">
										    <div>
										        <img src="img/icon-aries.png" alt="not-image">
										    </div>
										</div>
										<!--  -->
		  							</div>

		  							<span class="bold">
		  								Овен
		  							</span>
	  							</div>
	  						</div>
	  						<!--  -->

	  						<!-- all-block -->

	  						<!-- block-one-of-rating -->
	  						<div class="block-one-of-rating">
	  							<span class="header-one-of-rating">
	  								Этническая принадлежность
	  							</span>

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Белокожая
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting">
	  									<span class="one-line-progress">	  										
	  									</span>
	  									<span class="around-one-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										75%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Смешанная национальность/ другое
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting">
	  									<span class="two-line-progress">	  										
	  									</span>
	  									<span class="around-two-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										14,3%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Азиатка
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting">
	  									<span class="three-line-progress">	  										
	  									</span>
	  									<span class="around-three-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										7,1%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Латиноамериканка
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting">
	  									<span class="four-line-progress">	  										
	  									</span>
	  									<span class="around-four-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										3,6%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->
	  						</div>
	  						<!-- end-block-one-of-rating -->

	  						<!-- block-one-of-rating -->
	  						<div class="block-one-of-rating right">
	  							<span class="header-one-of-rating">
	  								Телосложение
	  							</span>

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Обычная
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting two">
	  									<span class="one-line-progress">	  										
	  									</span>
	  									<span class="around-one-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										42,9%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Атлетичная
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting two">
	  									<span class="two-line-progress">	  										
	  									</span>
	  									<span class="around-two-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										28,6%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	С формами
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting two">
	  									<span class="three-line-progress">	  										
	  									</span>
	  									<span class="around-three-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										14,3%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Худощавая
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting two">
	  									<span class="four-line-progress">	  										
	  									</span>
	  									<span class="around-four-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										14,3%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->
	  						</div>
	  						<!-- end-block-one-of-rating -->

	  						<!-- ===================================== -->

	  						<!-- block-one-of-rating -->
	  						<div class="block-one-of-rating">
	  							<span class="header-one-of-rating">
	  								Дети
	  							</span>

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Нет
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting three">
	  									<span class="one-line-progress">	  										
	  									</span>
	  									<span class="around-one-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										71,4%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Есть, живем вместе
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting three">
	  									<span class="two-line-progress">	  										
	  									</span>
	  									<span class="around-two-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										28,6%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  							</div>
	  							<!--  -->
	  						</div>
	  						<!-- end-block-one-of-rating -->

	  						<!-- block-one-of-rating -->
	  						<div class="block-one-of-rating right">
	  							<span class="header-one-of-rating">
	  								Отношение к курению
	  							</span>

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Нет
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting four">
	  									<span class="one-line-progress">	  										
	  									</span>
	  									<span class="around-one-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										64,3%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Да, в компании
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting four">
	  									<span class="two-line-progress">	  										
	  									</span>
	  									<span class="around-two-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										25%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Да, постоянно
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting four">
	  									<span class="three-line-progress">	  										
	  									</span>
	  									<span class="around-three-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										10,7%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->
	  						</div>
	  						<!-- end-block-one-of-rating -->

	  						<!-- ===================================== -->

	  						<!-- block-one-of-rating -->
	  						<div class="block-one-of-rating">
	  							<span class="header-one-of-rating">
	  								Отношение к религии
	  							</span>

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Христианка
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="one-line-progress">	  										
	  									</span>
	  									<span class="around-one-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										35,3%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Другое
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="two-line-progress">	  										
	  									</span>
	  									<span class="around-two-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										17,6%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Атеистка
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="three-line-progress">	  										
	  									</span>
	  									<span class="around-three-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										11,8%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Христианка (католичка)
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="four-line-progress">	  										
	  									</span>
	  									<span class="around-four-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										11,8%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Верующая
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="five-line-progress">	  										
	  									</span>
	  									<span class="around-five-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										5,9%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Индуска
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="six-line-progress">	  										
	  									</span>
	  									<span class="around-six-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										5,9%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Еврейка
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="seven-line-progress">	  										
	  									</span>
	  									<span class="around-seven-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										5,9%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Мусульманка
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="eight-line-progress">	  										
	  									</span>
	  									<span class="around-eight-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										5,9%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->
	  						</div>
	  						<!-- end-block-one-of-rating -->

	  						<!-- block-one-of-rating -->
	  						<div class="block-one-of-rating right">
	  							<span class="header-one-of-rating">
	  								Гражданство
	  							</span>

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Украинец
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="one-line-progress">	  										
	  									</span>
	  									<span class="around-one-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										35,3%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Американец
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="two-line-progress">	  										
	  									</span>
	  									<span class="around-two-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										17,6%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Немец
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="three-line-progress">	  										
	  									</span>
	  									<span class="around-three-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										11,8%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Гражданство 1
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="four-line-progress">	  										
	  									</span>
	  									<span class="around-four-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										11,8%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Гражданство 2
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="five-line-progress">	  										
	  									</span>
	  									<span class="around-five-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										5,9%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Гражданство 3
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="six-line-progress">	  										
	  									</span>
	  									<span class="around-six-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										5,9%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Гражданство 4
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="seven-line-progress">	  										
	  									</span>
	  									<span class="around-seven-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										5,9%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->

	  							<!--  -->
	  							<div class="line-block-one-of-reting">
	  								<span class="left-line-block-one-of-reting">
	  									<div class="parent-text">
										    <div>
										        <span class="regular">
										        	Гражданство 5
										        </span>
										    </div>
										</div>
	  								</span>

	  								<div class="right-line-block-one-of-reting five">
	  									<span class="eight-line-progress">	  										
	  									</span>
	  									<span class="around-eight-line-progress">	  										
	  									</span>
	  									<span class="percent">
	  										5,9%
	  									</span>
	  								</div>
	  							</div>
	  							<!--  -->
	  						</div>
	  						<!-- end-block-one-of-rating -->

	  						<!-- end-all-block -->

	  						<div class="header-around-pink-big two">
	  							<span class="line-header-around-pink-big two">
	  								Кого предпочитаю я?
	  							</span>
	  						</div>

	  						<div class="header-around-pink-big two">
	  							<span class="line-header-around-pink-big three">
	  								Рейтинг моей анкеты
	  							</span>
	  						</div>

	  						<!-- block-graphic -->
	  						<div class="block-graphic">

	  							<!-- one-of-slide -->
	  							<div class="one-of-slide">
		  							<div class="left-block-graphic">
		  								<div class="line-left-block-graphic">
		  									<span class="text">
		  										<span class="null-block-line">
		  											<span class="around-line-graphic">	  												
		  											</span>
		  										</span>

		  										Топ 100	  										
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-left-block-graphic">
		  									<span class="text">
		  										<span class="null-block-line">
		  											<span class="around-line-graphic">	  												
		  											</span>
		  										</span>
		  																			
		  										Топ 500	  										
		  									</span>
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="border-graphic">
		  								<div class="block-cylinder">
		  									<span class="cylinder-up">
		  										Ваша анкета на<br>
												<span class="big">368</span> <span class="transform">месте</span>.
		  									</span>
		  									<span class="cylinder-down">
		  										<span class="null-block">
		  											<span class="hover-graphic">
		  												Заполните все анкетные
														данные и Ваша анкета
														поднимется еще выше
														в рейтенге!
		  											</span>
		  										</span>
		  										По анкетным<br>
												данным<br>
												<span class="bold">62%</span>
		  									</span>
		  								</div>
		  							</div>
	  							</div>
	  							<!-- end-one-of-slide -->

	  							<!-- two-of-slide -->
	  							<div class="two-of-slide">
		  							<div class="left-block-graphic">
		  								<div class="line-left-block-graphic">
		  									<span class="text">
		  										<span class="null-block-line">
		  											<span class="around-line-graphic">	  												
		  											</span>
		  										</span>

		  										Топ 50	  										
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-left-block-graphic">
		  									<span class="text">
		  										<span class="null-block-line">
		  											<span class="around-line-graphic">	  												
		  											</span>
		  										</span>
		  																			
		  										Топ 100	  										
		  									</span>
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="border-graphic">
		  								<div class="block-cylinder">
		  									<span class="cylinder-up">
		  										Ваша анкета на<br>
												<span class="big">368</span> <span class="transform">месте</span>.
		  									</span>
		  									<span class="cylinder-down">
		  										<span class="null-block">
		  											<span class="hover-graphic">
		  												Заполните все анкетные
														данные и Ваша анкета
														поднимется еще выше
														в рейтенге!
		  											</span>
		  										</span>
		  										По анкетным<br>
												данным<br>
												<span class="bold">62%</span>
		  									</span>
		  								</div>
		  							</div>
	  							</div>
	  							<!-- end-two-of-slide -->

	  							<!-- three-of-slide -->
	  							<div class="three-of-slide">
		  							<div class="left-block-graphic">
		  								<div class="line-left-block-graphic">
		  									<span class="text">
		  										<span class="null-block-line">
		  											<span class="around-line-graphic">	  												
		  											</span>
		  										</span>

		  										Топ 1	  										
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-left-block-graphic">
		  									<span class="text">
		  										<span class="null-block-line">
		  											<span class="around-line-graphic">	  												
		  											</span>
		  										</span>
		  																			
		  										Топ 10	  										
		  									</span>
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="border-graphic">
		  								<div class="block-cylinder">
		  									<span class="cylinder-up">
		  										Ваша анкета на<br>
												<span class="big">368</span> <span class="transform">месте</span>.
		  									</span>
		  									<span class="cylinder-down">
		  										<span class="null-block">
		  											<span class="hover-graphic">
		  												Заполните все анкетные
														данные и Ваша анкета
														поднимется еще выше
														в рейтенге!
		  											</span>
		  										</span>
		  										По анкетным<br>
												данным<br>
												<span class="bold">62%</span>
		  									</span>
		  								</div>
		  							</div>
	  							</div>
	  							<!-- end-three-of-slide -->

	  							<!-- four-of-slide -->
	  							<div class="four-of-slide">
		  							<div class="left-block-graphic">
		  								<div class="line-left-block-graphic">
		  									<span class="text">
		  										<span class="null-block-line">
		  											<span class="around-line-graphic">	  												
		  											</span>
		  										</span>

		  										Топ 1000	  										
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-left-block-graphic">
		  									<span class="text">
		  										<span class="null-block-line">
		  											<span class="around-line-graphic">	  												
		  											</span>
		  										</span>
		  																			
		  										Топ 10 000	  										
		  									</span>
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="border-graphic">
		  								<div class="block-cylinder">
		  									<span class="cylinder-up">
		  										Ваша анкета на<br>
												<span class="big">368</span> <span class="transform">месте</span>.
		  									</span>
		  									<span class="cylinder-down">
		  										<span class="null-block">
		  											<span class="hover-graphic">
		  												Заполните все анкетные
														данные и Ваша анкета
														поднимется еще выше
														в рейтенге!
		  											</span>
		  										</span>
		  										По анкетным<br>
												данным<br>
												<span class="bold">62%</span>
		  									</span>
		  								</div>
		  							</div>
	  							</div>
	  							<!-- end-four-of-slide -->

	  							<!-- btn-graphic -->
	  							<div class="btn-graphic">
	  								<!--  -->
	  								<div class="block-one-of-btn-graphic one active">
	  									<div class="parent-text-btn">
										    <div>
										        <span>
										        	Общий рейтинг анкеты
										        </span>
										    </div>
										</div>
	  								</div>
	  								<!--  -->

	  								<!--  -->
	  								<div class="block-one-of-btn-graphic two">
	  									<div class="parent-text-btn">
										    <div>
										        <span>
										        	Анкетные данные<br>
										        	<span class="regular">
										        		Заполнить анкету?
										        	</span>
										        </span>
										    </div>
										</div>
	  								</div>
	  								<!--  -->

	  								<!--  -->
	  								<div class="block-one-of-btn-graphic three">
	  									<div class="parent-text-btn">
										    <div>
										        <span>
										        	Активность на
													сайте<br>
										        	<span class="regular">
										        		Перейти в Карусель?
										        	</span>
										        </span>
										    </div>
										</div>
	  								</div>
	  								<!--  -->

	  								<!--  -->
	  								<div class="block-one-of-btn-graphic four">
	  									<div class="parent-text-btn">
										    <div>
										        <span>
										        	Платные пакеты, услуги<br>
										        	<span class="regular">
										        		Перейти в пакеты услуг?
										        	</span>
										        </span>
										    </div>
										</div>
	  								</div>
	  								<!--  -->
	  							</div>
	  							<!-- end-btn-graphic -->
	  						</div>
	  						<!-- end-block-graphic -->


	  					</div>
	  					<!-- end-content-my-statistic -->
	  				</div>
	  			</div>
	  		</div>
	  	</div>
  		<!-- end-content -->

  		<!-- footer-display -->

  		<div class="footer-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

		  			<div class="footer-menu">

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					О нас
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Условия и правила
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Конфиденциальность
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Партнерам
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Отзывы
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Связаться с Нами
		  				</span>

		  				<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="block-soc-footer">

		  				<!--  -->

		  				<span class="header-soc-footer">
		  					Присоединяйся к <span>For two dating:</span>
		  				</span>

		  				<!--  -->

		  				<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-vk">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-face">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-twit">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-inst">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-yt">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-gp">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="footer-all-right">
		  				<span class="header-soc-footer">
		  					© 2016- 2016 For2date. Все права защищены. Разработка I-PR
		  				</span>
		  			</div>

		  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-footer-display -->

  	</div>

  	<!-- script -->

  	<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

  	<script type="text/javascript">
        $(".regular-first").slick({
          	dots: false,
          	autoplay: true,
           	prevArrow: false,
            nextArrow: false,
            centerMode: true,
          	infinite: true,
          	slidesToShow: 1,
          	slidesToScroll: 1,
          	autoplaySpeed: 10000
        });

        $(".regular-two").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });

        $(".regular-gift-for-user").slick({
          infinite: true,
          // centerMode: true,
          slidesToShow: 3,
          slidesToScroll: 3
        });
        
    </script>

    <!-- =============================================================== -->

    <script src="js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
	    var config = {
	      '.chosen-select'           : {},
	      '.chosen-select-deselect'  : {allow_single_deselect:true},
	      '.chosen-select-no-single' : {disable_search_threshold:1},
	      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	      '.chosen-select-width'     : {width:"95%"}
	    }
	    for (var selector in config) {
	      $(selector).chosen(config[selector]);
	    }
	</script>

	<!-- =============================================================== -->

	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>

	<script type="text/javascript" src="js/jquery.jscrollpane.js"></script>

	<script type="text/javascript">

		$('.one-of-menu.message').click(function () {
            $('.all-block-dropdown-menu.message').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.message').length ) 
            	return;
          	$('.all-block-dropdown-menu.message').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.message').mouseleave (function() {
          $('.all-block-dropdown-menu.message').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

        $('.one-of-menu.office').click(function () {
            $('.all-block-dropdown-menu.office').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.office').length ) 
            	return;
          	$('.all-block-dropdown-menu.office').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.office').mouseleave (function() {
          $('.all-block-dropdown-menu.office').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

		$('.btn-login').click(function () {
            $('.block-content-registretion').fadeOut(0);
            $('.block-content-login').fadeIn(0);
            $('.btn-login').addClass('active');
            $('.btn-registration').removeClass('active');
        });

        $('.btn-registration').click(function () {
            $('.block-content-login').fadeOut(0);
            $('.block-content-registretion').fadeIn(0);
            $('.btn-registration').addClass('active');
            $('.btn-login').removeClass('active');
        });

        // //////////////////////////////////////////////////////////

        $('.into-gift-center-content').find(".one-of-tab-content").click(function () {
		  	$('.one-of-tab-content').removeClass('active');
	        $(this).addClass('active');
		});

		$('.one-of-tab-content.all-gift').click(function () {
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.all-gift').fadeIn(200);
        });

		$('.one-of-tab-content.new-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeIn(200);
            $(".regular-gift-for-user-two").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        $('.one-of-tab-content.my-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeIn(200);
            $(".regular-gift-for-user-three").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        // //////////////////////////////////////////////////////////

		$('.select-registration-one').click(function () {
            $('.all-block-option').fadeIn(200);
            $('.scroll-pane').jScrollPane({showArrows: true});
            $('.btn-select').addClass('active');
        });

		$('.points-count.focus').click(function () {
            $('.all-block-option').fadeOut(200);
            $('.btn-select').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-one').length ) 
            	return;
          	$('.btn-select').removeClass('active');
          	$('.all-block-option').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-one').find(".points-count").click(function () {
		  	$('.points-count').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count.focus').click(function () {
          	var total = document.querySelector('.content-select-one .points-count.focus').innerHTML;
          	document.getElementById('total').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-two').click(function () {
            $('.all-block-option-two').fadeIn(200);
            $('.scroll-pane-two').jScrollPane({showArrows: true});
            $('.btn-select-two').addClass('active');
        });

		$('.points-count-two.focus').click(function () {
            $('.all-block-option-two').fadeOut(200);
            $('.btn-select-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-two').length ) 
            	return;
          	$('.btn-select-two').removeClass('active');
          	$('.all-block-option-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-two').find(".points-count-two").click(function () {
		  	$('.points-count-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-two.focus').click(function () {
          	var total = document.querySelector('.content-select-two .points-count-two.focus').innerHTML;
          	document.getElementById('total-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-three').click(function () {
            $('.all-block-option-three').fadeIn(200);
            $('.scroll-pane-three').jScrollPane({showArrows: true});
            $('.btn-select-three').addClass('active');
        });

		$('.points-count-three.focus').click(function () {
            $('.all-block-option-three').fadeOut(200);
            $('.btn-select-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-three').length ) 
            	return;
          	$('.btn-select-three').removeClass('active');
          	$('.all-block-option-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-three').find(".points-count-three").click(function () {
		  	$('.points-count-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-three.focus').click(function () {
          	var total = document.querySelector('.content-select-three .points-count-three.focus').innerHTML;
          	document.getElementById('total-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-lang').click(function () {
            $('.all-block-option-lang').fadeIn(200);
            // $('.scroll-pane-lang').jScrollPane({showArrows: true});
            $('.btn-select-lang').addClass('active');
        });

		$('.points-count-lang.focus').click(function () {
            $('.all-block-option-lang').fadeOut(200);
            $('.btn-select-lang').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-lang').length ) 
            	return;
          	$('.btn-select-lang').removeClass('active');
          	$('.all-block-option-lang').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-lang').find(".points-count-lang").click(function () {
		  	$('.points-count-lang').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-lang.focus').click(function () {
          	var total = document.querySelector('.content-select-lang .points-count-lang.focus').innerHTML;
          	document.getElementById('total-lang').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-one').click(function () {
            $('.all-block-option-search').fadeIn(200);
            $('.scroll-pane-search').jScrollPane({showArrows: true});
            $('.btn-select-search').addClass('active');
        });

		$('.points-count-search.focus').click(function () {
            $('.all-block-option-search').fadeOut(200);
            $('.btn-select-search').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-one').length ) 
            	return;
          	$('.btn-select-search').removeClass('active');
          	$('.all-block-option-search').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-one').find(".points-count-search").click(function () {
		  	$('.points-count-search').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search.focus').click(function () {
          	var total = document.querySelector('.content-select-search-one .points-count-search.focus').innerHTML;
          	document.getElementById('search').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

         // //////////////////////////////////////////////////////////

		$('.select-search-two').click(function () {
            $('.all-block-option-search-two').fadeIn(200);
            $('.scroll-pane-search-two').jScrollPane({showArrows: true});
            $('.btn-select-search-two').addClass('active');
        });

		$('.points-count-search-two.focus').click(function () {
            $('.all-block-option-search-two').fadeOut(200);
            $('.btn-select-search-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-two').length ) 
            	return;
          	$('.btn-select-search-two').removeClass('active');
          	$('.all-block-option-search-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-two').find(".points-count-search-two").click(function () {
		  	$('.points-count-search-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-two.focus').click(function () {
          	var total = document.querySelector('.content-select-search-two .points-count-search-two.focus').innerHTML;
          	document.getElementById('search-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-three').click(function () {
            $('.all-block-option-search-three').fadeIn(200);
            $('.scroll-pane-search-three').jScrollPane({showArrows: true});
            $('.btn-select-search-three').addClass('active');
        });

		$('.points-count-search-three.focus').click(function () {
            $('.all-block-option-search-three').fadeOut(200);
            $('.btn-select-search-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-three').length ) 
            	return;
          	$('.btn-select-search-three').removeClass('active');
          	$('.all-block-option-search-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-three').find(".points-count-search-three").click(function () {
		  	$('.points-count-search-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-three.focus').click(function () {
          	var total = document.querySelector('.content-select-search-three .points-count-search-three.focus').innerHTML;
          	document.getElementById('search-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-four').click(function () {
            $('.all-block-option-search-four').fadeIn(200);
            $('.scroll-pane-search-four').jScrollPane({showArrows: true});
            $('.btn-select-search-four').addClass('active');
        });

		$('.points-count-search-four.focus').click(function () {
            $('.all-block-option-search-four').fadeOut(200);
            $('.btn-select-search-four').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-four').length ) 
            	return;
          	$('.btn-select-search-four').removeClass('active');
          	$('.all-block-option-search-four').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-four').find(".points-count-search-four").click(function () {
		  	$('.points-count-search-four').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-four.focus').click(function () {
          	var total = document.querySelector('.content-select-search-four .points-count-search-four.focus').innerHTML;
          	document.getElementById('search-four').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-six').click(function () {
            $('.all-block-option-search-six').fadeIn(200);
            $('.scroll-pane-search-six').jScrollPane({showArrows: true});
            $('.btn-select-search-six').addClass('active');
        });

		$('.points-count-search-six.focus').click(function () {
            $('.all-block-option-search-six').fadeOut(200);
            $('.btn-select-search-six').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-six').length ) 
            	return;
          	$('.btn-select-search-six').removeClass('active');
          	$('.all-block-option-search-six').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-six').find(".points-count-search-six").click(function () {
		  	$('.points-count-search-six').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-six.focus').click(function () {
          	var total = document.querySelector('.content-select-search-six .points-count-search-six.focus').innerHTML;
          	document.getElementById('search-six').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-search-five').click(function () {
            $('.all-block-option-search-five').fadeIn(200);
            $('.scroll-pane-search-five').jScrollPane({showArrows: true});
            $('.btn-select-search-five').addClass('active');
        });

		$('.points-count-search-five.focus').click(function () {
            $('.all-block-option-search-five').fadeOut(200);
            $('.btn-select-search-five').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-five').length ) 
            	return;
          	$('.btn-select-search-five').removeClass('active');
          	$('.all-block-option-search-five').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-five').find(".points-count-search-five").click(function () {
		  	$('.points-count-search-five').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-five.focus').click(function () {
          	var total = document.querySelector('.content-select-search-five .points-count-search-five.focus').innerHTML;
          	document.getElementById('search-five').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

         // //////////////////////////////////////////////////////////

		$('.select-search-seven').click(function () {
            $('.all-block-option-search-seven').fadeIn(200);
            $('.scroll-pane-search-seven').jScrollPane({showArrows: true});
            $('.btn-select-search-seven').addClass('active');
        });

		$('.points-count-search-seven.focus').click(function () {
            $('.all-block-option-search-seven').fadeOut(200);
            $('.btn-select-search-seven').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-seven').length ) 
            	return;
          	$('.btn-select-search-seven').removeClass('active');
          	$('.all-block-option-search-seven').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-seven').find(".points-count-search-seven").click(function () {
		  	$('.points-count-search-seven').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-seven.focus').click(function () {
          	var total = document.querySelector('.content-select-search-seven .points-count-search-seven.focus').innerHTML;
          	document.getElementById('search-seven').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // //////////////////////////////////////////////////////////

		$('.select-search-eight').click(function () {
            $('.all-block-option-search-eight').fadeIn(200);
            $('.scroll-pane-search-eight').jScrollPane({showArrows: true});
            $('.btn-select-search-eight').addClass('active');
        });

		$('.points-count-search-eight.focus').click(function () {
            $('.all-block-option-search-eight').fadeOut(200);
            $('.btn-select-search-eight').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-eight').length ) 
            	return;
          	$('.btn-select-search-eight').removeClass('active');
          	$('.all-block-option-search-eight').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-eight').find(".points-count-search-eight").click(function () {
		  	$('.points-count-search-eight').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-eight.focus').click(function () {
          	var total = document.querySelector('.content-select-search-eight .points-count-search-eight.focus').innerHTML;
          	document.getElementById('search-eight').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-nine').click(function () {
            $('.all-block-option-search-nine').fadeIn(200);
            $('.scroll-pane-search-nine').jScrollPane({showArrows: true});
            $('.btn-select-search-nine').addClass('active');
        });

		$('.points-count-search-nine.focus').click(function () {
            $('.all-block-option-search-nine').fadeOut(200);
            $('.btn-select-search-nine').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-nine').length ) 
            	return;
          	$('.btn-select-search-nine').removeClass('active');
          	$('.all-block-option-search-nine').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-nine').find(".points-count-search-nine").click(function () {
		  	$('.points-count-search-nine').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-nine.focus').click(function () {
          	var total = document.querySelector('.content-select-search-nine .points-count-search-nine.focus').innerHTML;
          	document.getElementById('search-nine').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-ten').click(function () {
            $('.all-block-option-search-ten').fadeIn(200);
            $('.scroll-pane-search-ten').jScrollPane({showArrows: true});
            $('.btn-select-search-ten').addClass('active');
        });

		$('.points-count-search-ten.focus').click(function () {
            $('.all-block-option-search-ten').fadeOut(200);
            $('.btn-select-search-ten').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-ten').length ) 
            	return;
          	$('.btn-select-search-ten').removeClass('active');
          	$('.all-block-option-search-ten').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-ten').find(".points-count-search-ten").click(function () {
		  	$('.points-count-search-ten').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-ten.focus').click(function () {
          	var total = document.querySelector('.content-select-search-ten .points-count-search-ten.focus').innerHTML;
          	document.getElementById('search-ten').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-elewen').click(function () {
            $('.all-block-option-search-elewen').fadeIn(200);
            $('.scroll-pane-search-elewen').jScrollPane({showArrows: true});
            $('.btn-select-search-elewen').addClass('active');
        });

		$('.points-count-search-elewen.focus').click(function () {
            $('.all-block-option-search-elewen').fadeOut(200);
            $('.btn-select-search-elewen').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-elewen').length ) 
            	return;
          	$('.btn-select-search-elewen').removeClass('active');
          	$('.all-block-option-search-elewen').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-elewen').find(".points-count-search-elewen").click(function () {
		  	$('.points-count-search-elewen').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-elewen.focus').click(function () {
          	var total = document.querySelector('.content-select-search-elewen .points-count-search-elewen.focus').innerHTML;
          	document.getElementById('search-elewen').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-tvelwe').click(function () {
            $('.all-block-option-search-tvelwe').fadeIn(200);
            $('.scroll-pane-search-tvelwe').jScrollPane({showArrows: true});
            $('.btn-select-search-tvelwe').addClass('active');
        });

		$('.points-count-search-tvelwe.focus').click(function () {
            $('.all-block-option-search-tvelwe').fadeOut(200);
            $('.btn-select-search-tvelwe').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-tvelwe').length ) 
            	return;
          	$('.btn-select-search-tvelwe').removeClass('active');
          	$('.all-block-option-search-tvelwe').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-tvelwe').find(".points-count-search-tvelwe").click(function () {
		  	$('.points-count-search-tvelwe').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-tvelwe.focus').click(function () {
          	var total = document.querySelector('.content-select-search-tvelwe .points-count-search-tvelwe.focus').innerHTML;
          	document.getElementById('search-tvelwe').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-thirting').click(function () {
            $('.all-block-option-search-thirting').fadeIn(200);
            $('.scroll-pane-search-thirting').jScrollPane({showArrows: true});
            $('.btn-select-search-thirting').addClass('active');
        });

		$('.points-count-search-thirting.focus').click(function () {
            $('.all-block-option-search-thirting').fadeOut(200);
            $('.btn-select-search-thirting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-thirting').length ) 
            	return;
          	$('.btn-select-search-thirting').removeClass('active');
          	$('.all-block-option-search-thirting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-thirting').find(".points-count-search-thirting").click(function () {
		  	$('.points-count-search-thirting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-thirting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-thirting .points-count-search-thirting.focus').innerHTML;
          	document.getElementById('search-thirting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-fourting').click(function () {
            $('.all-block-option-search-fourting').fadeIn(200);
            $('.scroll-pane-search-fourting').jScrollPane({showArrows: true});
            $('.btn-select-search-fourting').addClass('active');
        });

		$('.points-count-search-fourting.focus').click(function () {
            $('.all-block-option-search-fourting').fadeOut(200);
            $('.btn-select-search-fourting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-fourting').length ) 
            	return;
          	$('.btn-select-search-fourting').removeClass('active');
          	$('.all-block-option-search-fourting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-fourting').find(".points-count-search-fourting").click(function () {
		  	$('.points-count-search-fourting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-fourting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-fourting .points-count-search-fourting.focus').innerHTML;
          	document.getElementById('search-fourting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-fiveting').click(function () {
            $('.all-block-option-search-fiveting').fadeIn(200);
            $('.scroll-pane-search-fiveting').jScrollPane({showArrows: true});
            $('.btn-select-search-fiveting').addClass('active');
        });

		$('.points-count-search-fiveting.focus').click(function () {
            $('.all-block-option-search-fiveting').fadeOut(200);
            $('.btn-select-search-fiveting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-fiveting').length ) 
            	return;
          	$('.btn-select-search-fiveting').removeClass('active');
          	$('.all-block-option-search-fiveting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-fiveting').find(".points-count-search-fiveting").click(function () {
		  	$('.points-count-search-fiveting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-fiveting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-fiveting .points-count-search-fiveting.focus').innerHTML;
          	document.getElementById('search-fiveting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////

        //////////////////////////////////////////////////////////////////////

        $('.select-search-sixting').click(function () {
            $('.all-block-option-search-sixting').fadeIn(200);
            $('.scroll-pane-search-sixting').jScrollPane({showArrows: true});
            $('.btn-select-search-sixting').addClass('active');
        });

		$('.points-count-search-sixting.focus').click(function () {
            $('.all-block-option-search-sixting').fadeOut(200);
            $('.btn-select-search-sixting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-sixting').length ) 
            	return;
          	$('.btn-select-search-sixting').removeClass('active');
          	$('.all-block-option-search-sixting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-sixting').find(".points-count-search-sixting").click(function () {
		  	$('.points-count-search-sixting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-sixting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-sixting .points-count-search-sixting.focus').innerHTML;
          	document.getElementById('search-sixting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-seventing').click(function () {
            $('.all-block-option-search-seventing').fadeIn(200);
            $('.scroll-pane-search-seventing').jScrollPane({showArrows: true});
            $('.btn-select-search-seventing').addClass('active');
        });

		$('.points-count-search-seventing.focus').click(function () {
            $('.all-block-option-search-seventing').fadeOut(200);
            $('.btn-select-search-seventing').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-seventing').length ) 
            	return;
          	$('.btn-select-search-seventing').removeClass('active');
          	$('.all-block-option-search-seventing').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-seventing').find(".points-count-search-seventing").click(function () {
		  	$('.points-count-search-seventing').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-seventing.focus').click(function () {
          	var total = document.querySelector('.content-select-search-seventing .points-count-search-seventing.focus').innerHTML;
          	document.getElementById('search-seventing').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-eighting').click(function () {
            $('.all-block-option-search-eighting').fadeIn(200);
            $('.scroll-pane-search-eighting').jScrollPane({showArrows: true});
            $('.btn-select-search-eighting').addClass('active');
        });

		$('.points-count-search-eighting.focus').click(function () {
            $('.all-block-option-search-eighting').fadeOut(200);
            $('.btn-select-search-eighting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-eighting').length ) 
            	return;
          	$('.btn-select-search-eighting').removeClass('active');
          	$('.all-block-option-search-eighting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-eighting').find(".points-count-search-eighting").click(function () {
		  	$('.points-count-search-eighting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-eighting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-eighting .points-count-search-eighting.focus').innerHTML;
          	document.getElementById('search-eighting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-nineting').click(function () {
            $('.all-block-option-search-nineting').fadeIn(200);
            $('.scroll-pane-search-nineting').jScrollPane({showArrows: true});
            $('.btn-select-search-nineting').addClass('active');
        });

		$('.points-count-search-nineting.focus').click(function () {
            $('.all-block-option-search-nineting').fadeOut(200);
            $('.btn-select-search-nineting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-nineting').length ) 
            	return;
          	$('.btn-select-search-nineting').removeClass('active');
          	$('.all-block-option-search-nineting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-nineting').find(".points-count-search-nineting").click(function () {
		  	$('.points-count-search-nineting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-nineting.focus').click(function () {
          	var total = document.querySelector('.content-select-search-nineting .points-count-search-nineting.focus').innerHTML;
          	document.getElementById('search-nineting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty').click(function () {
            $('.all-block-option-search-twenty').fadeIn(200);
            $('.scroll-pane-search-twenty').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty').addClass('active');
        });

		$('.points-count-search-twenty.focus').click(function () {
            $('.all-block-option-search-twenty').fadeOut(200);
            $('.btn-select-search-twenty').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty').length ) 
            	return;
          	$('.btn-select-search-twenty').removeClass('active');
          	$('.all-block-option-search-twenty').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty').find(".points-count-search-twenty").click(function () {
		  	$('.points-count-search-twenty').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty .points-count-search-twenty.focus').innerHTML;
          	document.getElementById('search-twenty').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-one').click(function () {
            $('.all-block-option-search-twenty-one').fadeIn(200);
            $('.scroll-pane-search-twenty-one').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-one').addClass('active');
        });

		$('.points-count-search-twenty-one.focus').click(function () {
            $('.all-block-option-search-twenty-one').fadeOut(200);
            $('.btn-select-search-twenty-one').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-one').length ) 
            	return;
          	$('.btn-select-search-twenty-one').removeClass('active');
          	$('.all-block-option-search-twenty-one').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-one').find(".points-count-search-twenty-one").click(function () {
		  	$('.points-count-search-twenty-one').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-one.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-one .points-count-search-twenty-one.focus').innerHTML;
          	document.getElementById('search-twenty-one').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-two').click(function () {
            $('.all-block-option-search-twenty-two').fadeIn(200);
            $('.scroll-pane-search-twenty-two').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-two').addClass('active');
        });

		$('.points-count-search-twenty-two.focus').click(function () {
            $('.all-block-option-search-twenty-two').fadeOut(200);
            $('.btn-select-search-twenty-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-two').length ) 
            	return;
          	$('.btn-select-search-twenty-two').removeClass('active');
          	$('.all-block-option-search-twenty-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-two').find(".points-count-search-twenty-two").click(function () {
		  	$('.points-count-search-twenty-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-two.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-two .points-count-search-twenty-two.focus').innerHTML;
          	document.getElementById('search-twenty-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-three').click(function () {
            $('.all-block-option-search-twenty-three').fadeIn(200);
            $('.scroll-pane-search-twenty-three').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-three').addClass('active');
        });

		$('.points-count-search-twenty-three.focus').click(function () {
            $('.all-block-option-search-twenty-three').fadeOut(200);
            $('.btn-select-search-twenty-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-three').length ) 
            	return;
          	$('.btn-select-search-twenty-three').removeClass('active');
          	$('.all-block-option-search-twenty-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-three').find(".points-count-search-twenty-three").click(function () {
		  	$('.points-count-search-twenty-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-three.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-three .points-count-search-twenty-three.focus').innerHTML;
          	document.getElementById('search-twenty-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-four').click(function () {
            $('.all-block-option-search-twenty-four').fadeIn(200);
            $('.scroll-pane-search-twenty-four').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-four').addClass('active');
        });

		$('.points-count-search-twenty-four.focus').click(function () {
            $('.all-block-option-search-twenty-four').fadeOut(200);
            $('.btn-select-search-twenty-four').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-four').length ) 
            	return;
          	$('.btn-select-search-twenty-four').removeClass('active');
          	$('.all-block-option-search-twenty-four').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-four').find(".points-count-search-twenty-four").click(function () {
		  	$('.points-count-search-twenty-four').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-four.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-four .points-count-search-twenty-four.focus').innerHTML;
          	document.getElementById('search-twenty-four').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-five').click(function () {
            $('.all-block-option-search-twenty-five').fadeIn(200);
            $('.scroll-pane-search-twenty-five').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-five').addClass('active');
        });

		$('.points-count-search-twenty-five.focus').click(function () {
            $('.all-block-option-search-twenty-five').fadeOut(200);
            $('.btn-select-search-twenty-five').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-five').length ) 
            	return;
          	$('.btn-select-search-twenty-five').removeClass('active');
          	$('.all-block-option-search-twenty-five').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-five').find(".points-count-search-twenty-five").click(function () {
		  	$('.points-count-search-twenty-five').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-five.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-five .points-count-search-twenty-five.focus').innerHTML;
          	document.getElementById('search-twenty-five').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-six').click(function () {
            $('.all-block-option-search-twenty-six').fadeIn(200);
            $('.scroll-pane-search-twenty-six').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-six').addClass('active');
        });

		$('.points-count-search-twenty-six.focus').click(function () {
            $('.all-block-option-search-twenty-six').fadeOut(200);
            $('.btn-select-search-twenty-six').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-six').length ) 
            	return;
          	$('.btn-select-search-twenty-six').removeClass('active');
          	$('.all-block-option-search-twenty-six').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-six').find(".points-count-search-twenty-six").click(function () {
		  	$('.points-count-search-twenty-six').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-six.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-six .points-count-search-twenty-six.focus').innerHTML;
          	document.getElementById('search-twenty-six').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-seven').click(function () {
            $('.all-block-option-search-twenty-seven').fadeIn(200);
            $('.scroll-pane-search-twenty-seven').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-seven').addClass('active');
        });

		$('.points-count-search-twenty-seven.focus').click(function () {
            $('.all-block-option-search-twenty-seven').fadeOut(200);
            $('.btn-select-search-twenty-seven').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-seven').length ) 
            	return;
          	$('.btn-select-search-twenty-seven').removeClass('active');
          	$('.all-block-option-search-twenty-seven').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-seven').find(".points-count-search-twenty-seven").click(function () {
		  	$('.points-count-search-twenty-seven').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-seven.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-seven .points-count-search-twenty-seven.focus').innerHTML;
          	document.getElementById('search-twenty-seven').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-eight').click(function () {
            $('.all-block-option-search-twenty-eight').fadeIn(200);
            $('.scroll-pane-search-twenty-eight').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-eight').addClass('active');
        });

		$('.points-count-search-twenty-eight.focus').click(function () {
            $('.all-block-option-search-twenty-eight').fadeOut(200);
            $('.btn-select-search-twenty-eight').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-eight').length ) 
            	return;
          	$('.btn-select-search-twenty-eight').removeClass('active');
          	$('.all-block-option-search-twenty-eight').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-eight').find(".points-count-search-twenty-eight").click(function () {
		  	$('.points-count-search-twenty-eight').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-eight.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-eight .points-count-search-twenty-eight.focus').innerHTML;
          	document.getElementById('search-twenty-eight').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-twenty-nine').click(function () {
            $('.all-block-option-search-twenty-nine').fadeIn(200);
            $('.scroll-pane-search-twenty-nine').jScrollPane({showArrows: true});
            $('.btn-select-search-twenty-nine').addClass('active');
        });

		$('.points-count-search-twenty-nine.focus').click(function () {
            $('.all-block-option-search-twenty-nine').fadeOut(200);
            $('.btn-select-search-twenty-nine').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-twenty-nine').length ) 
            	return;
          	$('.btn-select-search-twenty-nine').removeClass('active');
          	$('.all-block-option-search-twenty-nine').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-twenty-nine').find(".points-count-search-twenty-nine").click(function () {
		  	$('.points-count-search-twenty-nine').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-twenty-nine.focus').click(function () {
          	var total = document.querySelector('.content-select-search-twenty-nine .points-count-search-twenty-nine.focus').innerHTML;
          	document.getElementById('search-twenty-nine').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-thirty').click(function () {
            $('.all-block-option-search-thirty').fadeIn(200);
            $('.scroll-pane-search-thirty').jScrollPane({showArrows: true});
            $('.btn-select-search-thirty').addClass('active');
        });

		$('.points-count-search-thirty.focus').click(function () {
            $('.all-block-option-search-thirty').fadeOut(200);
            $('.btn-select-search-thirty').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-thirty').length ) 
            	return;
          	$('.btn-select-search-thirty').removeClass('active');
          	$('.all-block-option-search-thirty').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-thirty').find(".points-count-search-thirty").click(function () {
		  	$('.points-count-search-thirty').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-thirty.focus').click(function () {
          	var total = document.querySelector('.content-select-search-thirty .points-count-search-thirty.focus').innerHTML;
          	document.getElementById('search-thirty').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-thirty-one').click(function () {
            $('.all-block-option-search-thirty-one').fadeIn(200);
            $('.scroll-pane-search-thirty-one').jScrollPane({showArrows: true});
            $('.btn-select-search-thirty-one').addClass('active');
        });

		$('.points-count-search-thirty-one.focus').click(function () {
            $('.all-block-option-search-thirty-one').fadeOut(200);
            $('.btn-select-search-thirty-one').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-thirty-one').length ) 
            	return;
          	$('.btn-select-search-thirty-one').removeClass('active');
          	$('.all-block-option-search-thirty-one').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-thirty-one').find(".points-count-search-thirty-one").click(function () {
		  	$('.points-count-search-thirty-one').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-thirty-one.focus').click(function () {
          	var total = document.querySelector('.content-select-search-thirty-one .points-count-search-thirty-one.focus').innerHTML;
          	document.getElementById('search-thirty-one').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-search-thirty-two').click(function () {
            $('.all-block-option-search-thirty-two').fadeIn(200);
            $('.scroll-pane-search-thirty-two').jScrollPane({showArrows: true});
            $('.btn-select-search-thirty-two').addClass('active');
        });

		$('.points-count-search-thirty-two.focus').click(function () {
            $('.all-block-option-search-thirty-two').fadeOut(200);
            $('.btn-select-search-thirty-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-search-thirty-two').length ) 
            	return;
          	$('.btn-select-search-thirty-two').removeClass('active');
          	$('.all-block-option-search-thirty-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-search-thirty-two').find(".points-count-search-thirty-two").click(function () {
		  	$('.points-count-search-thirty-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-search-thirty-two.focus').click(function () {
          	var total = document.querySelector('.content-select-search-thirty-two .points-count-search-thirty-two.focus').innerHTML;
          	document.getElementById('search-thirty-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.center-content-perfect-pair').find(".one-of-like-user .left-block").hover(function() {
        	$(this).find('.block-hover-like-user').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.block-hover-like-user').fadeOut(200);
        	});
		});

		// ///////////////////////////////////////////////////////////////////

		$('.text-btn-more-search').click(function() {
        	$('.block-show-more-search').fadeIn(200);
		});

		$('.fade-out-block-more-search').click(function() {
        	$('.block-show-more-search').fadeOut(200);
		});

		// ///////////////////////////////////////////////////////////////////

		$('.border-graphic').find(".cylinder-down").hover(function() {
        	$(this).find('.hover-graphic').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.hover-graphic').fadeOut(200);
        	});
		});

		$('.btn-graphic').find(".block-one-of-btn-graphic").click(function() {
        	$('.block-one-of-btn-graphic').removeClass('active');
        	$(this).addClass('active');
		});

		$(".block-one-of-btn-graphic.one").click(function() {
        	$('.two-of-slide').fadeOut(0);
        	$('.three-of-slide').fadeOut(0);
        	$('.four-of-slide').fadeOut(0);
        	$('.one-of-slide').fadeIn(500);
		});

		$(".block-one-of-btn-graphic.two").click(function() {
        	$('.one-of-slide').fadeOut(0);
        	$('.three-of-slide').fadeOut(0);
        	$('.four-of-slide').fadeOut(0);
        	$('.two-of-slide').fadeIn(500);
		});

		$(".block-one-of-btn-graphic.three").click(function() {
        	$('.one-of-slide').fadeOut(0);
        	$('.two-of-slide').fadeOut(0);
        	$('.four-of-slide').fadeOut(0);
        	$('.three-of-slide').fadeIn(500);
		});

		$(".block-one-of-btn-graphic.four").click(function() {
        	$('.one-of-slide').fadeOut(0);
        	$('.three-of-slide').fadeOut(0);
        	$('.two-of-slide').fadeOut(0);
        	$('.four-of-slide').fadeIn(500);
		});

    </script>

    <script>

    if(document.documentElement.clientWidth > 1340) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 10,
		          	slidesToScroll: 10
		        });
			  }

			  else if(document.documentElement.clientWidth > 900) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 7,
		          	slidesToScroll: 7
		        });
			  }

			 else if(document.documentElement.clientWidth > 600) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 5,
		          	slidesToScroll: 5
		        });
			  }

			  else if(document.documentElement.clientWidth > 0) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 2,
		          	slidesToScroll: 2
		        });
			  }

    </script>

    <script type="text/javascript">

            $(document).ready(function(){

            $(".btn-mobile-open").click(function(){
                $(".btn-mobile-open").fadeOut(0);
                $(".under-header").animate({'width': "+=250px"}, 500);
                $(".under-header").css({'border': "2px solid white"});
                $(".btn-mobile-close").fadeIn(200);
            });

            $(".btn-mobile-close").click(function(){
                $(".under-header").animate({'width': "-=250px"}, 500);
                $(".under-header").css({'border': "0px"});
                $(".btn-mobile-close").fadeOut(0);
                $(".btn-mobile-open").fadeIn(200);
            });

            });
    </script>



	<!-- =============================================================== -->

  	<!-- end-script -->

  </body>
</html>