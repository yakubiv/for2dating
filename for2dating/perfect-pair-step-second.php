<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />
    <link rel="stylesheet" href="css/form-validation.css">

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity perfect-pair-step-first">

  		<!-- baground-slider -->

  		<div class="null-block">

  			<!-- slider-display-bg -->

	        <div class="slider-display-one" style="display:none;">
	            <div class="regular-first slider">
		            <!--  -->
		            <div>
		                <div class="into-slider-one one">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one two">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one three">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one four">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one five">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one six">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one seven">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one eight">
		                </div>
		            </div>
		            <!--  -->
	        	</div>
	    	</div>

	    	<!-- end-slider-display-bg -->

  		</div>

  		<!-- end-baground-slider -->

  		<!-- header -->

  		<div class="left-one-bg-header">
  			<div class="left-two-bg-header">
	  			<div class="center-all-block">
	  				<div class="left-center-all-block">

	  					<!--  -->

	  					<div class="over-header">

			  				<div class="block-logo-header">
			  					<a href="#">
			  						<div class="logo-header">
			  						</div>
			  					</a>
			  				</div>

			  				<!--  -->

			  				<div class="block-soc-header">
			  					<span>
			  						Мы в
			  					</span>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-vk">
			  							</div>
			  						</a>
			  					</div>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-face">
			  							</div>
			  						</a>
			  					</div>
			  				</div>

			  				<!-- lang-select -->

	  						<div class="block-select-registration-lang">
	  							<div class="select-registration-lang">
	  								<div id="total-lang">
	  									<div class="flag-one-of-lang">
						  				</div>

						  				<span>
						  					Русский
						  				</span>
	  								</div>

	  								<div class="btn-select-lang">
	  								</div>
	  							</div>

	  							<div class="all-block-option-lang">
									<div class="scroll-pane-lang">
										<div class="content-select-lang">
										    <div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">
								  				</div>

								  				<span>
								  					Русский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">
								  				</div>

								  				<span>
								  					Английский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">
								  				</div>

								  				<span>
								  					Норвежский
								  				</span>
										   	</div>
									    </div>
									</div>
								</div>
	  						</div>

	  						<!-- end-lang-select -->

			  				<!--  -->

			  				<div class="block-reg-enter">
			  					<div class="btn-enter">
			  						<span>
			  							Войти
			  						</span>
			  					</div>

			  					<div class="btn-reg">
			  						<span>
			  							Регистрация
			  						</span>
			  					</div>
			  				</div>

			  				<!--  -->

		  				</div>

		  				<!--  -->

		  				<!-- mobile-menu -->

		  				<div class="null-block">
		  					<div class="null-block">
		  						<div class="btn-mobile-open">
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="btn-mobile-close">
		  						</div>
		  					</div>
		  				</div>

		  				<!-- end-mobile-menu -->

		  				<!--  -->

		  				<div class="under-header">

		  					<!--  -->

		  					<div class="one-of-menu office">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Мой кабинет
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu office">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои альбомы
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Моя статистика
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой пакет услуг
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой счет
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои статьи
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои подарки
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Пригласи друга
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои настройки
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu message">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Уведомления
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="block-number-message">
		  								<span>
		  									648
		  								</span>
		  							</div>
		  						</div>
		  						<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu message">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои сообщения <span>(9)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подмигиваний <span>(315)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Просмотры <span>(49)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Избранные <span>(5)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подарки <span>(3)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Симпатии <span>(5)</span>
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">

		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Блог
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Карусель
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							ТОП 100
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Поиск
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Идеальная пара
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  				</div>

		  				<!--  -->

	  				</div>
	  			</div>
	  		</div>
  		</div>

  		<!-- end-header -->

  		<!-- line-block-under-header -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="line-block-under-header">
	  					<div class="one-step ok">
	  						<span class="bold">
	  							Шаг
	  						</span>

	  						<span class="bg-around">
	  							1
	  						</span>

	  						<span class="regular">
	  							Фото
	  						</span>
	  					</div>

	  					<!--  -->

	  					<div class="two-step now">
	  						<span class="bold">
	  							Шаг
	  						</span>

	  						<span class="bg-around">
	  							2
	  						</span>

	  						<span class="regular">
	  							Персональные данные
	  						</span>
	  					</div>

	  					<!--  -->

	  					<div class="three-step">
	  						<span class="bold">
	  							Шаг
	  						</span>

	  						<span class="bg-around">
	  							3
	  						</span>

	  						<span class="regular">
	  							О себе
	  						</span>
	  					</div>

	  					<!--  -->

	  					<div class="four-step">
	  						<span class="bold">
	  							Шаг
	  						</span>

	  						<span class="bg-around">
	  							4
	  						</span>

	  						<span class="regular">
	  							Я ищу
	  						</span>
	  					</div>

	  					<!--  -->

	  				</div>

	  				<!--  -->

	  				<!--  -->

	  				<span class="line-block-under-step">
	  					Заполните обязательные поля, чтобы вас находили чаще
	  				</span>

	  				<span class="point-pink">
	  					Ответы на эти вопросы профиля помогут другим пользователям найти вас в результатах поиска.
	  				</span>

	  				<span class="point-pink">
	  					Ответьте на все вопросы ниже для завершения этого шага.
	  				</span>

	  				<span class="point-pink">
	  					В разделе Персональные данные все вопросы обязательные для заполнения.
	  				</span>

	  				<span class="point-pink">
	  					Остальные вопросы не обязательные и вы можете к ним вернуться позже.
	  				</span>

	  				<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-line-block-under-header -->

  		<!-- one-display -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<div class="block-two-step-registration">

	  					<!-- registretion -->

	  					<form>

		  					<!--  -->

		  					<span class="header-two-step-registration">
		  						Персональные данные
		  					</span>

		  					<!--  -->

		  					<div class="block-content-registretion-step">

	  							<div class="line-block-input-registretion first">
	  								<div class="bg-input-registretion">
	  									<input class="input-registration" placeholder="Иван" minlength="2" required>
	  								</div>

	  								<div class="icon-must">
	  								</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-i-am">
	  								<span class="i-am">
	  									Я
	  								</span>

	  								<div class="block-check-woman-man">
			  							<input id="check-woman" type="checkbox" name="check-woman">
	                      				<label class="check-woman" for="check-woman"></label>

	                      				<span class="text-check-woman">
	                      					жещина
	                      				</span>
			  						</div>

			  						<div class="block-check-woman-man">
			  							<input id="check-man" type="checkbox" name="check-man">
	                      				<label class="check-man" for="check-man"></label>

	                      				<span class="text-check-woman">
	                      					мужчина
	                      				</span>
			  						</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-i-am">
	  								<span class="i-am">
	  									Ищу
	  								</span>

	  								<div class="block-check-woman-man">
			  							<input id="check-icon-woman" type="checkbox" name="check-icon-woman">
	                      				<label class="check-icon-woman" for="check-icon-woman"></label>

	                      				<span class="text-check-woman">
	                      					жещину
	                      				</span>
			  						</div>

			  						<div class="block-check-woman-man">
			  							<input id="check-icon-man" type="checkbox" name="check-icon-man">
	                      				<label class="check-icon-man" for="check-icon-man"></label>

	                      				<span class="text-check-woman">
	                      					мужчину
	                      				</span>
			  						</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-date-registration">

	  								<span class="date-registration">
	  									Дата рождения:
	  								</span>

	  								<!-- one-select -->

	  								<div class="block-select-registration-one">
	  									<div class="select-registration-one">
	  										<div id="total">
	  											15
	  										</div>

	  										<div class="btn-select">

	  										</div>
	  									</div>

	  									<div class="all-block-option">
											<div class="scroll-pane">
												<div class="content-select-one">
												    <div class="points-count focus">
												    	1
												    </div>

												    <div class="points-count focus">
												    	2
												    </div>

												    <div class="points-count focus">
												    	3
												    </div>

												    <div class="points-count focus">
												    	4
												    </div>

												    <div class="points-count focus">
												    	5
												    </div>

												    <div class="points-count focus">
												    	6
												    </div>

												    <div class="points-count focus">
												    	7
												    </div>

												    <div class="points-count focus">
												    	8
												    </div>

												    <div class="points-count focus">
												    	9
												    </div>

												    <div class="points-count focus">
												    	10
												    </div>

												    <div class="points-count focus">
												    	11
												    </div>

												    <div class="points-count focus">
												    	12
												    </div>

												    <div class="points-count focus">
												    	13
												    </div>

												    <div class="points-count focus">
												    	14
												    </div>

												    <div class="points-count focus">
												    	15
												    </div>

												    <div class="points-count focus">
												    	16
												    </div>

												    <div class="points-count focus">
												    	17
												    </div>

												    <div class="points-count focus">
												    	18
												    </div>

												    <div class="points-count focus">
												    	19
												    </div>

												    <div class="points-count focus">
												    	20
												    </div>

												    <div class="points-count focus">
												    	21
												    </div>

												    <div class="points-count focus">
												    	22
												    </div>

												    <div class="points-count focus">
												    	23
												    </div>

												    <div class="points-count focus">
												    	24
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-one-select -->

	  								<!-- two-select -->

	  								<div class="block-select-registration-two">
	  									<div class="select-registration-two">
	  										<div id="total-two">
	  											Января
	  										</div>

	  										<div class="btn-select-two">
	  										</div>
	  									</div>

	  									<div class="all-block-option-two">
											<div class="scroll-pane-two">
												<div class="content-select-two">
												    <div class="points-count-two focus">
												    	Января
												    </div>

												    <div class="points-count-two focus">
												    	Февраля
												    </div>

												    <div class="points-count-two focus">
												    	Марта
												    </div>

												    <div class="points-count-two focus">
												    	Апреля
												    </div>

												    <div class="points-count-two focus">
												    	Мая
												    </div>

												    <div class="points-count-two focus">
												    	Июня
												    </div>

												    <div class="points-count-two focus">
												    	Июля
												    </div>

												    <div class="points-count-two focus">
												    	Августа
												    </div>

												    <div class="points-count-two focus">
												    	Сентября
												    </div>

												    <div class="points-count-two focus">
												    	Октября
												    </div>

												    <div class="points-count-two focus">
												    	Ноября
												    </div>

												    <div class="points-count-two focus">
												    	Декабря
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-two-select -->

	  								<!-- three-select -->

	  								<div class="block-select-registration-three step">
	  									<div class="select-registration-three">
	  										<div id="total-three">
	  											1980
	  										</div>

	  										<div class="btn-select-three">
	  										</div>
	  									</div>

	  									<div class="all-block-option-three">
											<div class="scroll-pane-three">
												<div class="content-select-three">
												    <div class="points-count-three focus">
												    	2016
												    </div>

												    <div class="points-count-three focus">
												    	2015
												    </div>

												    <div class="points-count-three focus">
												    	2014
												    </div>

												    <div class="points-count-three focus">
												    	2013
												    </div>

												    <div class="points-count-three focus">
												    	2012
												    </div>

												    <div class="points-count-three focus">
												    	2011
												    </div>

												    <div class="points-count-three focus">
												    	2010
												    </div>

												    <div class="points-count-three focus">
												    	2009
												    </div>

												    <div class="points-count-three focus">
												    	2008
												    </div>

												    <div class="points-count-three focus">
												    	2007
												    </div>

												    <div class="points-count-three focus">
												    	2006
												    </div>

												    <div class="points-count-three focus">
												    	2005
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-three-select -->

	  								<div class="icon-must">
		  							</div>

	  								<!--  -->

	  							</div>

	  							<!--  -->



	  							<div class="line-block-input-registretion">
		  							<div class="bg-input-registretion">
		  								<input class="input-registration" type="email" placeholder="email@gmail.com"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}" required>
		  							</div>

	  								<div class="icon-must">
	  								</div>
	  							</div>

	  							<!--  -->

	  							<div class="line-block-input-registretion">
	  								<div class="bg-input-registretion">
	  									<input class="input-registration" placeholder="Гражданство" required>
	  								</div>

	  								<div class="icon-must">
	  								</div>
	  							</div>

	  							<!--  -->

	  							<div class="line-block-input-registretion">
	  								<div class="bg-input-registretion">
	  									<input class="input-registration" placeholder="Страна" required>
	  								</div>

	  								<div class="icon-must">
	  								</div>
	  							</div>

	  							<!--  -->

	  							<div class="line-block-input-registretion">
	  								<div class="bg-input-registretion">
	  									<input class="input-registration" placeholder="Регион/Провинция" required>
	  								</div>

	  								<div class="icon-must">
	  								</div>
	  							</div>

	  							<!--  -->

	  							<div class="line-block-input-registretion last">
	  								<div class="bg-input-registretion">
	  									<input class="input-registration" placeholder="Город" required>
	  								</div>

	  								<div class="icon-must">
	  								</div>
	  							</div>

	  							<!--  -->

	  							<div class="block-far-away">

	  								<span>
	  									Отдаленность от города
	  								</span>

		  							<div class="number-way">
				                      	<div class="minus-number-way"></div>
				                      		<input class="result-number-way" type="text" value="0">
				                      	<div class="plus-number-way"></div>
				                    </div>

				                    <span class="counter-number-way">
				                        км
				                    </span>

				                </div>

	  							<!--  -->

	  							<div class="block-change-country">
	  								<span class="change-country">
	  									Изменить страну
	  								</span>

	  								<div class="block-flag">
	  									<img src="img/flag-uk.png" alt="not-image">
	  								</div>

	  								<div class="bg-input-change-country">
	  									<input class="input-registration" placeholder="+38 введите номер телефона">
	  								</div>

	  								<span class="verify-number">
	  									Проверить телефон
	  								</span>

	  								<div class="bg-input-sms">
	  									<input class="input-registration" placeholder="Код из смс">
	  								</div>

	  								<div class="icon-ok">
	  								</div>
	  							</div>

	  							<!--  -->

	  							<textarea class="textarea-step" placeholder="Статус (необязательно)"></textarea>

	  							<!--  -->

	  						</div>

	  						<!--  -->

	  						<span class="header-person-two-step-registration">
			  					Ваша внешность
			  				</span>

			  				<!--  -->

			  				<div class="block-two-step-registration-without-border">

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Тип телосложения
			  						</span>

			  						<!-- select-body-type -->

	  								<div class="block-select-body-type">
	  									<div class="select-body-type">
	  										<div id="body-type">
	  											Выберите
	  										</div>

	  										<div class="btn-select-body-type">
	  										</div>
	  									</div>

	  									<div class="all-block-option-body-type">
											<div class="scroll-pane-body-type">
												<div class="content-select-body-type">
												    <div class="points-count-body-type focus">
												    	Выберите
												    </div>

												    <div class="points-count-body-type focus">
												    	Текст
												    </div>

												    <div class="points-count-body-type focus">
												    	Текст
												    </div>

												    <div class="points-count-body-type focus">
												    	Текст
												    </div>

												    <div class="points-count-body-type focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-body-type -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Этническая принадлежность
			  						</span>

			  						<!-- select-ethnicity -->

	  								<div class="block-select-ethnicity">
	  									<div class="select-ethnicity">
	  										<div id="ethnicity">
	  											Выберите
	  										</div>

	  										<div class="btn-select-ethnicity">
	  										</div>
	  									</div>

	  									<div class="all-block-option-ethnicity">
											<div class="scroll-pane-ethnicity">
												<div class="content-select-ethnicity">
												    <div class="points-count-ethnicity focus">
												    	Выберите
												    </div>

												    <div class="points-count-ethnicity focus">
												    	Текст
												    </div>

												    <div class="points-count-ethnicity focus">
												    	Текст
												    </div>

												    <div class="points-count-ethnicity focus">
												    	Текст
												    </div>

												    <div class="points-count-ethnicity focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-ethnicity -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Считаю свою внешность
			  						</span>

			  						<!-- select-appearance -->

	  								<div class="block-select-appearance">
	  									<div class="select-appearance">
	  										<div id="appearance">
	  											Выберите
	  										</div>

	  										<div class="btn-select-appearance">
	  										</div>
	  									</div>

	  									<div class="all-block-option-appearance">
											<div class="scroll-pane-appearance">
												<div class="content-select-appearance">
												    <div class="points-count-appearance focus">
												    	Выберите
												    </div>

												    <div class="points-count-appearance focus">
												    	Текст
												    </div>

												    <div class="points-count-appearance focus">
												    	Текст
												    </div>

												    <div class="points-count-appearance focus">
												    	Текст
												    </div>

												    <div class="points-count-appearance focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-appearance -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Цвет волос
			  						</span>

			  						<!-- select-hair-color -->

	  								<div class="block-select-hair-color">
	  									<div class="select-hair-color">
	  										<div id="hair-color">
	  											Выберите
	  										</div>

	  										<div class="btn-select-hair-color">
	  										</div>
	  									</div>

	  									<div class="all-block-option-hair-color">
											<div class="scroll-pane-hair-color">
												<div class="content-select-hair-color">
												    <div class="points-count-hair-color focus">
												    	Выберите
												    </div>

												    <div class="points-count-hair-color focus">
												    	Текст
												    </div>

												    <div class="points-count-hair-color focus">
												    	Текст
												    </div>

												    <div class="points-count-hair-color focus">
												    	Текст
												    </div>

												    <div class="points-count-hair-color focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-hair-color -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Длина волос
			  						</span>

			  						<!-- select-hair-length -->

	  								<div class="block-select-hair-length">
	  									<div class="select-hair-length">
	  										<input class="input-injected input-registration" type="text" name="" placeholder="Введите">
	  									</div>
	  								</div>

	  								<!-- end-select-hair-length -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Тип волос
			  						</span>

			  						<!-- select-hair-type -->

	  								<div class="block-select-hair-type">
	  									<div class="select-hair-type">
	  										<div id="hair-type">
	  											Выберите
	  										</div>

	  										<div class="btn-select-hair-type">
	  										</div>
	  									</div>

	  									<div class="all-block-option-hair-type">
											<div class="scroll-pane-hair-type">
												<div class="content-select-hair-type">
												    <div class="points-count-hair-type focus">
												    	Выберите
												    </div>

												    <div class="points-count-hair-type focus">
												    	Текст
												    </div>

												    <div class="points-count-hair-type focus">
												    	Текст
												    </div>

												    <div class="points-count-hair-type focus">
												    	Текст
												    </div>

												    <div class="points-count-hair-type focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-hair-type -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Цвет глаз
			  						</span>

			  						<!-- select-eye-color -->

	  								<div class="block-select-eye-color">
	  									<div class="select-eye-color">
	  										<div id="eye-color">
	  											Выберите
	  										</div>

	  										<div class="btn-select-eye-color">
	  										</div>
	  									</div>

	  									<div class="all-block-option-eye-color">
											<div class="scroll-pane-eye-color">
												<div class="content-select-eye-color">
												    <div class="points-count-eye-color focus">
												    	Выберите
												    </div>

												    <div class="points-count-eye-color focus">
												    	Текст
												    </div>

												    <div class="points-count-eye-color focus">
												    	Текст
												    </div>

												    <div class="points-count-eye-color focus">
												    	Текст
												    </div>

												    <div class="points-count-eye-color focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-eye-color -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Контактные линзы, очки
			  						</span>

			  						<!-- select-glasses -->

	  								<div class="block-select-glasses">
	  									<div class="select-glasses">
	  										<div id="glasses">
	  											Выберите
	  										</div>

	  										<div class="btn-select-glasses">
	  										</div>
	  									</div>

	  									<div class="all-block-option-glasses">
											<div class="scroll-pane-glasses">
												<div class="content-select-glasses">
												    <div class="points-count-glasses focus">
												    	Выберите
												    </div>

												    <div class="points-count-glasses focus">
												    	Текст
												    </div>

												    <div class="points-count-glasses focus">
												    	Текст
												    </div>

												    <div class="points-count-glasses focus">
												    	Текст
												    </div>

												    <div class="points-count-glasses focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-glasses -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Самое привлекательное в моей внешности
			  						</span>

			  						<!-- select-most-attractive -->

	  								<div class="block-select-most-attractive">
	  									<div class="select-most-attractive">
	  										<div id="most-attractive">
	  											Выберите
	  										</div>

	  										<div class="btn-select-most-attractive">
	  										</div>
	  									</div>

	  									<div class="all-block-option-most-attractive">
											<div class="scroll-pane-most-attractive">
												<div class="content-select-most-attractive">
												    <div class="points-count-most-attractive focus">
												    	Выберите
												    </div>

												    <div class="points-count-most-attractive focus">
												    	Текст
												    </div>

												    <div class="points-count-most-attractive focus">
												    	Текст
												    </div>

												    <div class="points-count-most-attractive focus">
												    	Текст
												    </div>

												    <div class="points-count-most-attractive focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-most-attractive -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select last">
			  						<span class="name-chose">
			  							Боди-арт
			  						</span>

			  						<!-- select-body-art -->

	  								<div class="block-select-body-art">
	  									<div class="select-body-art">
	  										<div id="body-art">
	  											Выберите
	  										</div>

	  										<div class="btn-select-body-art">
	  										</div>
	  									</div>

	  									<div class="all-block-option-body-art">
											<div class="scroll-pane-body-art">
												<div class="content-select-body-art">
												    <div class="points-count-body-art focus">
												    	Выберите
												    </div>

												    <div class="points-count-body-art focus">
												    	Текст
												    </div>

												    <div class="points-count-body-art focus">
												    	Текст
												    </div>

												    <div class="points-count-body-art focus">
												    	Текст
												    </div>

												    <div class="points-count-body-art focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-body-art -->

			  					</div>

			  					<!-- end-line-block-select -->

			  				</div>

			  				<!--  -->

	  						<span class="header-lifestyle-two-step-registration">
			  					Образ жизни
			  				</span>

			  				<!--  -->

			  				<div class="block-two-step-registration-without-border">

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Отношение к курению?
			  						</span>

			  						<!-- select-smoking -->

	  								<div class="block-select-smoking">
	  									<div class="select-smoking">
	  										<div id="smoking">
	  											Выберите
	  										</div>

	  										<div class="btn-select-smoking">
	  										</div>
	  									</div>

	  									<div class="all-block-option-smoking">
											<div class="scroll-pane-smoking">
												<div class="content-select-smoking">
												    <div class="points-count-smoking focus">
												    	Выберите
												    </div>

												    <div class="points-count-smoking focus">
												    	Текст
												    </div>

												    <div class="points-count-smoking focus">
												    	Текст
												    </div>

												    <div class="points-count-smoking focus">
												    	Текст
												    </div>

												    <div class="points-count-smoking focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-smoking -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Отношение к алкоголю?
			  						</span>

			  						<!-- select-alcohol -->

	  								<div class="block-select-alcohol">
	  									<div class="select-alcohol">
	  										<div id="alcohol">
	  											Выберите
	  										</div>

	  										<div class="btn-select-alcohol">
	  										</div>
	  									</div>

	  									<div class="all-block-option-alcohol">
											<div class="scroll-pane-alcohol">
												<div class="content-select-alcohol">
												    <div class="points-count-alcohol focus">
												    	Выберите
												    </div>

												    <div class="points-count-alcohol focus">
												    	Текст
												    </div>

												    <div class="points-count-alcohol focus">
												    	Текст
												    </div>

												    <div class="points-count-alcohol focus">
												    	Текст
												    </div>

												    <div class="points-count-alcohol focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-alcohol -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Хотел(а) бы сменить место жительства на
			  						</span>

			  						<!-- select-change-residence -->

	  								<div class="block-select-change-residence">
	  									<div class="select-change-residence">
	  										<div id="change-residence">
	  											Выберите
	  										</div>

	  										<div class="btn-select-change-residence">
	  										</div>
	  									</div>

	  									<div class="all-block-option-change-residence">
											<div class="scroll-pane-change-residence">
												<div class="content-select-change-residence">
												    <div class="points-count-change-residence focus">
												    	Выберите
												    </div>

												    <div class="points-count-change-residence focus">
												    	Текст
												    </div>

												    <div class="points-count-change-residence focus">
												    	Текст
												    </div>

												    <div class="points-count-change-residence focus">
												    	Текст
												    </div>

												    <div class="points-count-change-residence focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-change-residence -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Семейное положение
			  						</span>

			  						<!-- select-marital-status -->

	  								<div class="block-select-marital-status">
	  									<div class="select-marital-status">
	  										<div id="marital-status">
	  											Выберите
	  										</div>

	  										<div class="btn-select-marital-status">
	  										</div>
	  									</div>

	  									<div class="all-block-option-marital-status">
											<div class="scroll-pane-marital-status">
												<div class="content-select-marital-status">
												    <div class="points-count-marital-status focus">
												    	Выберите
												    </div>

												    <div class="points-count-marital-status focus">
												    	Текст
												    </div>

												    <div class="points-count-marital-status focus">
												    	Текст
												    </div>

												    <div class="points-count-marital-status focus">
												    	Текст
												    </div>

												    <div class="points-count-marital-status focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-marital-status -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							У вас есть дети?
			  						</span>

			  						<!-- select-have-children -->

	  								<div class="block-select-have-children">
	  									<div class="select-have-children">
	  										<div id="have-children">
	  											Выберите
	  										</div>

	  										<div class="btn-select-have-children">
	  										</div>
	  									</div>

	  									<div class="all-block-option-have-children">
											<div class="scroll-pane-have-children">
												<div class="content-select-have-children">
												    <div class="points-count-have-children focus">
												    	Выберите
												    </div>

												    <div class="points-count-have-children focus">
												    	Текст
												    </div>

												    <div class="points-count-have-children focus">
												    	Текст
												    </div>

												    <div class="points-count-have-children focus">
												    	Текст
												    </div>

												    <div class="points-count-have-children focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-have-children -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Количество детей
			  						</span>

			  						<!-- select-number-of-children -->

	  								<div class="block-select-number-of-children">
	  									<div class="select-number-of-children">
	  										<div id="number-of-children">
	  											Выберите
	  										</div>

	  										<div class="btn-select-number-of-children">
	  										</div>
	  									</div>

	  									<div class="all-block-option-number-of-children">
											<div class="scroll-pane-number-of-children">
												<div class="content-select-number-of-children">
												    <div class="points-count-number-of-children focus">
												    	Выберите
												    </div>

												    <div class="points-count-number-of-children focus">
												    	Текст
												    </div>

												    <div class="points-count-number-of-children focus">
												    	Текст
												    </div>

												    <div class="points-count-number-of-children focus">
												    	Текст
												    </div>

												    <div class="points-count-number-of-children focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-number-of-children -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Возраст самого старшего ребенка
			  						</span>

			  						<!-- select-oldest-child -->

	  								<div class="block-select-oldest-child">
	  									<div class="select-oldest-child">
	  										<div id="oldest-child">
	  											Выберите
	  										</div>

	  										<div class="btn-select-oldest-child">
	  										</div>
	  									</div>

	  									<div class="all-block-option-oldest-child">
											<div class="scroll-pane-oldest-child">
												<div class="content-select-oldest-child">
												    <div class="points-count-oldest-child focus">
												    	Выберите
												    </div>

												    <div class="points-count-oldest-child focus">
												    	Текст
												    </div>

												    <div class="points-count-oldest-child focus">
												    	Текст
												    </div>

												    <div class="points-count-oldest-child focus">
												    	Текст
												    </div>

												    <div class="points-count-oldest-child focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-oldest-child -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Возраст самого младшего ребенка
			  						</span>

			  						<!-- select-youngest-child -->

	  								<div class="block-select-youngest-child">
	  									<div class="select-youngest-child">
	  										<div id="youngest-child">
	  											Выберите
	  										</div>

	  										<div class="btn-select-youngest-child">
	  										</div>
	  									</div>

	  									<div class="all-block-option-youngest-child">
											<div class="scroll-pane-youngest-child">
												<div class="content-select-youngest-child">
												    <div class="points-count-youngest-child focus">
												    	Выберите
												    </div>

												    <div class="points-count-youngest-child focus">
												    	Текст
												    </div>

												    <div class="points-count-youngest-child focus">
												    	Текст
												    </div>

												    <div class="points-count-youngest-child focus">
												    	Текст
												    </div>

												    <div class="points-count-youngest-child focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-youngest-child -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Хотите ли вы  (еще) детей?
			  						</span>

			  						<!-- select-want-children -->

	  								<div class="block-select-want-children">
	  									<div class="select-want-children">
	  										<div id="want-children">
	  											Выберите
	  										</div>

	  										<div class="btn-select-want-children">
	  										</div>
	  									</div>

	  									<div class="all-block-option-want-children">
											<div class="scroll-pane-want-children">
												<div class="content-select-want-children">
												    <div class="points-count-want-children focus">
												    	Выберите
												    </div>

												    <div class="points-count-want-children focus">
												    	Текст
												    </div>

												    <div class="points-count-want-children focus">
												    	Текст
												    </div>

												    <div class="points-count-want-children focus">
												    	Текст
												    </div>

												    <div class="points-count-want-children focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-want-children -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Есть ли у вас домашние животные
			  						</span>

			  						<!-- select-have-pets -->

	  								<div class="block-select-have-pets">
	  									<div class="select-have-pets">
	  										<div id="have-pets">
	  											Выберите
	  										</div>

	  										<div class="btn-select-have-pets">
	  										</div>
	  									</div>

	  									<div class="all-block-option-have-pets">
											<div class="scroll-pane-have-pets">
												<div class="content-select-have-pets">
												    <div class="points-count-have-pets focus">
												    	Выберите
												    </div>

												    <div class="points-count-have-pets focus">
												    	Текст
												    </div>

												    <div class="points-count-have-pets focus">
												    	Текст
												    </div>

												    <div class="points-count-have-pets focus">
												    	Текст
												    </div>

												    <div class="points-count-have-pets focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-have-pets -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Специальность
			  						</span>

			  						<!-- select-profession -->

	  								<div class="block-select-profession">
	  									<div class="select-profession">
	  										<div id="profession">
	  											Выберите
	  										</div>

	  										<div class="btn-select-profession">
	  										</div>
	  									</div>

	  									<div class="all-block-option-profession">
											<div class="scroll-pane-profession">
												<div class="content-select-profession">
												    <div class="points-count-profession focus">
												    	Выберите
												    </div>

												    <div class="points-count-profession focus">
												    	Текст
												    </div>

												    <div class="points-count-profession focus">
												    	Текст
												    </div>

												    <div class="points-count-profession focus">
												    	Текст
												    </div>

												    <div class="points-count-profession focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-profession -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Рабочий статус
			  						</span>

			  						<!-- select-work-status -->

	  								<div class="block-select-work-status">
	  									<div class="select-work-status">
	  										<div id="work-status">
	  											Выберите
	  										</div>

	  										<div class="btn-select-work-status">
	  										</div>
	  									</div>

	  									<div class="all-block-option-work-status">
											<div class="scroll-pane-work-status">
												<div class="content-select-work-status">
												    <div class="points-count-work-status focus">
												    	Выберите
												    </div>

												    <div class="points-count-work-status focus">
												    	Текст
												    </div>

												    <div class="points-count-work-status focus">
												    	Текст
												    </div>

												    <div class="points-count-work-status focus">
												    	Текст
												    </div>

												    <div class="points-count-work-status focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-work-status -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Тип собственности/дома
			  						</span>

			  						<!-- select-type-property -->

	  								<div class="block-select-type-property">
	  									<div class="select-type-property">
	  										<div id="type-property">
	  											Выберите
	  										</div>

	  										<div class="btn-select-type-property">
	  										</div>
	  									</div>

	  									<div class="all-block-option-type-property">
											<div class="scroll-pane-type-property">
												<div class="content-select-type-property">
												    <div class="points-count-type-property focus">
												    	Выберите
												    </div>

												    <div class="points-count-type-property focus">
												    	Текст
												    </div>

												    <div class="points-count-type-property focus">
												    	Текст
												    </div>

												    <div class="points-count-type-property focus">
												    	Текст
												    </div>

												    <div class="points-count-type-property focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-type-property -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Ситуация с жильем
			  						</span>

			  						<!-- select-housing-situation -->

	  								<div class="block-select-housing-situation">
	  									<div class="select-housing-situation">
	  										<div id="housing-situation">
	  											Выберите
	  										</div>

	  										<div class="btn-select-housing-situation">
	  										</div>
	  									</div>

	  									<div class="all-block-option-housing-situation">
											<div class="scroll-pane-housing-situation">
												<div class="content-select-housing-situation">
												    <div class="points-count-housing-situation focus">
												    	Выберите
												    </div>

												    <div class="points-count-housing-situation focus">
												    	Текст
												    </div>

												    <div class="points-count-housing-situation focus">
												    	Текст
												    </div>

												    <div class="points-count-housing-situation focus">
												    	Текст
												    </div>

												    <div class="points-count-housing-situation focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-housing-situation -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Выберите основной язык
			  						</span>

			  						<!-- select-primary-language -->

	  								<div class="block-select-primary-language">
	  									<div class="select-primary-language">
	  										<div id="primary-language">
	  											Выберите
	  										</div>

	  										<div class="btn-select-primary-language">
	  										</div>
	  									</div>

	  									<div class="all-block-option-primary-language">
											<div class="scroll-pane-primary-language">
												<div class="content-select-primary-language">
												    <div class="points-count-primary-language focus">
												    	Выберите
												    </div>

												    <div class="points-count-primary-language focus">
												    	Текст
												    </div>

												    <div class="points-count-primary-language focus">
												    	Текст
												    </div>

												    <div class="points-count-primary-language focus">
												    	Текст
												    </div>

												    <div class="points-count-primary-language focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-primary-language -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Выберите языки которые знаете
			  						</span>

			  						<!-- select-languages-know -->

	  								<div class="block-select-languages-know">
	  									<div class="select-languages-know">
	  										<div id="languages-know">
	  											Выберите
	  										</div>

	  										<div class="btn-select-languages-know">
	  										</div>
	  									</div>

	  									<div class="all-block-option-languages-know">
											<div class="scroll-pane-languages-know">
												<div class="content-select-languages-know">
												    <div class="points-count-languages-know focus">
												    	Выберите
												    </div>

												    <div class="points-count-languages-know focus">
												    	Текст
												    </div>

												    <div class="points-count-languages-know focus">
												    	Текст
												    </div>

												    <div class="points-count-languages-know focus">
												    	Текст
												    </div>

												    <div class="points-count-languages-know focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-languages-know -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Знак зодиака по месяцу
			  						</span>

			  						<!-- select-zodiac -->

	  								<div class="block-select-zodiac">
	  									<div class="select-zodiac">
	  										<div id="zodiac">
	  											Выберите
	  										</div>

	  										<div class="btn-select-zodiac">
	  										</div>
	  									</div>

	  									<div class="all-block-option-zodiac">
											<div class="scroll-pane-zodiac">
												<div class="content-select-zodiac">
												    <div class="points-count-zodiac focus">
												    	Выберите
												    </div>

												    <div class="points-count-zodiac focus">
												    	Текст
												    </div>

												    <div class="points-count-zodiac focus">
												    	Текст
												    </div>

												    <div class="points-count-zodiac focus">
												    	Текст
												    </div>

												    <div class="points-count-zodiac focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-zodiac -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select last">
			  						<span class="name-chose">
			  							Есть ли у вас автомобиль
			  						</span>

			  						<!-- select-have-car -->

	  								<div class="block-select-have-car">
	  									<div class="select-have-car">
	  										<div id="have-car">
	  											Выберите
	  										</div>

	  										<div class="btn-select-have-car">
	  										</div>
	  									</div>

	  									<div class="all-block-option-have-car">
											<div class="scroll-pane-have-car">
												<div class="content-select-have-car">
												    <div class="points-count-have-car focus">
												    	Выберите
												    </div>

												    <div class="points-count-have-car focus">
												    	Текст
												    </div>

												    <div class="points-count-have-car focus">
												    	Текст
												    </div>

												    <div class="points-count-have-car focus">
												    	Текст
												    </div>

												    <div class="points-count-have-car focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-have-car -->

			  					</div>

			  					<!-- end-line-block-select -->

			  				</div>

			  				<!--  -->

	  						<span class="header-personality-two-step-registration">
			  					Личность
			  				</span>

			  				<!--  -->

			  				<div class="block-two-step-registration-without-border">

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Образование
			  						</span>

			  						<!-- select-education -->

	  								<div class="block-select-education">
	  									<div class="select-education">
	  										<div id="education">
	  											Выберите
	  										</div>

	  										<div class="btn-select-education">
	  										</div>
	  									</div>

	  									<div class="all-block-option-education">
											<div class="scroll-pane-education">
												<div class="content-select-education">
												    <div class="points-count-education focus">
												    	Выберите
												    </div>

												    <div class="points-count-education focus">
												    	Текст
												    </div>

												    <div class="points-count-education focus">
												    	Текст
												    </div>

												    <div class="points-count-education focus">
												    	Текст
												    </div>

												    <div class="points-count-education focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-education -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Чем вы занимаетесь для развлечения
			  						</span>

			  						<!-- select-do-for-fun -->

	  								<div class="block-select-do-for-fun">
	  									<div class="select-do-for-fun">
	  										<div id="do-for-fun">
	  											Выберите
	  										</div>

	  										<div class="btn-select-do-for-fun">
	  										</div>
	  									</div>

	  									<div class="all-block-option-do-for-fun">
											<div class="scroll-pane-do-for-fun">
												<div class="content-select-do-for-fun">
												    <div class="points-count-do-for-fun focus">
												    	Выберите
												    </div>

												    <div class="points-count-do-for-fun focus">
												    	Текст
												    </div>

												    <div class="points-count-do-for-fun focus">
												    	Текст
												    </div>

												    <div class="points-count-do-for-fun focus">
												    	Текст
												    </div>

												    <div class="points-count-do-for-fun focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-do-for-fun -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Какая еда вам нравится?
			  						</span>

			  						<!-- select-food-like -->

	  								<div class="block-select-food-like">
	  									<div class="select-food-like">
	  										<div id="food-like">
	  											Выберите
	  										</div>

	  										<div class="btn-select-food-like">
	  										</div>
	  									</div>

	  									<div class="all-block-option-food-like">
											<div class="scroll-pane-food-like">
												<div class="content-select-food-like">
												    <div class="points-count-food-like focus">
												    	Выберите
												    </div>

												    <div class="points-count-food-like focus">
												    	Текст
												    </div>

												    <div class="points-count-food-like focus">
												    	Текст
												    </div>

												    <div class="points-count-food-like focus">
												    	Текст
												    </div>

												    <div class="points-count-food-like focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-food-like -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Какую музыку вы слушаете?
			  						</span>

			  						<!-- select-sort-music -->

	  								<div class="block-select-sort-music">
	  									<div class="select-sort-music">
	  										<div id="sort-music">
	  											Выберите
	  										</div>

	  										<div class="btn-select-sort-music">
	  										</div>
	  									</div>

	  									<div class="all-block-option-sort-music">
											<div class="scroll-pane-sort-music">
												<div class="content-select-sort-music">
												    <div class="points-count-sort-music focus">
												    	Выберите
												    </div>

												    <div class="points-count-sort-music focus">
												    	Текст
												    </div>

												    <div class="points-count-sort-music focus">
												    	Текст
												    </div>

												    <div class="points-count-sort-music focus">
												    	Текст
												    </div>

												    <div class="points-count-sort-music focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-sort-music -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Каким видом спорта вы занимаетесь или любите смотреть?
			  						</span>

			  						<!-- select-kind-sports -->

	  								<div class="block-select-kind-sports">
	  									<div class="select-kind-sports">
	  										<div id="kind-sports">
	  											Выберите
	  										</div>

	  										<div class="btn-select-kind-sports">
	  										</div>
	  									</div>

	  									<div class="all-block-option-kind-sports">
											<div class="scroll-pane-kind-sports">
												<div class="content-select-kind-sports">
												    <div class="points-count-kind-sports focus">
												    	Выберите
												    </div>

												    <div class="points-count-kind-sports focus">
												    	Текст
												    </div>

												    <div class="points-count-kind-sports focus">
												    	Текст
												    </div>

												    <div class="points-count-kind-sports focus">
												    	Текст
												    </div>

												    <div class="points-count-kind-sports focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-kind-sports -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Цель знакомства
			  						</span>

			  						<!-- select-purpose-meeting -->

	  								<div class="block-select-purpose-meeting">
	  									<div class="select-purpose-meeting">
	  										<div id="purpose-meeting">
	  											Выберите
	  										</div>

	  										<div class="btn-select-purpose-meeting">
	  										</div>
	  									</div>

	  									<div class="all-block-option-purpose-meeting">
											<div class="scroll-pane-purpose-meeting">
												<div class="content-select-purpose-meeting">
												    <div class="points-count-purpose-meeting focus">
												    	Выберите
												    </div>

												    <div class="points-count-purpose-meeting focus">
												    	Текст
												    </div>

												    <div class="points-count-purpose-meeting focus">
												    	Текст
												    </div>

												    <div class="points-count-purpose-meeting focus">
												    	Текст
												    </div>

												    <div class="points-count-purpose-meeting focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-purpose-meeting -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Ориентация
			  						</span>

			  						<!-- select-orientation -->

	  								<div class="block-select-orientation">
	  									<div class="select-orientation">
	  										<div id="orientation">
	  											Выберите
	  										</div>

	  										<div class="btn-select-orientation">
	  										</div>
	  									</div>

	  									<div class="all-block-option-orientation">
											<div class="scroll-pane-orientation">
												<div class="content-select-orientation">
												    <div class="points-count-orientation focus">
												    	Выберите
												    </div>

												    <div class="points-count-orientation focus">
												    	Текст
												    </div>

												    <div class="points-count-orientation focus">
												    	Текст
												    </div>

												    <div class="points-count-orientation focus">
												    	Текст
												    </div>

												    <div class="points-count-orientation focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-orientation -->

			  					</div>

			  					<!-- end-line-block-select -->

			  					<!-- line-block-select -->

			  					<div class="line-block-select">
			  						<span class="name-chose">
			  							Религия
			  						</span>

			  						<!-- select-religion -->

	  								<div class="block-select-religion">
	  									<div class="select-religion">
	  										<div id="religion">
	  											Выберите
	  										</div>

	  										<div class="btn-select-religion">
	  										</div>
	  									</div>

	  									<div class="all-block-option-religion">
											<div class="scroll-pane-religion">
												<div class="content-select-religion">
												    <div class="points-count-religion focus">
												    	Выберите
												    </div>

												    <div class="points-count-religion focus">
												    	Текст
												    </div>

												    <div class="points-count-religion focus">
												    	Текст
												    </div>

												    <div class="points-count-religion focus">
												    	Текст
												    </div>

												    <div class="points-count-religion focus">
												    	Текст
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-religion -->

			  					</div>

			  					<!-- end-line-block-select -->

			  				</div>

	  					</form>

	  					<!-- end-registretion -->

	  				</div>

	  				<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-one-display -->

  		<!-- display-chose-photo-two -->

  		<div class="display-chose-photo-two">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

	  				<span class="btn-back">
	  					Назад
	  				</span>

	  				<!--  -->

	  				<span class="btn-next-step-two">
	  					Далее
	  				</span>

	  				<!--  -->

	  			</div>
	  		</div>
	  	</div>

	  	<!-- end-display-chose-photo-two -->


  		<!-- footer-display -->

  		<div class="footer-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

		  			<div class="footer-menu">

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					О нас
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Условия и правила
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Конфиденциальность
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Партнерам
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Отзывы
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Связаться с Нами
		  				</span>

		  				<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="block-soc-footer">

		  				<!--  -->

		  				<span class="header-soc-footer">
		  					Присоединяйся к <span>For two dating:</span>
		  				</span>

		  				<!--  -->

		  				<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-vk">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-face">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-twit">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-inst">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-yt">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-gp">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="footer-all-right">
		  				<span class="header-soc-footer">
		  					© 2016- 2016 For2date. Все права защищены. Разработка I-PR
		  				</span>
		  			</div>

		  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-footer-display -->

  	</div>

  	<!-- script -->

  	<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

  	<script type="text/javascript">
        $(".regular-first").slick({
          	dots: false,
          	autoplay: true,
           	prevArrow: false,
            nextArrow: false,
            centerMode: true,
          	infinite: true,
          	slidesToShow: 1,
          	slidesToScroll: 1,
          	autoplaySpeed: 10000
        });

        $(".regular-two").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });
    </script>

    <!-- =============================================================== -->

    <script src="js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
	    var config = {
	      '.chosen-select'           : {},
	      '.chosen-select-deselect'  : {allow_single_deselect:true},
	      '.chosen-select-no-single' : {disable_search_threshold:1},
	      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	      '.chosen-select-width'     : {width:"95%"}
	    }
	    for (var selector in config) {
	      $(selector).chosen(config[selector]);
	    }
	</script>

	<!-- =============================================================== -->

	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>

	<script type="text/javascript" src="js/jquery.jscrollpane.js"></script>

	<script type="text/javascript">

		$('.one-of-menu.message').click(function () {
            $('.all-block-dropdown-menu.message').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.message').length )
            	return;
          	$('.all-block-dropdown-menu.message').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.message').mouseleave (function() {
          $('.all-block-dropdown-menu.message').fadeOut(200);
        });

        // //////////////////////////////////////////////////////////

        $('.one-of-menu.office').click(function () {
            $('.all-block-dropdown-menu.office').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.office').length )
            	return;
          	$('.all-block-dropdown-menu.office').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.office').mouseleave (function() {
          $('.all-block-dropdown-menu.office').fadeOut(200);
        });

        // //////////////////////////////////////////////////////////

		$('.btn-login').click(function () {
            $('.block-content-registretion').fadeOut(0);
            $('.block-content-login').fadeIn(0);
            $('.btn-login').addClass('active');
            $('.btn-registration').removeClass('active');
        });

        $('.btn-registration').click(function () {
            $('.block-content-login').fadeOut(0);
            $('.block-content-registretion').fadeIn(0);
            $('.btn-registration').addClass('active');
            $('.btn-login').removeClass('active');
        });

        // //////////////////////////////////////////////////////////

		$('.select-registration-one').click(function () {
            $('.all-block-option').fadeIn(200);
            $('.scroll-pane').jScrollPane({showArrows: true});
            $('.btn-select').addClass('active');
        });

		$('.points-count.focus').click(function () {
            $('.all-block-option').fadeOut(200);
            $('.btn-select').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-one').length )
            	return;
          	$('.btn-select').removeClass('active');
          	$('.all-block-option').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-one').find(".points-count").click(function () {
		  	$('.points-count').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count.focus').click(function () {
          	var total = document.querySelector('.content-select-one .points-count.focus').innerHTML;
          	document.getElementById('total').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-two').click(function () {
            $('.all-block-option-two').fadeIn(200);
            $('.scroll-pane-two').jScrollPane({showArrows: true});
            $('.btn-select-two').addClass('active');
        });

		$('.points-count-two.focus').click(function () {
            $('.all-block-option-two').fadeOut(200);
            $('.btn-select-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-two').length )
            	return;
          	$('.btn-select-two').removeClass('active');
          	$('.all-block-option-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-two').find(".points-count-two").click(function () {
		  	$('.points-count-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-two.focus').click(function () {
          	var total = document.querySelector('.content-select-two .points-count-two.focus').innerHTML;
          	document.getElementById('total-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-three').click(function () {
            $('.all-block-option-three').fadeIn(200);
            $('.scroll-pane-three').jScrollPane({showArrows: true});
            $('.btn-select-three').addClass('active');
        });

		$('.points-count-three.focus').click(function () {
            $('.all-block-option-three').fadeOut(200);
            $('.btn-select-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-three').length )
            	return;
          	$('.btn-select-three').removeClass('active');
          	$('.all-block-option-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-three').find(".points-count-three").click(function () {
		  	$('.points-count-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-three.focus').click(function () {
          	var total = document.querySelector('.content-select-three .points-count-three.focus').innerHTML;
          	document.getElementById('total-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-lang').click(function () {
            $('.all-block-option-lang').fadeIn(200);
            // $('.scroll-pane-lang').jScrollPane({showArrows: true});
            $('.btn-select-lang').addClass('active');
        });

		$('.points-count-lang.focus').click(function () {
            $('.all-block-option-lang').fadeOut(200);
            $('.btn-select-lang').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-lang').length )
            	return;
          	$('.btn-select-lang').removeClass('active');
          	$('.all-block-option-lang').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-lang').find(".points-count-lang").click(function () {
		  	$('.points-count-lang').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-lang.focus').click(function () {
          	var total = document.querySelector('.content-select-lang .points-count-lang.focus').innerHTML;
          	document.getElementById('total-lang').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        $('.select-body-type').click(function () {
            $('.all-block-option-body-type').fadeIn(200);
            // $('.scroll-pane-body-type').jScrollPane({showArrows: true});
            $('.btn-select-body-type').addClass('active');
        });

		$('.points-count-body-type.focus').click(function () {
            $('.all-block-option-body-type').fadeOut(200);
            $('.btn-select-body-type').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-body-type').length )
            	return;
          	$('.btn-select-body-type').removeClass('active');
          	$('.all-block-option-body-type').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-body-type').find(".points-count-body-type").click(function () {
		  	$('.points-count-body-type').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-body-type.focus').click(function () {
          	var total = document.querySelector('.content-select-body-type .points-count-body-type.focus').innerHTML;
          	document.getElementById('body-type').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-ethnicity').click(function () {
            $('.all-block-option-ethnicity').fadeIn(200);
            // $('.scroll-pane-ethnicity').jScrollPane({showArrows: true});
            $('.btn-select-ethnicity').addClass('active');
        });

		$('.points-count-ethnicity.focus').click(function () {
            $('.all-block-option-ethnicity').fadeOut(200);
            $('.btn-select-ethnicity').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-ethnicity').length )
            	return;
          	$('.btn-select-ethnicity').removeClass('active');
          	$('.all-block-option-ethnicity').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-ethnicity').find(".points-count-ethnicity").click(function () {
		  	$('.points-count-ethnicity').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-ethnicity.focus').click(function () {
          	var total = document.querySelector('.content-select-ethnicity .points-count-ethnicity.focus').innerHTML;
          	document.getElementById('ethnicity').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-appearance').click(function () {
            $('.all-block-option-appearance').fadeIn(200);
            // $('.scroll-pane-appearance').jScrollPane({showArrows: true});
            $('.btn-select-appearance').addClass('active');
        });

		$('.points-count-appearance.focus').click(function () {
            $('.all-block-option-appearance').fadeOut(200);
            $('.btn-select-appearance').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-appearance').length )
            	return;
          	$('.btn-select-appearance').removeClass('active');
          	$('.all-block-option-appearance').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-appearance').find(".points-count-appearance").click(function () {
		  	$('.points-count-appearance').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-appearance.focus').click(function () {
          	var total = document.querySelector('.content-select-appearance .points-count-appearance.focus').innerHTML;
          	document.getElementById('appearance').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-hair-color').click(function () {
            $('.all-block-option-hair-color').fadeIn(200);
            // $('.scroll-pane-hair-color').jScrollPane({showArrows: true});
            $('.btn-select-hair-color').addClass('active');
        });

		$('.points-count-hair-color.focus').click(function () {
            $('.all-block-option-hair-color').fadeOut(200);
            $('.btn-select-hair-color').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-hair-color').length )
            	return;
          	$('.btn-select-hair-color').removeClass('active');
          	$('.all-block-option-hair-color').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-hair-color').find(".points-count-hair-color").click(function () {
		  	$('.points-count-hair-color').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-hair-color.focus').click(function () {
          	var total = document.querySelector('.content-select-hair-color .points-count-hair-color.focus').innerHTML;
          	document.getElementById('hair-color').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-hair-length').click(function () {
            $('.all-block-option-hair-length').fadeIn(200);
            // $('.scroll-pane-hair-length').jScrollPane({showArrows: true});
            $('.btn-select-hair-length').addClass('active');
        });

		$('.points-count-hair-length.focus').click(function () {
            $('.all-block-option-hair-length').fadeOut(200);
            $('.btn-select-hair-length').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-hair-length').length )
            	return;
          	$('.btn-select-hair-length').removeClass('active');
          	$('.all-block-option-hair-length').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-hair-length').find(".points-count-hair-length").click(function () {
		  	$('.points-count-hair-length').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-hair-length.focus').click(function () {
          	var total = document.querySelector('.content-select-hair-length .points-count-hair-length.focus').innerHTML;
          	document.getElementById('hair-length').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-hair-type').click(function () {
            $('.all-block-option-hair-type').fadeIn(200);
            // $('.scroll-pane-hair-type').jScrollPane({showArrows: true});
            $('.btn-select-hair-type').addClass('active');
        });

		$('.points-count-hair-type.focus').click(function () {
            $('.all-block-option-hair-type').fadeOut(200);
            $('.btn-select-hair-type').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-hair-type').length )
            	return;
          	$('.btn-select-hair-type').removeClass('active');
          	$('.all-block-option-hair-type').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-hair-type').find(".points-count-hair-type").click(function () {
		  	$('.points-count-hair-type').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-hair-type.focus').click(function () {
          	var total = document.querySelector('.content-select-hair-type .points-count-hair-type.focus').innerHTML;
          	document.getElementById('hair-type').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-eye-color').click(function () {
            $('.all-block-option-eye-color').fadeIn(200);
            // $('.scroll-pane-eye-color').jScrollPane({showArrows: true});
            $('.btn-select-eye-color').addClass('active');
        });

		$('.points-count-eye-color.focus').click(function () {
            $('.all-block-option-eye-color').fadeOut(200);
            $('.btn-select-eye-color').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-eye-color').length )
            	return;
          	$('.btn-select-eye-color').removeClass('active');
          	$('.all-block-option-eye-color').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-eye-color').find(".points-count-eye-color").click(function () {
		  	$('.points-count-eye-color').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-eye-color.focus').click(function () {
          	var total = document.querySelector('.content-select-eye-color .points-count-eye-color.focus').innerHTML;
          	document.getElementById('eye-color').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-glasses').click(function () {
            $('.all-block-option-glasses').fadeIn(200);
            // $('.scroll-pane-glasses').jScrollPane({showArrows: true});
            $('.btn-select-glasses').addClass('active');
        });

		$('.points-count-glasses.focus').click(function () {
            $('.all-block-option-glasses').fadeOut(200);
            $('.btn-select-glasses').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-glasses').length )
            	return;
          	$('.btn-select-glasses').removeClass('active');
          	$('.all-block-option-glasses').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-glasses').find(".points-count-glasses").click(function () {
		  	$('.points-count-glasses').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-glasses.focus').click(function () {
          	var total = document.querySelector('.content-select-glasses .points-count-glasses.focus').innerHTML;
          	document.getElementById('glasses').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-most-attractive').click(function () {
            $('.all-block-option-most-attractive').fadeIn(200);
            // $('.scroll-pane-most-attractive').jScrollPane({showArrows: true});
            $('.btn-select-most-attractive').addClass('active');
        });

		$('.points-count-most-attractive.focus').click(function () {
            $('.all-block-option-most-attractive').fadeOut(200);
            $('.btn-select-most-attractive').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-most-attractive').length )
            	return;
          	$('.btn-select-most-attractive').removeClass('active');
          	$('.all-block-option-most-attractive').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-most-attractive').find(".points-count-most-attractive").click(function () {
		  	$('.points-count-most-attractive').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-most-attractive.focus').click(function () {
          	var total = document.querySelector('.content-select-most-attractive .points-count-most-attractive.focus').innerHTML;
          	document.getElementById('most-attractive').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-body-art').click(function () {
            $('.all-block-option-body-art').fadeIn(200);
            // $('.scroll-pane-body-art').jScrollPane({showArrows: true});
            $('.btn-select-body-art').addClass('active');
        });

		$('.points-count-body-art.focus').click(function () {
            $('.all-block-option-body-art').fadeOut(200);
            $('.btn-select-body-art').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-body-art').length )
            	return;
          	$('.btn-select-body-art').removeClass('active');
          	$('.all-block-option-body-art').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-body-art').find(".points-count-body-art").click(function () {
		  	$('.points-count-body-art').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-body-art.focus').click(function () {
          	var total = document.querySelector('.content-select-body-art .points-count-body-art.focus').innerHTML;
          	document.getElementById('body-art').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-smoking').click(function () {
            $('.all-block-option-smoking').fadeIn(200);
            // $('.scroll-pane-smoking').jScrollPane({showArrows: true});
            $('.btn-select-smoking').addClass('active');
        });

		$('.points-count-smoking.focus').click(function () {
            $('.all-block-option-smoking').fadeOut(200);
            $('.btn-select-smoking').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-smoking').length )
            	return;
          	$('.btn-select-smoking').removeClass('active');
          	$('.all-block-option-smoking').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-smoking').find(".points-count-smoking").click(function () {
		  	$('.points-count-smoking').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-smoking.focus').click(function () {
          	var total = document.querySelector('.content-select-smoking .points-count-smoking.focus').innerHTML;
          	document.getElementById('smoking').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-alcohol').click(function () {
            $('.all-block-option-alcohol').fadeIn(200);
            // $('.scroll-pane-alcohol').jScrollPane({showArrows: true});
            $('.btn-select-alcohol').addClass('active');
        });

		$('.points-count-alcohol.focus').click(function () {
            $('.all-block-option-alcohol').fadeOut(200);
            $('.btn-select-alcohol').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-alcohol').length )
            	return;
          	$('.btn-select-alcohol').removeClass('active');
          	$('.all-block-option-alcohol').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-alcohol').find(".points-count-alcohol").click(function () {
		  	$('.points-count-alcohol').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-alcohol.focus').click(function () {
          	var total = document.querySelector('.content-select-alcohol .points-count-alcohol.focus').innerHTML;
          	document.getElementById('alcohol').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-change-residence').click(function () {
            $('.all-block-option-change-residence').fadeIn(200);
            // $('.scroll-pane-change-residence').jScrollPane({showArrows: true});
            $('.btn-select-change-residence').addClass('active');
        });

		$('.points-count-change-residence.focus').click(function () {
            $('.all-block-option-change-residence').fadeOut(200);
            $('.btn-select-change-residence').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-change-residence').length )
            	return;
          	$('.btn-select-change-residence').removeClass('active');
          	$('.all-block-option-change-residence').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-change-residence').find(".points-count-change-residence").click(function () {
		  	$('.points-count-change-residence').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-change-residence.focus').click(function () {
          	var total = document.querySelector('.content-select-change-residence .points-count-change-residence.focus').innerHTML;
          	document.getElementById('change-residence').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        $('.select-marital-status').click(function () {
            $('.all-block-option-marital-status').fadeIn(200);
            // $('.scroll-pane-marital-status').jScrollPane({showArrows: true});
            $('.btn-select-marital-status').addClass('active');
        });

		$('.points-count-marital-status.focus').click(function () {
            $('.all-block-option-marital-status').fadeOut(200);
            $('.btn-select-marital-status').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-marital-status').length )
            	return;
          	$('.btn-select-marital-status').removeClass('active');
          	$('.all-block-option-marital-status').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-marital-status').find(".points-count-marital-status").click(function () {
		  	$('.points-count-marital-status').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-marital-status.focus').click(function () {
          	var total = document.querySelector('.content-select-marital-status .points-count-marital-status.focus').innerHTML;
          	document.getElementById('marital-status').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-have-children').click(function () {
            $('.all-block-option-have-children').fadeIn(200);
            // $('.scroll-pane-have-children').jScrollPane({showArrows: true});
            $('.btn-select-have-children').addClass('active');
        });

		$('.points-count-have-children.focus').click(function () {
            $('.all-block-option-have-children').fadeOut(200);
            $('.btn-select-have-children').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-have-children').length )
            	return;
          	$('.btn-select-have-children').removeClass('active');
          	$('.all-block-option-have-children').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-have-children').find(".points-count-have-children").click(function () {
		  	$('.points-count-have-children').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-have-children.focus').click(function () {
          	var total = document.querySelector('.content-select-have-children .points-count-have-children.focus').innerHTML;
          	document.getElementById('have-children').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-number-of-children').click(function () {
            $('.all-block-option-number-of-children').fadeIn(200);
            // $('.scroll-pane-number-of-children').jScrollPane({showArrows: true});
            $('.btn-select-number-of-children').addClass('active');
        });

		$('.points-count-number-of-children.focus').click(function () {
            $('.all-block-option-number-of-children').fadeOut(200);
            $('.btn-select-number-of-children').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-number-of-children').length )
            	return;
          	$('.btn-select-number-of-children').removeClass('active');
          	$('.all-block-option-number-of-children').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-number-of-children').find(".points-count-number-of-children").click(function () {
		  	$('.points-count-number-of-children').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-number-of-children.focus').click(function () {
          	var total = document.querySelector('.content-select-number-of-children .points-count-number-of-children.focus').innerHTML;
          	document.getElementById('number-of-children').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-oldest-child').click(function () {
            $('.all-block-option-oldest-child').fadeIn(200);
            // $('.scroll-pane-oldest-child').jScrollPane({showArrows: true});
            $('.btn-select-oldest-child').addClass('active');
        });

		$('.points-count-oldest-child.focus').click(function () {
            $('.all-block-option-oldest-child').fadeOut(200);
            $('.btn-select-oldest-child').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-oldest-child').length )
            	return;
          	$('.btn-select-oldest-child').removeClass('active');
          	$('.all-block-option-oldest-child').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-oldest-child').find(".points-count-oldest-child").click(function () {
		  	$('.points-count-oldest-child').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-oldest-child.focus').click(function () {
          	var total = document.querySelector('.content-select-oldest-child .points-count-oldest-child.focus').innerHTML;
          	document.getElementById('oldest-child').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-youngest-child').click(function () {
            $('.all-block-option-youngest-child').fadeIn(200);
            // $('.scroll-pane-youngest-child').jScrollPane({showArrows: true});
            $('.btn-select-youngest-child').addClass('active');
        });

		$('.points-count-youngest-child.focus').click(function () {
            $('.all-block-option-youngest-child').fadeOut(200);
            $('.btn-select-youngest-child').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-youngest-child').length )
            	return;
          	$('.btn-select-youngest-child').removeClass('active');
          	$('.all-block-option-youngest-child').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-youngest-child').find(".points-count-youngest-child").click(function () {
		  	$('.points-count-youngest-child').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-youngest-child.focus').click(function () {
          	var total = document.querySelector('.content-select-youngest-child .points-count-youngest-child.focus').innerHTML;
          	document.getElementById('youngest-child').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-want-children').click(function () {
            $('.all-block-option-want-children').fadeIn(200);
            // $('.scroll-pane-want-children').jScrollPane({showArrows: true});
            $('.btn-select-want-children').addClass('active');
        });

		$('.points-count-want-children.focus').click(function () {
            $('.all-block-option-want-children').fadeOut(200);
            $('.btn-select-want-children').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-want-children').length )
            	return;
          	$('.btn-select-want-children').removeClass('active');
          	$('.all-block-option-want-children').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-want-children').find(".points-count-want-children").click(function () {
		  	$('.points-count-want-children').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-want-children.focus').click(function () {
          	var total = document.querySelector('.content-select-want-children .points-count-want-children.focus').innerHTML;
          	document.getElementById('want-children').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-have-pets').click(function () {
            $('.all-block-option-have-pets').fadeIn(200);
            // $('.scroll-pane-have-pets').jScrollPane({showArrows: true});
            $('.btn-select-have-pets').addClass('active');
        });

		$('.points-count-have-pets.focus').click(function () {
            $('.all-block-option-have-pets').fadeOut(200);
            $('.btn-select-have-pets').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-have-pets').length )
            	return;
          	$('.btn-select-have-pets').removeClass('active');
          	$('.all-block-option-have-pets').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-have-pets').find(".points-count-have-pets").click(function () {
		  	$('.points-count-have-pets').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-have-pets.focus').click(function () {
          	var total = document.querySelector('.content-select-have-pets .points-count-have-pets.focus').innerHTML;
          	document.getElementById('have-pets').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-profession').click(function () {
            $('.all-block-option-profession').fadeIn(200);
            // $('.scroll-pane-profession').jScrollPane({showArrows: true});
            $('.btn-select-profession').addClass('active');
        });

		$('.points-count-profession.focus').click(function () {
            $('.all-block-option-profession').fadeOut(200);
            $('.btn-select-profession').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-profession').length )
            	return;
          	$('.btn-select-profession').removeClass('active');
          	$('.all-block-option-profession').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-profession').find(".points-count-profession").click(function () {
		  	$('.points-count-profession').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-profession.focus').click(function () {
          	var total = document.querySelector('.content-select-profession .points-count-profession.focus').innerHTML;
          	document.getElementById('profession').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-work-status').click(function () {
            $('.all-block-option-work-status').fadeIn(200);
            // $('.scroll-pane-work-status').jScrollPane({showArrows: true});
            $('.btn-select-work-status').addClass('active');
        });

		$('.points-count-work-status.focus').click(function () {
            $('.all-block-option-work-status').fadeOut(200);
            $('.btn-select-work-status').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-work-status').length )
            	return;
          	$('.btn-select-work-status').removeClass('active');
          	$('.all-block-option-work-status').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-work-status').find(".points-count-work-status").click(function () {
		  	$('.points-count-work-status').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-work-status.focus').click(function () {
          	var total = document.querySelector('.content-select-work-status .points-count-work-status.focus').innerHTML;
          	document.getElementById('work-status').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-type-property').click(function () {
            $('.all-block-option-type-property').fadeIn(200);
            // $('.scroll-pane-type-property').jScrollPane({showArrows: true});
            $('.btn-select-type-property').addClass('active');
        });

		$('.points-count-type-property.focus').click(function () {
            $('.all-block-option-type-property').fadeOut(200);
            $('.btn-select-type-property').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-type-property').length )
            	return;
          	$('.btn-select-type-property').removeClass('active');
          	$('.all-block-option-type-property').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-type-property').find(".points-count-type-property").click(function () {
		  	$('.points-count-type-property').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-type-property.focus').click(function () {
          	var total = document.querySelector('.content-select-type-property .points-count-type-property.focus').innerHTML;
          	document.getElementById('type-property').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-housing-situation').click(function () {
            $('.all-block-option-housing-situation').fadeIn(200);
            // $('.scroll-pane-housing-situation').jScrollPane({showArrows: true});
            $('.btn-select-housing-situation').addClass('active');
        });

		$('.points-count-housing-situation.focus').click(function () {
            $('.all-block-option-housing-situation').fadeOut(200);
            $('.btn-select-housing-situation').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-housing-situation').length )
            	return;
          	$('.btn-select-housing-situation').removeClass('active');
          	$('.all-block-option-housing-situation').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-housing-situation').find(".points-count-housing-situation").click(function () {
		  	$('.points-count-housing-situation').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-housing-situation.focus').click(function () {
          	var total = document.querySelector('.content-select-housing-situation .points-count-housing-situation.focus').innerHTML;
          	document.getElementById('housing-situation').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-primary-language').click(function () {
            $('.all-block-option-primary-language').fadeIn(200);
            // $('.scroll-pane-primary-language').jScrollPane({showArrows: true});
            $('.btn-select-primary-language').addClass('active');
        });

		$('.points-count-primary-language.focus').click(function () {
            $('.all-block-option-primary-language').fadeOut(200);
            $('.btn-select-primary-language').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-primary-language').length )
            	return;
          	$('.btn-select-primary-language').removeClass('active');
          	$('.all-block-option-primary-language').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-primary-language').find(".points-count-primary-language").click(function () {
		  	$('.points-count-primary-language').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-primary-language.focus').click(function () {
          	var total = document.querySelector('.content-select-primary-language .points-count-primary-language.focus').innerHTML;
          	document.getElementById('primary-language').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-languages-know').click(function () {
            $('.all-block-option-languages-know').fadeIn(200);
            // $('.scroll-pane-languages-know').jScrollPane({showArrows: true});
            $('.btn-select-languages-know').addClass('active');
        });

		$('.points-count-languages-know.focus').click(function () {
            $('.all-block-option-languages-know').fadeOut(200);
            $('.btn-select-languages-know').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-languages-know').length )
            	return;
          	$('.btn-select-languages-know').removeClass('active');
          	$('.all-block-option-languages-know').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-languages-know').find(".points-count-languages-know").click(function () {
		  	$('.points-count-languages-know').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-languages-know.focus').click(function () {
          	var total = document.querySelector('.content-select-languages-know .points-count-languages-know.focus').innerHTML;
          	document.getElementById('languages-know').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-zodiac').click(function () {
            $('.all-block-option-zodiac').fadeIn(200);
            // $('.scroll-pane-zodiac').jScrollPane({showArrows: true});
            $('.btn-select-zodiac').addClass('active');
        });

		$('.points-count-zodiac.focus').click(function () {
            $('.all-block-option-zodiac').fadeOut(200);
            $('.btn-select-zodiac').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-zodiac').length )
            	return;
          	$('.btn-select-zodiac').removeClass('active');
          	$('.all-block-option-zodiac').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-zodiac').find(".points-count-zodiac").click(function () {
		  	$('.points-count-zodiac').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-zodiac.focus').click(function () {
          	var total = document.querySelector('.content-select-zodiac .points-count-zodiac.focus').innerHTML;
          	document.getElementById('zodiac').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-have-car').click(function () {
            $('.all-block-option-have-car').fadeIn(200);
            // $('.scroll-pane-have-car').jScrollPane({showArrows: true});
            $('.btn-select-have-car').addClass('active');
        });

		$('.points-count-have-car.focus').click(function () {
            $('.all-block-option-have-car').fadeOut(200);
            $('.btn-select-have-car').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-have-car').length )
            	return;
          	$('.btn-select-have-car').removeClass('active');
          	$('.all-block-option-have-car').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-have-car').find(".points-count-have-car").click(function () {
		  	$('.points-count-have-car').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-have-car.focus').click(function () {
          	var total = document.querySelector('.content-select-have-car .points-count-have-car.focus').innerHTML;
          	document.getElementById('have-car').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-education').click(function () {
            $('.all-block-option-education').fadeIn(200);
            // $('.scroll-pane-education').jScrollPane({showArrows: true});
            $('.btn-select-education').addClass('active');
        });

		$('.points-count-education.focus').click(function () {
            $('.all-block-option-education').fadeOut(200);
            $('.btn-select-education').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-education').length )
            	return;
          	$('.btn-select-education').removeClass('active');
          	$('.all-block-option-education').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-education').find(".points-count-education").click(function () {
		  	$('.points-count-education').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-education.focus').click(function () {
          	var total = document.querySelector('.content-select-education .points-count-education.focus').innerHTML;
          	document.getElementById('education').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-do-for-fun').click(function () {
            $('.all-block-option-do-for-fun').fadeIn(200);
            // $('.scroll-pane-do-for-fun').jScrollPane({showArrows: true});
            $('.btn-select-do-for-fun').addClass('active');
        });

		$('.points-count-do-for-fun.focus').click(function () {
            $('.all-block-option-do-for-fun').fadeOut(200);
            $('.btn-select-do-for-fun').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-do-for-fun').length )
            	return;
          	$('.btn-select-do-for-fun').removeClass('active');
          	$('.all-block-option-do-for-fun').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-do-for-fun').find(".points-count-do-for-fun").click(function () {
		  	$('.points-count-do-for-fun').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-do-for-fun.focus').click(function () {
          	var total = document.querySelector('.content-select-do-for-fun .points-count-do-for-fun.focus').innerHTML;
          	document.getElementById('do-for-fun').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-food-like').click(function () {
            $('.all-block-option-food-like').fadeIn(200);
            // $('.scroll-pane-food-like').jScrollPane({showArrows: true});
            $('.btn-select-food-like').addClass('active');
        });

		$('.points-count-food-like.focus').click(function () {
            $('.all-block-option-food-like').fadeOut(200);
            $('.btn-select-food-like').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-food-like').length )
            	return;
          	$('.btn-select-food-like').removeClass('active');
          	$('.all-block-option-food-like').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-food-like').find(".points-count-food-like").click(function () {
		  	$('.points-count-food-like').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-food-like.focus').click(function () {
          	var total = document.querySelector('.content-select-food-like .points-count-food-like.focus').innerHTML;
          	document.getElementById('food-like').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-sort-music').click(function () {
            $('.all-block-option-sort-music').fadeIn(200);
            // $('.scroll-pane-sort-music').jScrollPane({showArrows: true});
            $('.btn-select-sort-music').addClass('active');
        });

		$('.points-count-sort-music.focus').click(function () {
            $('.all-block-option-sort-music').fadeOut(200);
            $('.btn-select-sort-music').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-sort-music').length )
            	return;
          	$('.btn-select-sort-music').removeClass('active');
          	$('.all-block-option-sort-music').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-sort-music').find(".points-count-sort-music").click(function () {
		  	$('.points-count-sort-music').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-sort-music.focus').click(function () {
          	var total = document.querySelector('.content-select-sort-music .points-count-sort-music.focus').innerHTML;
          	document.getElementById('sort-music').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-kind-sports').click(function () {
            $('.all-block-option-kind-sports').fadeIn(200);
            // $('.scroll-pane-kind-sports').jScrollPane({showArrows: true});
            $('.btn-select-kind-sports').addClass('active');
        });

		$('.points-count-kind-sports.focus').click(function () {
            $('.all-block-option-kind-sports').fadeOut(200);
            $('.btn-select-kind-sports').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-kind-sports').length )
            	return;
          	$('.btn-select-kind-sports').removeClass('active');
          	$('.all-block-option-kind-sports').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-kind-sports').find(".points-count-kind-sports").click(function () {
		  	$('.points-count-kind-sports').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-kind-sports.focus').click(function () {
          	var total = document.querySelector('.content-select-kind-sports .points-count-kind-sports.focus').innerHTML;
          	document.getElementById('kind-sports').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-purpose-meeting').click(function () {
            $('.all-block-option-purpose-meeting').fadeIn(200);
            // $('.scroll-pane-purpose-meeting').jScrollPane({showArrows: true});
            $('.btn-select-purpose-meeting').addClass('active');
        });

		$('.points-count-purpose-meeting.focus').click(function () {
            $('.all-block-option-purpose-meeting').fadeOut(200);
            $('.btn-select-purpose-meeting').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-purpose-meeting').length )
            	return;
          	$('.btn-select-purpose-meeting').removeClass('active');
          	$('.all-block-option-purpose-meeting').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-purpose-meeting').find(".points-count-purpose-meeting").click(function () {
		  	$('.points-count-purpose-meeting').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-purpose-meeting.focus').click(function () {
          	var total = document.querySelector('.content-select-purpose-meeting .points-count-purpose-meeting.focus').innerHTML;
          	document.getElementById('purpose-meeting').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-orientation').click(function () {
            $('.all-block-option-orientation').fadeIn(200);
            // $('.scroll-pane-orientation').jScrollPane({showArrows: true});
            $('.btn-select-orientation').addClass('active');
        });

		$('.points-count-orientation.focus').click(function () {
            $('.all-block-option-orientation').fadeOut(200);
            $('.btn-select-orientation').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-orientation').length )
            	return;
          	$('.btn-select-orientation').removeClass('active');
          	$('.all-block-option-orientation').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-orientation').find(".points-count-orientation").click(function () {
		  	$('.points-count-orientation').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-orientation.focus').click(function () {
          	var total = document.querySelector('.content-select-orientation .points-count-orientation.focus').innerHTML;
          	document.getElementById('orientation').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-religion').click(function () {
            $('.all-block-option-religion').fadeIn(200);
            // $('.scroll-pane-religion').jScrollPane({showArrows: true});
            $('.btn-select-religion').addClass('active');
        });

		$('.points-count-religion.focus').click(function () {
            $('.all-block-option-religion').fadeOut(200);
            $('.btn-select-religion').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-religion').length )
            	return;
          	$('.btn-select-religion').removeClass('active');
          	$('.all-block-option-religion').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-religion').find(".points-count-religion").click(function () {
		  	$('.points-count-religion').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-religion.focus').click(function () {
          	var total = document.querySelector('.content-select-religion .points-count-religion.focus').innerHTML;
          	document.getElementById('religion').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////



    </script>

    <script type="text/javascript">
      $(document).ready(function() {
          $('.minus-number-way').click(function () {
              var $input = $(this).parent().find('input');
              var count = parseInt($input.val()) - 1;
              count = count < 1 ? 0 : count;
              $input.val(count);
              $input.change();
              return false;
          });
          $('.plus-number-way').click(function () {
              var $input = $(this).parent().find('input');
              $input.val(parseInt($input.val()) + 1);
              $input.change();
              return false;
          });
      });
    </script>

    <script type="text/javascript">

            $(document).ready(function(){

            $(".btn-mobile-open").click(function(){
                $(".btn-mobile-open").fadeOut(0);
                $(".under-header").animate({'width': "+=250px"}, 500);
                $(".under-header").css({'border': "2px solid white"});
                $(".btn-mobile-close").fadeIn(200);
            });

            $(".btn-mobile-close").click(function(){
                $(".under-header").animate({'width': "-=250px"}, 500);
                $(".under-header").css({'border': "0px"});
                $(".btn-mobile-close").fadeOut(0);
                $(".btn-mobile-open").fadeIn(200);
            });

            });
    </script>



	<!-- =============================================================== -->

  	<!-- end-script -->

    <script src="js/form-validation.js"></script>

  </body>
</html>
