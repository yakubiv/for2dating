(function (jQuery) {
  var $ = jQuery;

  $(document).ready(function () {
    $('.input-registration').change(function () {
      $(this).addClass('edited');
    });

    // index
    var passwordCheck = function () {
      $passwordCheck = $("#register-password-check");
      if ($passwordCheck.val() === $('#register-password').val()) {
        $passwordCheck.removeClass('invalid').addClass('valid');
      } else {
        $passwordCheck.removeClass('valid').addClass('invalid');
      }
    }
    $('#register-password').keyup(passwordCheck);
    $('#register-password-check').keyup(passwordCheck);

    // auto-payments
    $('.input-card').keydown(function (event) {
      if (event.keyCode >= 32 && (event.key < '0' || event.key > '9')) return false;

      $this = $(this);
      if ($this.val().length >= Number($this.attr('maxlength'))) {
        var $next = $this.parent().next();
        if ($next) $next.find('input').focus();
        $next.focus();
      } else if ($this.val().length === 0 && event.keyCode === 8) {
        var $prev = $this.parent().prev();
        if ($prev) $prev.find('input').focus();
      }
    });

    // perfect-pair-step-first
    // uploading images using ajax
    $('#form-search-file').change(function () {
      $this = $(this);
      $this.parent().find('input.input-search').val($this.val());
      if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#form-search-image')
            .attr('src', e.target.result)
        };
        reader.readAsDataURL(this.files[0]);
      }
    });
  });
})(window.jQuery);
