<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity perfect-pair-step-first">
  		
  		<!-- baground-slider -->

  		<div class="null-block">

  			<!-- slider-display-bg -->

	        <div class="slider-display-one" style="display:none;">
	            <div class="regular-first slider">
		            <!--  -->
		            <div>
		                <div class="into-slider-one one">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one two">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one three">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one four">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one five">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one six">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one seven">
		                </div>                  
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one eight">
		                </div>                  
		            </div>
		            <!--  -->
	        	</div>
	    	</div>

	    	<!-- end-slider-display-bg -->
  				
  		</div>

  		<!-- end-baground-slider -->

  		<!-- header -->

  		<div class="left-one-bg-header">
  			<div class="left-two-bg-header">
	  			<div class="center-all-block">
	  				<div class="left-center-all-block">

	  					<!--  -->

	  					<div class="over-header">

			  				<div class="block-logo-header">
			  					<a href="#">
			  						<div class="logo-header">		  							
			  						</div>
			  					</a>
			  				</div>

			  				<!--  -->

			  				<div class="block-soc-header">
			  					<span>
			  						Мы в
			  					</span>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-vk">		  								
			  							</div>
			  						</a>
			  					</div>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-face">		  								
			  							</div>
			  						</a>
			  					</div>
			  				</div>

			  				<!-- lang-select -->
	  								
	  						<div class="block-select-registration-lang">
	  							<div class="select-registration-lang">
	  								<div id="total-lang">
	  									<div class="flag-one-of-lang">			  							
						  				</div>

						  				<span>
						  					Русский
						  				</span>
	  								</div>

	  								<div class="btn-select-lang">	  											
	  								</div>
	  							</div>

	  							<div class="all-block-option-lang">
									<div class="scroll-pane-lang">
										<div class="content-select-lang">
										    <div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Русский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Английский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">			  							
								  				</div>

								  				<span>
								  					Норвежский
								  				</span>
										   	</div>
									    </div>
									</div>
								</div>
	  						</div>
	  								
	  						<!-- end-lang-select -->

			  				<!-- account -->

			  				<div class="block-account">
			  					<div class="block-coints">
			  						<span class="coints">
			  							0
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-user-header">
			  						<div class="border-photo-user-header">
			  							<img src="img/photo-user-header.png" alt="not-image">
			  						</div>

			  						<span>
			  							Александр
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-setting">
			  						<div class="icon-setting">			  							
			  						</div>
			  					</div>

			  					<!--  -->

			  					<div class="block-icon-enter-header">
			  						<div class="icon-enter-header">			  							
			  						</div>
			  					</div>

			  					<!--  -->

			  				</div>

			  				<!-- end-account -->

		  				</div>

		  				<!--  -->

		  				<!-- mobile-menu -->

		  				<div class="null-block">
		  					<div class="null-block">
		  						<div class="btn-mobile-open">
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="btn-mobile-close">
		  						</div>
		  					</div>
		  				</div>

		  				<!-- end-mobile-menu -->

		  				<!--  -->

		  				<div class="under-header">

		  					<!--  -->

		  					<div class="one-of-menu office">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Мой кабинет
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu office">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои альбомы
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Моя статистика
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой пакет услуг
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой счет
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои статьи
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои подарки
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Пригласи друга
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои настройки
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu message">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Уведомления
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>			  					
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">		  								
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="block-number-message">
		  								<span>
		  									648
		  								</span>
		  							</div>
		  						</div>
		  						<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu message">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои сообщения <span>(9)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подмигиваний <span>(315)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Просмотры <span>(49)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Избранные <span>(5)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подарки <span>(3)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Симпатии <span>(5)</span>
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">
		  									
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Блог
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Карусель
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->
		  					
		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							ТОП 100
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Поиск
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">		  								
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Идеальная пара
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">		  								
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  				</div>

		  				<!--  -->
	  					
	  				</div>
	  			</div>
	  		</div>
  		</div>

  		<!-- end-header -->

  		<!-- slider-under-header -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  			<!--  -->

	  			<div class="block-slider-under-header">
	  				<div class="all-slider-under-header">

	  					<div class="block-trap-photo">
	  						<span class="bold">
	  							Ловушка:
	  						</span>

	  						<span class="regular">
	  							Попасть<br>
	  							в ленту
	  						</span>
	  					</div>

	  					<!--  -->

				        <div class="slider-display-under-header">
				            <div class="regular-under-header slider-under-header">
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/one-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/two-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/three-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/four-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/five-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>                  
					            </div>
					            <!--  -->
				        	</div>
				    	</div>

				    	<!--  -->

	  				</div>
	  			</div>

	  			<!--  -->

	  			</div>
	  		</div>  			
  		</div>

  		<!-- end-slider-under-header -->

  		<!-- content -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<div class="content-my-account">
	  					
	  					<!--  -->

	  					<span class="header-content-my-account package-page">
	  						Мой пакет услуг
	  					</span>

	  					<!--  -->

	  					<div class="block-content-my-package">	  

	  						<!-- block-left-package -->
		  					
		  					<div class="block-left-package">
		  						<div class="center-left-package">

		  							<span class="header-left-package">
		  								Ваш пакет "Базовый"
		  							</span>

		  							<div class="content-left-package">
		  								<span class="header-content-left-package">
		  									Сегодня осталось:
		  								</span>

		  								<span class="point-content-left-package">
		  									Просмотр анкет - <span class="pink">3</span>
		  								</span>

		  								<span class="point-content-left-package">
		  									Исходящих сообщений - <span class="pink">1</span>
		  								</span>

		  								<span class="point-content-left-package">
		  									Подмигивание - <span class="pink">2</span>
		  								</span>

		  								<span class="point-content-left-package">
		  									Проявить симпатию - <span class="pink">7</span>
		  								</span>		  								
		  							</div>

		  							<div class="block-btn-left-package">
		  								<span>
		  									Переходите с выгодо до 30% в пакет "Платина"
		  								</span>

		  								<span class="violet">
		  									Купить?
		  								</span>

		  							</div>
		  						</div>

		  						<div class="null-block">
		  							<div class="bg-left-package-down">		  								
		  							</div>
		  						</div>
		  					</div>

		  					<!-- end-block-left-package -->

		  					<!-- block-right-package -->
		  					
		  					<div class="block-right-package">
		  						<div class="center-right-package">

		  							<span class="header-right-package">
		  								Дополнительные услуги
		  							</span>

		  							<div class="content-right-package">

		  								<!--  -->

		  								<div class="line-content-right-package first">
		  									<span class="point-content-right-package">
		  										
		  									</span>

			  								<span class="border-content-right-package">
			  									
			  								</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-content-right-package">
		  									<span class="point-content-right-package">
			  									Стать лидером (фото+сообщение)	
			  								</span>

			  								<span class="border-content-right-package">
			  									1 монета
			  								</span>

			  								<span class="pink-content-right-package">
			  									Активно до 16.09.2016
			  								</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-content-right-package">
		  									<span class="point-content-right-package">
			  									Попасть в ленту (под главным меню)	
			  								</span>

			  								<span class="border-content-right-package">
			  									3 монета
			  								</span>

			  								<span class="pink-content-right-package">
			  									подробнее
			  								</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-content-right-package">
		  									<span class="point-content-right-package">
			  									Подняться на странице поиска
			  								</span>

			  								<span class="border-content-right-package">
			  									3 монета
			  								</span>

			  								<span class="pink-content-right-package">
			  									подробнее
			  								</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-content-right-package">
		  									<span class="point-content-right-package">
			  									Дополнительный пакет показов в "Карусели"	
			  								</span>

			  								<span class="border-content-right-package">
			  									5 монета
			  								</span>

			  								<span class="pink-content-right-package">
			  									подробнее
			  								</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-content-right-package">
		  									<span class="point-content-right-package">
			  									Попасть в ТОП	
			  								</span>

			  								<span class="border-content-right-package">
			  									10 монета
			  								</span>

			  								<span class="pink-content-right-package">
			  									подробнее
			  								</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-content-right-package">
		  									<span class="point-content-right-package">
			  									Статус невидимка (фото размыто)	
			  								</span>

			  								<span class="border-content-right-package">
			  									10 монета
			  								</span>

			  								<span class="pink-content-right-package">
			  									подробнее
			  								</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-content-right-package">
		  									<span class="point-content-right-package">
			  									Статус инкогнито
			  								</span>

			  								<span class="border-content-right-package">
			  									25 монета
			  								</span>

			  								<span class="pink-content-right-package">
			  									подробнее
			  								</span>
		  								</div>

		  								<!--  -->

		  								<div class="line-content-right-package">
		  									<span class="point-content-right-package">
			  									Дополнительный пакет просмотра анкет и сообщений
			  								</span>

			  								<span class="border-content-right-package last">
			  									50 монета
			  								</span>

			  								<span class="pink-content-right-package">
			  									подробнее
			  								</span>
		  								</div>

		  								<!--  -->
		  							</div>
		  						</div>

		  						<div class="null-block">
		  							<div class="bg-right-package-down">
			  							<!--  -->

			  							<div class="line-content-right-package last">
				  							<span class="border-content-right-package">
				  									
				  							</span>
			  							</div>

			  							<!--  -->
		  							</div>
		  						</div>
		  					</div>

		  					<!-- end-block-right-package -->

		  					<!-- text-under-left-right-package -->

		  					<div class="text-under-left-right-package">
		  						<span class="regular">
		  							Ваш выбранный пакет, срок действия:
		  						</span><br>
		  						<span class="infinity">
		  							с 1.01.2016 до
		  						</span>		  						
		  					</div>

		  					<!-- end-text-under-left-right-package -->

		  					<!-- block-four-package -->

		  					<div class="block-four-package">

		  						<!-- one-of-four-package -->

		  						<div class="one-of-four-package basic">

		  							<!--  -->

		  							<span class="header-one-of-four-package">
		  								Пакет "Базовый"
		  							</span>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр анкет - <span class="bold">5 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр фото - <span class="bold">основное фото профиля, без возможности увеличить фото</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр альбомов - <span class="bold">нет</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр информации о другом пользователе - <span class="bold">краткая информация</span>
		  								</span>

		  								<div class="icon-info">
		  									<div class="null-block">
		  										<span class="block-info-more">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Исходящие сообщения - <span class="bold">5 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Подмигивание - <span class="bold">5 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Проявить симпатию в "Карусели" - <span class="bold">10 в день</span>
		  								</span>

		  								<div class="icon-info">		  
		  									<div class="null-block">
		  										<span class="block-info-more">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Показы в "Карусели" - <span class="bold">только для тех пользователей, кто нравится</span>
		  								</span>

		  								<div class="icon-info">
		  									<div class="null-block">
		  										<span class="block-info-more">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>	  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Добавить в избранное - <span class="bold">нет возможности</span>
		  								</span>

		  								<div class="icon-info">
		  									<div class="null-block">
		  										<span class="block-info-more">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>		  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр у кого я в избранных - <span class="bold">нет возможности</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<span class="free-pay">
		  								Бесплатно
		  							</span>

		  							<!--  -->

		  							<div class="more-on-of-four-package">
		  								<span>
		  									Подробнее
		  								</span>
		  							</div>

		  						</div>

		  						<!-- end-one-of-four-package -->

		  						<!-- one-of-four-package -->

		  						<div class="one-of-four-package silver">

		  							<div class="null-block">
		  								<div class="block-akciy-top">
		  									<span class="around-percent first">
		  										%
		  									</span>
		  								</div>
		  							</div>

		  							<!--  -->

		  							<span class="header-one-of-four-package">
		  								Пакет "Серебро"
		  							</span>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр анкет - <span class="bold">10 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр фото - <span class="bold">три фото профиля, с возможностью увеличить фото</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр альбомов - <span class="bold">нет</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр информации о другом пользователе - <span class="bold">неограниченно</span>
		  								</span>

		  								<div class="icon-info">
		  									<div class="null-block">
		  										<span class="block-info-more">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>		  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Исходящие сообщения - <span class="bold">10 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Подмигивание - <span class="bold">10 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Проявить симпатию в "Карусели" - <span class="bold">20 в день</span>
		  								</span>

		  								<div class="icon-info">
		  									<div class="null-block">
		  										<span class="block-info-more">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>		  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Показы в "Карусели" - <span class="bold">дополнительно 20 в день</span>
		  								</span>

		  								<div class="icon-info">
		  									<div class="null-block">
		  										<span class="block-info-more">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>		  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Добавить в избранное - <span class="bold">5 анкет</span>
		  								</span>

		  								<div class="icon-info">
		  									<div class="null-block">
		  										<span class="block-info-more">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>		  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр у кого я в избранных - <span class="bold">нет возможности</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="block-buy-period">

		  								<!--  -->

		  								<div class="one-of-buy-period">
		  									<span class="bold">
		  										12 месяцев....100 монет <span class="regular">(экономия 20 монет)</span>
		  									</span>

		  									<span class="btn-buy-period">
		  										Купить
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="one-of-buy-period">
		  									<span class="bold">
		  										3 месяца.........25 монет <span class="regular">(экономия 5 монет)</span>
		  									</span>

		  									<span class="btn-buy-period">
		  										Купить
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="one-of-buy-period">
		  									<span class="bold">
		  										1 месяц............10 монет
		  									</span>

		  									<span class="btn-buy-period">
		  										Купить
		  									</span>
		  								</div>

		  								<!--  -->

		  							</div>

		  							<!--  -->

		  							<div class="more-on-of-four-package">
		  								<span>
		  									Подробнее
		  								</span>
		  							</div>

		  						</div>

		  						<!-- end-one-of-four-package -->

		  						<!-- one-of-four-package -->

		  						<div class="one-of-four-package gold">

		  							<div class="null-block">
		  								<div class="block-akciy-top">
		  									<span class="around-akciy">
		  										акция
		  									</span>

		  									<span class="around-percent">
		  										%
		  									</span>
		  								</div>
		  							</div>

		  							<!--  -->

		  							<span class="header-one-of-four-package">
		  								Пакет "Золото"
		  							</span>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр анкет - <span class="bold">20 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр фото - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр альбомов - <span class="bold">да</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр информации о другом пользователе - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Исходящие сообщения - <span class="bold">20 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Подмигивание - <span class="bold">20 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Проявить симпатию в "Карусели" - <span class="bold">50 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Показы в "Карусели" - <span class="bold">дополнительно 50 в день</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Добавить в избранное - <span class="bold">максимально 20 анкет можно добавить</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр у кого я в избранных - <span class="bold">просмотр только кол-во добавлений</span>
		  								</span>

		  								<div class="icon-info">
		  									<div class="null-block">
		  										<span class="block-info-more right">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>		  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="block-buy-period gold">

		  								<!--  -->

		  								<div class="one-of-buy-period">
		  									<span class="bold">
		  										12 месяцев....100 монет <span class="regular">(экономия 20 монет)</span>
		  									</span>

		  									<span class="btn-buy-period">
		  										Купить
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="one-of-buy-period">
		  									<span class="bold">
		  										3 месяца.........25 монет <span class="regular">(экономия 5 монет)</span>
		  									</span>

		  									<span class="btn-buy-period">
		  										Купить
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="one-of-buy-period">
		  									<span class="bold">
		  										1 месяц............10 монет
		  									</span>

		  									<span class="btn-buy-period">
		  										Купить
		  									</span>
		  								</div>

		  								<!--  -->

		  							</div>

		  							<!--  -->

		  							<div class="more-on-of-four-package">
		  								<span>
		  									Подробнее
		  								</span>
		  							</div>

		  						</div>

		  						<!-- end-one-of-four-package -->

		  						<!-- one-of-four-package -->

		  						<div class="one-of-four-package platinum">

		  							<div class="null-block">
		  								<div class="block-akciy-top">
		  									<span class="around-akciy">
		  										акция
		  									</span>

		  									<span class="around-percent">
		  										%
		  									</span>

		  									<span class="around-top">
		  										ТОП
		  									</span>
		  								</div>
		  							</div>

		  							<!--  -->

		  							<span class="header-one-of-four-package">
		  								Пакет "Платина"
		  							</span>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр анкет - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр фото - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр альбомов - <span class="bold">да</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр информации о другом пользователе - <span class="bold">неограниченно + возможность настройки отображаемой информации самостоятельно</span>
		  								</span>

		  								<div class="icon-info">
		  									<div class="null-block">
		  										<span class="block-info-more right">
		  											Краткая информация: имя, пол, возраст,
													кем интересуется, страна, удаленность,
													семейное положение, ориентация и цель
													знакомства
		  										</span>
		  									</div>  									
		  								</div>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Исходящие сообщения - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Подмигивание - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Проявить симпатию в "Карусели" - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Показы в "Карусели" - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Добавить в избранное - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="line-info-one-of-four-package">
		  								<span class="regular">
		  									Просмотр у кого я в избранных - <span class="bold">неограниченно</span>
		  								</span>
		  							</div>

		  							<!--  -->

		  							<div class="block-buy-period platinum">

		  								<!--  -->

		  								<div class="one-of-buy-period">
		  									<span class="bold">
		  										12 месяцев....100 монет <span class="regular">(экономия 20 монет)</span>
		  									</span>

		  									<span class="btn-buy-period">
		  										Купить
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="one-of-buy-period">
		  									<span class="bold">
		  										3 месяца.........25 монет <span class="regular">(экономия 5 монет)</span>
		  									</span>

		  									<span class="btn-buy-period">
		  										Купить
		  									</span>
		  								</div>

		  								<!--  -->

		  								<div class="one-of-buy-period">
		  									<span class="bold">
		  										1 месяц............10 монет
		  									</span>

		  									<span class="btn-buy-period">
		  										Купить
		  									</span>
		  								</div>

		  								<!--  -->

		  							</div>

		  							<!--  -->

		  							<div class="more-on-of-four-package">
		  								<span>
		  									Подробнее
		  								</span>
		  							</div>

		  						</div>

		  						<!-- end-one-of-four-package -->

		  					</div>

		  					<!-- end-block-four-package -->

	  					</div>

	  				</div>

	  			</div>
	  		</div>
  		</div>

  		<!-- end-content -->

  		<!-- footer-display -->

  		<div class="footer-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

		  			<div class="footer-menu">

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					О нас
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Условия и правила
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Конфиденциальность
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Партнерам
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Отзывы
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Связаться с Нами
		  				</span>

		  				<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="block-soc-footer">

		  				<!--  -->

		  				<span class="header-soc-footer">
		  					Присоединяйся к <span>For two dating:</span>
		  				</span>

		  				<!--  -->

		  				<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-vk">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-face">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-twit">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-inst">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-yt">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-gp">		  								
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="footer-all-right">
		  				<span class="header-soc-footer">
		  					© 2016- 2016 For2date. Все права защищены. Разработка I-PR
		  				</span>
		  			</div>

		  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-footer-display -->

  	</div>

  	<!-- script -->

  	<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

  	<script type="text/javascript">
        $(".regular-first").slick({
          	dots: false,
          	autoplay: true,
           	prevArrow: false,
            nextArrow: false,
            centerMode: true,
          	infinite: true,
          	slidesToShow: 1,
          	slidesToScroll: 1,
          	autoplaySpeed: 10000
        });

        $(".regular-two").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });

        $(".regular-gift-for-user").slick({
          infinite: true,
          // centerMode: true,
          slidesToShow: 3,
          slidesToScroll: 3
        });

        $(".regular-carousel").slick({
          	dots: false,
          	// autoplay: true,
            // centerMode: true,
          	// autoplaySpeed: 5000,
          	infinite: true,
          	variableWidth: true,
          	slidesToShow: 1,
          	slidesToScroll: 1
        });
        
    </script>

    <!-- =============================================================== -->

    <script src="js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
	    var config = {
	      '.chosen-select'           : {},
	      '.chosen-select-deselect'  : {allow_single_deselect:true},
	      '.chosen-select-no-single' : {disable_search_threshold:1},
	      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	      '.chosen-select-width'     : {width:"95%"}
	    }
	    for (var selector in config) {
	      $(selector).chosen(config[selector]);
	    }
	</script>

	<!-- =============================================================== -->

	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>

	<script type="text/javascript" src="js/jquery.jscrollpane.js"></script>

	<script type="text/javascript">

		$('.one-of-menu.message').click(function () {
            $('.all-block-dropdown-menu.message').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.message').length ) 
            	return;
          	$('.all-block-dropdown-menu.message').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.message').mouseleave (function() {
          $('.all-block-dropdown-menu.message').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

        $('.one-of-menu.office').click(function () {
            $('.all-block-dropdown-menu.office').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.office').length ) 
            	return;
          	$('.all-block-dropdown-menu.office').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.office').mouseleave (function() {
          $('.all-block-dropdown-menu.office').fadeOut(200);       
        });

        // //////////////////////////////////////////////////////////

		$('.btn-login').click(function () {
            $('.block-content-registretion').fadeOut(0);
            $('.block-content-login').fadeIn(0);
            $('.btn-login').addClass('active');
            $('.btn-registration').removeClass('active');
        });

        $('.btn-registration').click(function () {
            $('.block-content-login').fadeOut(0);
            $('.block-content-registretion').fadeIn(0);
            $('.btn-registration').addClass('active');
            $('.btn-login').removeClass('active');
        });

        // //////////////////////////////////////////////////////////

        $('.into-gift-center-content').find(".one-of-tab-content").click(function () {
		  	$('.one-of-tab-content').removeClass('active');
	        $(this).addClass('active');
		});

		$('.one-of-tab-content.all-gift').click(function () {
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.all-gift').fadeIn(200);
        });

		$('.one-of-tab-content.new-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeIn(200);
            $(".regular-gift-for-user-two").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        $('.one-of-tab-content.my-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeIn(200);
            $(".regular-gift-for-user-three").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        // //////////////////////////////////////////////////////////

		$('.select-registration-one').click(function () {
            $('.all-block-option').fadeIn(200);
            $('.scroll-pane').jScrollPane({showArrows: true});
            $('.btn-select').addClass('active');
        });

		$('.points-count.focus').click(function () {
            $('.all-block-option').fadeOut(200);
            $('.btn-select').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-one').length ) 
            	return;
          	$('.btn-select').removeClass('active');
          	$('.all-block-option').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-one').find(".points-count").click(function () {
		  	$('.points-count').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count.focus').click(function () {
          	var total = document.querySelector('.content-select-one .points-count.focus').innerHTML;
          	document.getElementById('total').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-two').click(function () {
            $('.all-block-option-two').fadeIn(200);
            $('.scroll-pane-two').jScrollPane({showArrows: true});
            $('.btn-select-two').addClass('active');
        });

		$('.points-count-two.focus').click(function () {
            $('.all-block-option-two').fadeOut(200);
            $('.btn-select-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-two').length ) 
            	return;
          	$('.btn-select-two').removeClass('active');
          	$('.all-block-option-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-two').find(".points-count-two").click(function () {
		  	$('.points-count-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-two.focus').click(function () {
          	var total = document.querySelector('.content-select-two .points-count-two.focus').innerHTML;
          	document.getElementById('total-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-three').click(function () {
            $('.all-block-option-three').fadeIn(200);
            $('.scroll-pane-three').jScrollPane({showArrows: true});
            $('.btn-select-three').addClass('active');
        });

		$('.points-count-three.focus').click(function () {
            $('.all-block-option-three').fadeOut(200);
            $('.btn-select-three').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-three').length ) 
            	return;
          	$('.btn-select-three').removeClass('active');
          	$('.all-block-option-three').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-three').find(".points-count-three").click(function () {
		  	$('.points-count-three').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-three.focus').click(function () {
          	var total = document.querySelector('.content-select-three .points-count-three.focus').innerHTML;
          	document.getElementById('total-three').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-lang').click(function () {
            $('.all-block-option-lang').fadeIn(200);
            // $('.scroll-pane-lang').jScrollPane({showArrows: true});
            $('.btn-select-lang').addClass('active');
        });

		$('.points-count-lang.focus').click(function () {
            $('.all-block-option-lang').fadeOut(200);
            $('.btn-select-lang').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-lang').length ) 
            	return;
          	$('.btn-select-lang').removeClass('active');
          	$('.all-block-option-lang').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-lang').find(".points-count-lang").click(function () {
		  	$('.points-count-lang').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-lang.focus').click(function () {
          	var total = document.querySelector('.content-select-lang .points-count-lang.focus').innerHTML;
          	document.getElementById('total-lang').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        $('.into-slider-carousel').find(".one-of-like-user .left-block").hover(function() {
        	$(this).find('.block-hover-like-user').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.block-hover-like-user').fadeOut(200);
        	});
		});

		// ///////////////////////////////////////////////////////////////////

		$('.line-block-sympathy-user').find(".one-of-sympathy-user").hover(function() {
        	$(this).find('.block-hover-sympathy-user').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.block-hover-sympathy-user').fadeOut(200);
        	});
		});


        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////


        $('.one-of-four-package').find(".icon-info").hover(function() {
        	$(this).find('.block-info-more').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.block-info-more').fadeOut(50);
        	});
		});


        // ///////////////////////////////////////////////////////////////////



    </script>

    <script>

			  if(document.documentElement.clientWidth > 1340) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 10,
		          	slidesToScroll: 10
		        });
			  }

			  else if(document.documentElement.clientWidth > 900) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 7,
		          	slidesToScroll: 7
		        });
			  }

			 else if(document.documentElement.clientWidth > 600) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 5,
		          	slidesToScroll: 5
		        });
			  }

			  else if(document.documentElement.clientWidth > 0) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 2,
		          	slidesToScroll: 2
		        });
			  }

			  else {

			  }
        </script>

        <script type="text/javascript">

            $(document).ready(function(){

            $(".btn-mobile-open").click(function(){
                $(".btn-mobile-open").fadeOut(0);
                $(".under-header").animate({'width': "+=250px"}, 500);
                $(".under-header").css({'border': "2px solid white"});
                $(".btn-mobile-close").fadeIn(200);
            });

            $(".btn-mobile-close").click(function(){
                $(".under-header").animate({'width': "-=250px"}, 500);
                $(".under-header").css({'border': "0px"});
                $(".btn-mobile-close").fadeOut(0);
                $(".btn-mobile-open").fadeIn(200);
            });

            });
    </script>



	<!-- =============================================================== -->

  	<!-- end-script -->

  </body>
</html>