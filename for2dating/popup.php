<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity bg-popup">
	  	<div class="popup">
	  		<!--  -->
	  		<div class="all-block-question-psihology">
	  			<div class="block-question-psihology">
	  				<span class="header-block-question-psihology">
	  					Задать вопрос Психологу
	  				</span>

	  				<textarea id="textarea-question-psihology" class="textarea-question-psihology" placeholder="Напишите свой вопрос"></textarea>

	  				<!--  -->
	  				<div id="category-three" class="active">
					  	<div class="left-block-setting">
						  	<span class="category-all pasive">
						  		Выберете рубрику 
						  	</span>

						  	<span class="category-all active">
						  		Выберете рубрику 
						  	</span>
					  	</div>

					  	<div class="left-block">
						    <input id="input-one" name="input-one" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-one">Семья</label>
						</div>

						<div class="left-block">
						    <input id="input-two" name="input-two" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-two">Психология общения</label>
						</div>

						<div class="left-block">
						   <input id="input-three" name="input-three" class="checkbox" type="checkbox" value="Battery">
						   <label class="one-of-category-all" for="input-three">Психотип славянок</label>
						</div>

						<div class="left-block">
						    <input id="input-four" name="input-four" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-four">Психотип славянок</label>
						</div>

						<div class="left-block">
						    <input id="input-five" name="input-five" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-five">Психология общения</label>
						</div>

						<div class="left-block">
						    <input id="input-six" name="input-six" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-six">Рубрика 5</label>
						</div>

						<div class="left-block">
						    <input id="input-seven" name="input-seven" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-seven">Рубрика 6</label>
						</div>

						<div class="left-block">
						    <input id="input-eight" name="input-eight" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-eight">Рубрика 7</label>
						</div>

						<div class="left-block">
						    <input id="input-nine" name="input-nine" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-nine">Рубрика 8</label>
						</div>

						<div class="left-block">
						    <input id="input-ten" name="input-ten" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-ten">Рубрика 9</label>
						</div>

						<div class="left-block">
						    <input id="input-elewen" name="input-elewen" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-elewen">Рубрика 10</label>
						</div>

						<div class="left-block">
						    <input id="input-tvelwe" name="input-tvelwe" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-tvelwe">Рубрика 11</label>
						</div>

						<div class="left-block">
						    <input id="input-thirting" name="input-thirting" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-thirting">Рубрика ...</label>
						</div>

						<div class="left-block">
						    <input id="input-fourting" name="input-fourting" class="checkbox" type="checkbox" value="Battery">
						    <label class="one-of-category-all" for="input-fourting">Рубрика 15</label>
						</div>
					</div>
					<!--  -->

					<div class="footer-block-question-psihology">
						<div class="block-check-econom">
			  				<input id="check-econom" type="checkbox" name="check-econom">
	                      	<label class="check-econom" for="check-econom"></label>

	                      	<span class="text-check-econom">
	                      		Отправить анонимно?
	                      	</span>
			  			</div>

			  			<span class="btn-cancel">
			  				Отменить
			  			</span>

			  			<span class="btn-send-message-popup">
			  				Отправить сообщение
			  			</span>
					</div>
	  			</div>
	  		</div>
	  		<!--  -->

	  		<!-- buy-coints -->
	  		<div class="block-calculator-six">
    								
    			<!--  -->

    			<form name="cl_form" class="cl_form">   
                    <div class="demo">

                        <span class="header-calc">
                        	Дополнительные услуги
                        </span>                                       

                        <div class="three-slide-block-six">
                        	<div class="block-text-three-slide-six">
                        		<span class="bold">
                        			Статус инкогнито
                        		</span>

                        		<span class="regular">
                        			Возможно ходить по
									страницам пользователей
									и они этого не увидят.
                        		</span>
	                        </div>

	                        <div class="three-slide-six">
                            	<div id="slider-range-max3"></div>                                          			
                        		<input type="hidden" id="amount3" onchange="calc(this.value);">
                        	</div>     

                            <div class="block-value-three-slide">
                            	<span class="one">
                            		3 дня
                            	</span>

                            	<span class="two">
                            		7 дней
                            	</span>

                            	<span class="three">
                            		14 дней
                            	</span>

                            	<span class="four">
                            		21 дней
                            	</span>

                            	<span class="five">
                            		30 дней
                            	</span>
                            </div>

                        </div>

                        <div class="block-results">
                        	<input class="input-results" value="5" name="summ" readonly="readonly" type="text">
                        	
                        	<span class="bold">
                        		монет
                        	</span>

                        	<span class="btn-calc thumbnail-calc">
                        		Купить
                        	</span>
                        </div>
                    </div>

                    <div class="bg-calc-down">
                    	<span>
                    		У Вас На счету:  <span class="bold-summ">32 </span><span class="bold-coints">монеты</span>
                    	</span>
                    </div>
                </form>
    								
    			<!--  -->

    		</div>
	  		<!-- end-buy-coints -->

	  		<!-- gift -->

	  		<div class="all-block-gift-popup">
	  			<div class="block-gift-popup">

		  			<!--  -->
		  			<div class="block-tab-gift-popup">
		  			
		  				<!--  -->
		  				<div class="all-gift one-of-tab-gift-popup active">
		  					<span>
		  						Популярные
		  					</span>
		  				</div>
		  				<!--  -->

		  				<!--  -->
		  				<div class="new-gift center one-of-tab-gift-popup">
		  					<span>
		  						Новые
		  					</span>
		  				</div>
		  				<!--  -->

		  				<!--  -->
		  				<div class="my-gift one-of-tab-gift-popup">
		  					<span>
		  						Для нее
		  					</span>
		  				</div>
		  				<!--  -->

		  				<!--  -->
		  				<div class="him-gift one-of-tab-gift-popup">
		  					<span>
		  						Для него
		  					</span>
		  				</div>
		  				<!--  -->

		  			</div>

		  			<!-- one-of-tab -->
		  			<div class="block-fade-tab-gift-popup all-gift">

		  				<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/one-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/two-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/three-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/four-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/five-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!-- line -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/six-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/seven-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/eight-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/nine-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/ten-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

		  			</div>
		  			<!-- end-one-of-tab -->

		  			<!-- one-of-tab -->
		  			<div class="block-fade-tab-gift-popup new-gift">

		  				<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/one-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/two-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/three-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/four-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/five-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!-- line -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/six-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/seven-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/eight-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/nine-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/ten-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

		  			</div>
		  			<!-- end-one-of-tab -->

		  			<!-- one-of-tab -->
		  			<div class="block-fade-tab-gift-popup my-gift">

		  				<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/one-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/two-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/three-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/four-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/five-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!-- line -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/six-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/seven-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/eight-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/nine-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/ten-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

		  			</div>
		  			<!-- end-one-of-tab -->

		  			<!-- one-of-tab -->
		  			<div class="block-fade-tab-gift-popup him-gift">

		  				<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/one-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/two-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/three-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/four-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/five-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!-- line -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/six-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/seven-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/eight-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/nine-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

						<!--  -->
		  				<div class="block-one-of-gift-popup">
		  					<div class="left-block">
								<div class="parent-gift-popup">
								    <div>
								        <img src="img/ten-gift.png" alt="not-image">
								    </div>
								</div>
							</div>

							<span>
								1 монета
							</span>
						</div>
						<!--  -->

		  			</div>
		  			<!-- end-one-of-tab -->

		  			<!--  -->
		  			<div class="block-textarea-gift-popup">
		  				<div class="bg-textarea-gift-popup">
		  					<textarea class="textarea-gift-popup" placeholder="Напишите приветствие..."></textarea>

		  					<div class="icon-acclamation">		  						
		  					</div>

		  					<span class="number-symbol">
		  						80
		  					</span>
		  				</div>
		  			</div>
		  			<!--  -->

		  			<!--  -->
		  			<div class="footer-block-gift-popup">
						<div class="block-check-econom">
			  				<input id="gift-popup" type="checkbox" name="gift-popup">
	                      	<label class="check-econom" for="gift-popup"></label>

	                      	<span class="text-check-econom">
	                      		Скрыть, кем передан комплимент
	                      	</span>
			  			</div>

			  			<span class="btn-send-gift-popup">
			  				Сделать подарок
			  			</span>
					</div>
		  			<!--  -->

				</div>

			</div>
			<!--  -->

	  		<!-- end-gift -->
	  	</div>
  	</div>

  	<!-- script -->

  	<script type="text/javascript">
        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-lang').click(function () {
            $('.all-block-option-lang').fadeIn(200);
            // $('.scroll-pane-lang').jScrollPane({showArrows: true});
            $('.btn-select-lang').addClass('active');
        });

		$('.points-count-lang.focus').click(function () {
            $('.all-block-option-lang').fadeOut(200);
            $('.btn-select-lang').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-lang').length ) 
            	return;
          	$('.btn-select-lang').removeClass('active');
          	$('.all-block-option-lang').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-lang').find(".points-count-lang").click(function () {
		  	$('.points-count-lang').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-lang.focus').click(function () {
          	var total = document.querySelector('.content-select-lang .points-count-lang.focus').innerHTML;
          	document.getElementById('total-lang').innerHTML = total;
        });

        $('.block-question-psihology').find(".category-all.pasive").click(function(){
			$('#category-three').addClass('active');
			$(this).fadeOut(0);
			$('.category-all.active').fadeIn(0);
		});

		$('.block-question-psihology').find(".category-all.active").click(function(){
			$('#category-three').removeClass('active');
			$(this).fadeOut(0);
			$('.category-all.pasive').fadeIn(0);
		});

		$('textarea.textarea-question-psihology')
  		.focus(function() { 
  							$('textarea.textarea-question-psihology').css('line-height', '18px');
  							$('textarea.textarea-question-psihology').css('text-align', 'left')
  		})
 	 	.blur(function() { if ($('textarea.textarea-question-psihology')[0].value == '') { 
 	 						$('textarea.textarea-question-psihology').css('line-height', '273px'); 
 	 						$('textarea.textarea-question-psihology').css('text-align', 'center')
 	 	} });

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        $('.block-gift-popup').find(".one-of-tab-gift-popup").click(function () {
		  	$('.one-of-tab-gift-popup').removeClass('active');
	        $(this).addClass('active');
		});

		$('.one-of-tab-gift-popup.all-gift').click(function () {
            $('.block-fade-tab-gift-popup.new-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.my-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.him-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.all-gift').fadeIn(200);
        });

		$('.one-of-tab-gift-popup.new-gift').click(function () {
            $('.block-fade-tab-gift-popup.all-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.my-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.him-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.new-gift').fadeIn(200);            
        });

        $('.one-of-tab-gift-popup.my-gift').click(function () {
            $('.block-fade-tab-gift-popup.all-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.new-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.him-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.my-gift').fadeIn(200);            
        });

        $('.one-of-tab-gift-popup.him-gift').click(function () {
            $('.block-fade-tab-gift-popup.all-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.new-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.my-gift').fadeOut(0);
            $('.block-fade-tab-gift-popup.him-gift').fadeIn(200);            
        });

        // //////////////////////////////////////////////////////////



    </script>

    <link href="jquery-calculator.css" rel="stylesheet" type="text/css"/>
	    <script src="js/jquery-calculator-1-4.js"></script>
	    <script src="js/jquery-calculator-1-8.js"></script>

    <script>

		    $(function() {
		        $( "#slider-range-max3" ).slider({
		            range: "max",
		            min: 0,
		            max: 4,
		            value: 1,
		            slide: function( event, ui ) {
		                $( "#amount3" ).val( ui.value );
		                calc();
		            }
		        });
		        $( "#amount3" ).val( $( "#slider-range-max3" ).slider( "value" ) );
		    });
		 
		    function calc(par){
		    amount3 = document.cl_form.amount3.value;

		    summ = ( Number(amount3) * 5 );
		    document.cl_form.summ.value=summ;
		    

		   		if ($('input.input-results')[0].value == '0') {
		    	document.cl_form.summ.value= '3';
		   		}

		   		else if ($('input.input-results')[0].value == '5') {
		    	document.cl_form.summ.value='5';
		   		}

		   		else if ($('input.input-results')[0].value == '10') {
		    	document.cl_form.summ.value='9';
		   		}

		   		else if ($('input.input-results')[0].value == '15') {
		    	document.cl_form.summ.value='13';
		   		}

		   		else if ($('input.input-results')[0].value == '20') {
		    	document.cl_form.summ.value='17';
		   		}

		   		else {
		   			
		   		}

		   		return false; 

		    //var summ;ui-slider-handle ui-state-default ui-corner-all
		    
		    }
	    </script>

	    <!--  -->



	<!-- =============================================================== -->

  	<!-- end-script -->

  </body>
</html>