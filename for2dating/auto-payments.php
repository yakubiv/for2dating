<!DOCTYPE html>

<html lang="ru">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Social</title>
    <link href="style.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/chosen.css">
    <link type="text/css" href="css/jquery.jscrollpane.css" rel="stylesheet" media="all" />
    <link rel="stylesheet" href="css/form-validation.css">

    <script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>

  </head>

  <body>

  	<div class="opacity perfect-pair-step-first">

  		<!-- baground-slider -->

  		<div class="null-block">

  			<!-- slider-display-bg -->

	        <div class="slider-display-one" style="display:none;">
	            <div class="regular-first slider">
		            <!--  -->
		            <div>
		                <div class="into-slider-one one">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one two">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one three">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one four">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one five">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one six">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one seven">
		                </div>
		            </div>
		            <!--  -->
		            <!--  -->
		            <div>
		                <div class="into-slider-one eight">
		                </div>
		            </div>
		            <!--  -->
	        	</div>
	    	</div>

	    	<!-- end-slider-display-bg -->

  		</div>

  		<!-- end-baground-slider -->

  		<!-- header -->

  		<div class="left-one-bg-header">
  			<div class="left-two-bg-header">
	  			<div class="center-all-block">
	  				<div class="left-center-all-block">

	  					<!--  -->

	  					<div class="over-header">

			  				<div class="block-logo-header">
			  					<a href="#">
			  						<div class="logo-header">
			  						</div>
			  					</a>
			  				</div>

			  				<!--  -->

			  				<div class="block-soc-header">
			  					<span>
			  						Мы в
			  					</span>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-vk">
			  							</div>
			  						</a>
			  					</div>

			  					<div class="block-one-soc-header">
			  						<a href="#">
			  							<div class="icon-face">
			  							</div>
			  						</a>
			  					</div>
			  				</div>

			  				<!-- lang-select -->

	  						<div class="block-select-registration-lang">
	  							<div class="select-registration-lang">
	  								<div id="total-lang">
	  									<div class="flag-one-of-lang">
						  				</div>

						  				<span>
						  					Русский
						  				</span>
	  								</div>

	  								<div class="btn-select-lang">
	  								</div>
	  							</div>

	  							<div class="all-block-option-lang">
									<div class="scroll-pane-lang">
										<div class="content-select-lang">
										    <div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">
								  				</div>

								  				<span>
								  					Русский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">
								  				</div>

								  				<span>
								  					Английский
								  				</span>
										   	</div>

											<div class="points-count-lang focus">
										    	<div class="flag-one-of-lang">
								  				</div>

								  				<span>
								  					Норвежский
								  				</span>
										   	</div>
									    </div>
									</div>
								</div>
	  						</div>

	  						<!-- end-lang-select -->

			  				<!-- account -->

			  				<div class="block-account">
			  					<div class="block-coints">
			  						<span class="coints">
			  							0
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-user-header">
			  						<div class="border-photo-user-header">
			  							<img src="img/photo-user-header.png" alt="not-image">
			  						</div>

			  						<span>
			  							Александр
			  						</span>
			  					</div>

			  					<!--  -->

			  					<div class="block-setting">
			  						<div class="icon-setting">
			  						</div>
			  					</div>

			  					<!--  -->

			  					<div class="block-icon-enter-header">
			  						<div class="icon-enter-header">
			  						</div>
			  					</div>

			  					<!--  -->

			  				</div>

			  				<!-- end-account -->

		  				</div>

		  				<!--  -->

		  				<!-- mobile-menu -->

		  				<div class="null-block">
		  					<div class="null-block">
		  						<div class="btn-mobile-open">
		  						</div>
		  					</div>

		  					<div class="null-block">
		  						<div class="btn-mobile-close">
		  						</div>
		  					</div>
		  				</div>

		  				<!-- end-mobile-menu -->

		  				<!--  -->

		  				<div class="under-header">

		  					<!--  -->

		  					<div class="one-of-menu office">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Мой кабинет
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu office">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои альбомы
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Моя статистика
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой пакет услуг
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мой счет
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои статьи
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои подарки
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Пригласи друга
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Мои настройки
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">
		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu message">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Уведомления
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
			  						<div class="one-of-menu-more">
			  						</div>
			  					</div>
			  					<div class="null-block-vertical">
		  							<div class="block-number-message">
		  								<span>
		  									648
		  								</span>
		  							</div>
		  						</div>
		  						<div class="null-block-vertical">
		  							<div class="all-block-dropdown-menu message">
		  								<div class="content-dropdown-menu">
		  									<span class="one-of-dropdown-menu">
		  										Мои сообщения <span>(9)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подмигиваний <span>(315)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Просмотры <span>(49)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Избранные <span>(5)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Подарки <span>(3)</span>
		  									</span>

		  									<!--  -->

		  									<span class="one-of-dropdown-menu">
		  										Симпатии <span>(5)</span>
		  									</span>

		  									<!--  -->
		  								</div>

		  								<div class="down-bg-dropdown-menu">

		  								</div>
		  							</div>
		  						</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Блог
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Карусель
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							ТОП 100
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Поиск
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  					<div class="one-of-menu">
		  						<div class="null-block-vertical">
			  						<div class="bg-left-one-of-menu">
			  						</div>
			  					</div>
		  						<span class="content-one-of-menu">
		  							Идеальная пара
		  						</span>
		  						<div class="null-block-vertical">
			  						<div class="bg-right-one-of-menu">
			  						</div>
			  					</div>
		  					</div>

		  					<!--  -->

		  				</div>

		  				<!--  -->

	  				</div>
	  			</div>
	  		</div>
  		</div>

  		<!-- end-header -->

  		<!-- slider-under-header -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  			<!--  -->

	  			<div class="block-slider-under-header">
	  				<div class="all-slider-under-header">

	  					<div class="block-trap-photo">
	  						<span class="bold">
	  							Ловушка:
	  						</span>

	  						<span class="regular">
	  							Попасть<br>
	  							в ленту
	  						</span>
	  					</div>

	  					<!--  -->

				        <div class="slider-display-under-header">
				            <div class="regular-under-header slider-under-header">
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/one-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/two-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/three-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/four-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/five-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/eight-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/ten-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/six-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/nine-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
					            <!--  -->
					            <div>
					                <div class="into-slider-under-header">
					                	<img src="img/seven-image-trap.png" alt="not-image">
					                </div>
					            </div>
					            <!--  -->
				        	</div>
				    	</div>

				    	<!--  -->

	  				</div>
	  			</div>

	  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-slider-under-header -->

  		<!-- content -->

  		<div class="left-block">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<div class="content-my-account">

	  					<!--  -->

	  					<span class="header-content-my-account">
	  						Мой счет
	  					</span>

	  					<!--  -->

	  					<div class="block-under-header-content-my-account">
	  						<div class="on-account">
	  							<span>
	  								На Вашем счёте:
	  							</span>

	  							<span class="bold">
	  								0 монет
	  							</span>
	  						</div>

	  						<!--  -->

	  						<div class="on-account">
	  							<span>
	  								Курс:
	  							</span>

	  							<span class="bold">
	  								1 монета = 1 грн.
	  							</span>
	  						</div>

	  						<!--  -->

	  						<div class="block-btn-deposit-info">
	  							<span class="btn-deposit">
	  								Пополнение
	  							</span>

	  							<span class="btn-deposit-info">
	  								Выписка
	  							</span>
	  						</div>

	  						<!--  -->

	  						<!-- table -->

	  						<div class="block-table">
	  							<div class="center-table">
	  								<div class="header-table">
	  									<span class="header-auto-payments">
	  										Автоплатежи
	  									</span>
	  								</div>

	  								<div class="table">

	  									<!--  -->

	  									<div class="line-table first">
	  										<div class="table-name-payments">
	  											<span>
	  												Наименование
	  											</span>
	  										</div>

	  										<div class="table-sum-payments">
	  											<span>
	  												Сумма
	  											</span>
	  										</div>

	  										<div class="table-date-payments">
	  											<span>
	  												Дата списания
	  											</span>
	  										</div>

	  										<div class="table-system-payments">
	  											<span>
	  												Платежная система
	  											</span>
	  										</div>

	  										<div class="table-move-payments">
	  											<span>
	  												Действие
	  											</span>
	  										</div>
	  									</div>

	  									<!--  -->

	  									<!--  -->

	  									<div class="line-table">
	  										<div class="table-name-payments content">
	  											<span>
	  												Пополнение аккаунта
	  											</span>
	  										</div>

	  										<div class="table-sum-payments content">
	  											<span>
	  												100 грн
	  											</span>
	  										</div>

	  										<div class="table-date-payments content">
	  											<span>
	  												Каждого 20-го числа месяца
	  											</span>
	  										</div>

	  										<div class="table-system-payments content">
	  											<span>
	  												LiqPay
	  											</span>
	  										</div>

	  										<div class="table-move-payments content">
	  											<span class="btn-cancel">
	  												Отредактировать
	  											</span>

	  											<span class="btn-cancel right">
	  												Отменить
	  											</span>
	  										</div>
	  									</div>

	  									<!--  -->

	  								</div>
	  							</div>

	  							<div class="null-block">
	  								<div class="bg-table-down">
	  								</div>
	  							</div>
	  						</div>

	  						<!-- end-table -->

	  						<div class="block-methods-pay">
	  							<div class="center-methods-pay">

	  								<!--  -->

	  								<div class="one-of-methods-pay">
	  									<div class="block-icon-one-of-methods-pay">
	  										<div class="icon-one-of-methods-pay one">
	  											<img src="img/icon-money.png" alt="not-image">
	  										</div>
	  									</div>

	  									<span>
	  										Электронные<br>
											деньги
	  									</span>
	  								</div>

	  								<!--  -->

	  								<!--  -->

	  								<div class="one-of-methods-pay center">
	  									<div class="block-icon-one-of-methods-pay">
	  										<div class="icon-one-of-methods-pay two">
	  											<img src="img/icon-cards.png" alt="not-image">
	  										</div>
	  									</div>

	  									<span>
	  										Кредитные<br>
											карты
	  									</span>
	  								</div>

	  								<!--  -->

	  								<!--  -->

	  								<div class="one-of-methods-pay">
	  									<div class="block-icon-one-of-methods-pay">
	  										<div class="icon-one-of-methods-pay three">
	  											<img src="img/icon-pay-pal.png" alt="not-image">
	  										</div>
	  									</div>

	  									<span>
	  										PayPal
	  									</span>
	  								</div>

	  								<!--  -->

	  							</div>
	  						</div>

	  						<!--  -->

	  						<!--  -->

	  						<div class="block-under-methods-pay">

	  							<!-- left-block-under-methods-pay -->

	  							<div class="left-block-under-methods-pay">
	  								<div class="coints-left-block">
	  									<span class="text-coints-left-block">
	  										Сколько монет внести на счет?
	  									</span>
	  								</div>

									<!-- select-ethnicity -->

	  								<div class="block-select-ethnicity">
	  									<div class="select-ethnicity">
	  										<div id="ethnicity">
	  											10 монет
	  										</div>

	  										<div class="btn-select-ethnicity">
	  										</div>
	  									</div>

	  									<div class="all-block-option-ethnicity">
											<div class="scroll-pane-ethnicity">
												<div class="content-select-ethnicity">
												    <div class="points-count-ethnicity focus">
												    	Ручной ввод
												    </div>

												    <div class="points-count-ethnicity focus">
												    	1 монет
												    </div>

												    <div class="points-count-ethnicity focus">
												    	5 монет
												    </div>

												    <div class="points-count-ethnicity focus">
												    	10 монет
												    </div>

												    <div class="points-count-ethnicity focus">
												    	20 монет
												    </div>

												    <div class="points-count-ethnicity focus">
												    	50 монет
												    </div>

												    <div class="points-count-ethnicity focus">
												    	100 монет
												    </div>

												    <div class="points-count-ethnicity focus">
												    	500 монет
												    </div>
											    </div>
											</div>
										</div>
	  								</div>

	  								<!-- end-select-ethnicity -->

	  								<!--  -->

	  								<div class="arrow-pink">

	  								</div>

	  								<!--  -->

	  							</div>

	  							<!-- end-left-block-under-methods-pay -->

	  							<!-- right-block-under-methods-pay -->

	  							<div class="right-block-under-methods-pay">

	  								<!--  -->

	  								<div class="text-right-block-under-methods-pay">
	  									<span class="bold">
	  										Оплата кредитной картой<br>
											<span class="regular">Сумма платежа</span> 100 грн.
	  									</span>
	  								</div>

	  								<div class="icon-master-card">
	  								</div>

	  								<div class="icon-visa">
	  								</div>

	  								<!--  -->

	  								<!--  -->

	  								<div class="block-econom-auto-pay">
	  									<span class="grey">
	  										Экономьте с услугой автоплатеж!!!
	  									</span>

	  									<span class="grey">
	  										Покупайте монеты <span class="pink">на </span><span class="pink-big">20 % дешевле</span>
	  									</span>
	  								</div>

	  								<!--  -->

	  								<!--  -->

	  								<div class="block-check-econom">
			  							<input id="check-econom" type="checkbox" name="check-econom">
	                      				<label class="check-econom" for="check-econom"></label>

	                      				<span class="text-check-econom">
	                      					Сделать автоплатеж получи скидку 20%
	                      				</span>
			  						</div>

	  								<!--  -->

	  								<!--  -->

	  								<div class="block-hover-econom">
	  									<span class="regular">
	  										Один раз в месяц, в назначенную дату, с вашего счета будут автоматически списываться средства для оплаты дополнительного пакета монет в том же размере, что и первоначальная покупка.<br>
	  										For two dating имеет право снимать средства, используя любой из ваших способов оплаты, с целью осуществления будущих или регулярных платежей.
	  									</span>

	  									<span class="regular last">
	  										Подключив автоплатеж, вы подтверждаете свое согласие с приведенными выше условиями, <span class="violet">условиями пользования</span> и <span class="violet">политикой конфиденциальности.</span>
	  									</span>
	  								</div>

	  								<!--  -->

	  								<!--  -->

	  								<div class="block-check-econom">
			  							<input id="check-econom-two" type="checkbox" name="check-econom-two">
	                      				<label class="check-econom-two" for="check-econom-two"></label>

	                      				<span class="text-check-econom">
	                      					Указать данные другой карты
	                      				</span>
			  						</div>

	  								<!--  -->

	  								<!--  -->

	  								<div class="block-two-card">



	  									<!--  -->
	  									<div class="block-one-card">
	  										<!--  -->
	  										<div class="block-icon-card">
	  											<div class="icon-visa-card">
	  											</div>

	  											<div class="icon-mastercard-card">
	  											</div>
	  										</div>
	  										<!--  -->

	  										<!--  -->
	  										<span class="name-card">
	  											Номер карты
	  										</span>
	  										<!--  -->

	  										<!--  -->
	  										<div class="line-input-card">
	  											<!--  -->
		  										<div class="bg-input-card">
		  											<input type="text" class="input-card" minlength="4" maxlength="4" pattern="^[0-9]+$">
		  										</div>
		  										<!--  -->
		  										<div class="bg-input-card">
		  											<input type="text" class="input-card" minlength="4" maxlength="4" pattern="^[0-9]+$">
		  										</div>
		  										<!--  -->
		  										<div class="bg-input-card">
		  											<input type="text" class="input-card" minlength="4" maxlength="4" pattern="^[0-9]+$">
		  										</div>
		  										<!--  -->
		  										<div class="bg-input-card last">
		  											<input type="text" class="input-card" minlength="4" maxlength="4" pattern="^[0-9]+$">
		  										</div>
		  										<!--  -->
		  									</div>
		  									<!--  -->

		  									<!--  -->
		  									<span class="name-card">
	  											Имя и фамилия
	  										</span>
	  										<!--  -->

	  										<!--  -->
	  										<div class="line-input-card-two">
	  											<!--  -->
	  											<div class="bg-big-input">
	  												<input type="text" class="input-big-card">
	  											</div>
		  										<!--  -->

		  										<!-- one-select -->

				  								<div class="block-select-registration-one-card">
				  									<div class="select-registration-one-card">
				  										<div id="total-one-card">
				  											MM
				  										</div>

				  										<div class="btn-select-one-card">
				  										</div>
				  									</div>

				  									<div class="all-block-option-one-card">
														<div class="scroll-pane-one-card">
															<div class="content-select-one-card">
															    <div class="points-count-one-card focus">
															    	01
															    </div>

															    <div class="points-count-one-card focus">
															    	02
															    </div>

															    <div class="points-count-one-card focus">
															    	03
															    </div>

															    <div class="points-count-one-card focus">
															    	04
															    </div>

															    <div class="points-count-one-card focus">
															    	05
															    </div>

															    <div class="points-count-one-card focus">
															    	06
															    </div>

															    <div class="points-count-one-card focus">
															    	07
															    </div>

															    <div class="points-count-one-card focus">
															    	08
															    </div>

															    <div class="points-count-one-card focus">
															    	09
															    </div>

															    <div class="points-count-one-card focus">
															    	10
															    </div>

															    <div class="points-count-one-card focus">
															    	11
															    </div>

															    <div class="points-count-one-card focus">
															    	12
															    </div>
														    </div>
														</div>
													</div>
				  								</div>

				  								<!-- end-one-select -->

				  								<!--  -->
				  								<span class="slash-card">
				  									/
				  								</span>
				  								<!--  -->

				  								<!-- two-select -->

				  								<div class="block-select-registration-two-card">
				  									<div class="select-registration-two-card">
				  										<div id="total-two-card">
				  											YY
				  										</div>

				  										<div class="btn-select-two-card">
				  										</div>
				  									</div>

				  									<div class="all-block-option-two-card">
														<div class="scroll-pane-two-card">
															<div class="content-select-two-card">
																<div class="points-count-two-card focus">
															    	16
															    </div>

															    <div class="points-count-two-card focus">
															    	17
															    </div>

															    <div class="points-count-two-card focus">
															    	18
															    </div>

															    <div class="points-count-two-card focus">
															    	19
															    </div>

															    <div class="points-count-two-card focus">
															    	20
															    </div>

															    <div class="points-count-two-card focus">
															    	21
															    </div>

															    <div class="points-count-two-card focus">
															    	22
															    </div>
														    </div>
														</div>
													</div>
				  								</div>

				  								<!-- end-two-select -->

		  									</div>
		  									<!--  -->
	  									</div>

	  									<div class="null-block">
	  										<div class="block-second-card">
	  											<div class="block-input-question">
	  												<div class="icon-question-card">
	  												</div>
	  												<!--  -->
			  										<div class="bg-input-card right">
			  											<input type="password" class="input-card" minlength="3" maxlength="3" pattern="^[0-9]+$">
			  										</div>
			  										<!--  -->
	  											</div>

	  											<!--  -->
	  											<span>
			  										Код<br>
													безопасности<br>
													карты (CVV/CVC)
			  									</span>
			  									<!--  -->
	  										</div>
	  									</div>
	  									<!--  -->
	  								</div>

	  								<!--  -->

	  								<!--  -->

	  								<div class="block-btn-two-card">
	  									<span class="btn-cancel-two-card">
	  										Отменить
	  									</span>

	  									<span class="btn-send-two-card">
	  										Оплатить
	  									</span>
	  								</div>

	  								<!--  -->

	  							</div>

	  							<!-- end-right-block-under-methods-pay -->

	  						</div>

	  						<!--  -->

	  					</div>

	  				</div>

	  			</div>
	  		</div>
  		</div>

  		<!-- end-content -->

  		<!-- footer-display -->

  		<div class="footer-display">
  			<div class="center-all-block">
	  			<div class="left-center-all-block">

	  				<!--  -->

		  			<div class="footer-menu">

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					О нас
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Условия и правила
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Конфиденциальность
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Партнерам
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Отзывы
		  				</span>

		  				<!--  -->

		  				<span class="one-of-footer-menu">
		  					Связаться с Нами
		  				</span>

		  				<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="block-soc-footer">

		  				<!--  -->

		  				<span class="header-soc-footer">
		  					Присоединяйся к <span>For two dating:</span>
		  				</span>

		  				<!--  -->

		  				<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-vk">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-face">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-twit">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-inst">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-yt">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

			  			<div class="block-one-soc-header">
			  				<a href="#">
			  					<div class="icon-gp">
			  					</div>
			  				</a>
			  			</div>

			  			<!--  -->

		  			</div>

		  			<!--  -->

		  			<!--  -->

		  			<div class="footer-all-right">
		  				<span class="header-soc-footer">
		  					© 2016- 2016 For2date. Все права защищены. Разработка I-PR
		  				</span>
		  			</div>

		  			<!--  -->

	  			</div>
	  		</div>
  		</div>

  		<!-- end-footer-display -->

  	</div>

  	<!-- script -->

  	<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>

  	<script type="text/javascript">
        $(".regular-first").slick({
          	dots: false,
          	autoplay: true,
           	prevArrow: false,
            nextArrow: false,
            centerMode: true,
          	infinite: true,
          	slidesToShow: 1,
          	slidesToScroll: 1,
          	autoplaySpeed: 10000
        });



        $(".regular-two").slick({
          dots: true,
          infinite: true,
          centerMode: true,
          slidesToShow: 1,
          prevArrow: false,
          nextArrow: false,
          slidesToScroll: 1
        });

        $(".regular-gift-for-user").slick({
          infinite: true,
          // centerMode: true,
          slidesToShow: 3,
          slidesToScroll: 3
        });

        $(".regular-carousel").slick({
          	dots: false,
          	// autoplay: true,
            // centerMode: true,
          	// autoplaySpeed: 5000,
          	infinite: true,
          	variableWidth: true,
          	slidesToShow: 1,
          	slidesToScroll: 1
        });

    </script>

    <!-- =============================================================== -->

    <script src="js/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
	    var config = {
	      '.chosen-select'           : {},
	      '.chosen-select-deselect'  : {allow_single_deselect:true},
	      '.chosen-select-no-single' : {disable_search_threshold:1},
	      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	      '.chosen-select-width'     : {width:"95%"}
	    }
	    for (var selector in config) {
	      $(selector).chosen(config[selector]);
	    }
	</script>

	<!-- =============================================================== -->

	<script type="text/javascript" src="js/jquery.mousewheel.js"></script>

	<script type="text/javascript" src="js/jquery.jscrollpane.js"></script>

	<script type="text/javascript">

		$('.one-of-menu.message').click(function () {
            $('.all-block-dropdown-menu.message').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.message').length )
            	return;
          	$('.all-block-dropdown-menu.message').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.message').mouseleave (function() {
          $('.all-block-dropdown-menu.message').fadeOut(200);
        });

        // //////////////////////////////////////////////////////////

        $('.one-of-menu.office').click(function () {
            $('.all-block-dropdown-menu.office').fadeIn(200);
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.one-of-menu.office').length )
            	return;
          	$('.all-block-dropdown-menu.office').fadeOut(200);
         	event.stopPropagation();
        });

        $('.one-of-menu.office').mouseleave (function() {
          $('.all-block-dropdown-menu.office').fadeOut(200);
        });

        // //////////////////////////////////////////////////////////

		$('.btn-login').click(function () {
            $('.block-content-registretion').fadeOut(0);
            $('.block-content-login').fadeIn(0);
            $('.btn-login').addClass('active');
            $('.btn-registration').removeClass('active');
        });

        $('.btn-registration').click(function () {
            $('.block-content-login').fadeOut(0);
            $('.block-content-registretion').fadeIn(0);
            $('.btn-registration').addClass('active');
            $('.btn-login').removeClass('active');
        });

        // //////////////////////////////////////////////////////////

        $('.into-gift-center-content').find(".one-of-tab-content").click(function () {
		  	$('.one-of-tab-content').removeClass('active');
	        $(this).addClass('active');
		});

		$('.one-of-tab-content.all-gift').click(function () {
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.all-gift').fadeIn(200);
        });

		$('.one-of-tab-content.new-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeIn(200);
            $(".regular-gift-for-user-two").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        $('.one-of-tab-content.my-gift').click(function () {
            $('.block-fade-tab.all-gift').fadeOut(0);
            $('.block-fade-tab.new-gift').fadeOut(0);
            $('.block-fade-tab.my-gift').fadeIn(200);
            $(".regular-gift-for-user-three").slick({
	          infinite: true,
	          // centerMode: true,
	          slidesToShow: 3,
	          slidesToScroll: 3
	        });
        });

        // //////////////////////////////////////////////////////////

		$('.select-registration-one-card').click(function () {
            $('.all-block-option-one-card').fadeIn(200);
            $('.scroll-pane-one-card').jScrollPane({showArrows: true});
            $('.btn-select-one-card').addClass('active');
        });

		$('.points-count-one-card.focus').click(function () {
            $('.all-block-option-one-card').fadeOut(200);
            $('.btn-select-one-card').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-one-card').length )
            	return;
          	$('.btn-select-one-card').removeClass('active');
          	$('.all-block-option-one-card').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-one-card').find(".points-count-one-card").click(function () {
		  	$('.points-count-one-card').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-one-card.focus').click(function () {
          	var total = document.querySelector('.content-select-one-card .points-count-one-card.focus').innerHTML;
          	document.getElementById('total-one-card').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // //////////////////////////////////////////////////////////

		$('.select-registration-two-card').click(function () {
            $('.all-block-option-two-card').fadeIn(200);
            $('.scroll-pane-two-card').jScrollPane({showArrows: true});
            $('.btn-select-two-card').addClass('active');
        });

		$('.points-count-two-card.focus').click(function () {
            $('.all-block-option-two-card').fadeOut(200);
            $('.btn-select-two-card').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-two-card').length )
            	return;
          	$('.btn-select-two-card').removeClass('active');
          	$('.all-block-option-two-card').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-two-card').find(".points-count-two-card").click(function () {
		  	$('.points-count-two-card').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-two-card.focus').click(function () {
          	var total = document.querySelector('.content-select-two-card .points-count-two-card.focus').innerHTML;
          	document.getElementById('total-two-card').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-two').click(function () {
            $('.all-block-option-two').fadeIn(200);
            $('.scroll-pane-two').jScrollPane({showArrows: true});
            $('.btn-select-two').addClass('active');
        });

		$('.points-count-two.focus').click(function () {
            $('.all-block-option-two').fadeOut(200);
            $('.btn-select-two').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-two').length )
            	return;
          	$('.btn-select-two').removeClass('active');
          	$('.all-block-option-two').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-two').find(".points-count-two").click(function () {
		  	$('.points-count-two').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-two.focus').click(function () {
          	var total = document.querySelector('.content-select-two .points-count-two.focus').innerHTML;
          	document.getElementById('total-two').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        $('.select-registration-lang').click(function () {
            $('.all-block-option-lang').fadeIn(200);
            // $('.scroll-pane-lang').jScrollPane({showArrows: true});
            $('.btn-select-lang').addClass('active');
        });

		$('.points-count-lang.focus').click(function () {
            $('.all-block-option-lang').fadeOut(200);
            $('.btn-select-lang').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-registration-lang').length )
            	return;
          	$('.btn-select-lang').removeClass('active');
          	$('.all-block-option-lang').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-lang').find(".points-count-lang").click(function () {
		  	$('.points-count-lang').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-lang.focus').click(function () {
          	var total = document.querySelector('.content-select-lang .points-count-lang.focus').innerHTML;
          	document.getElementById('total-lang').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////
        // ///////////////////////////////////////////////////////////////////

        $('.into-slider-carousel').find(".one-of-like-user .left-block").hover(function() {
        	$(this).find('.block-hover-like-user').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.block-hover-like-user').fadeOut(200);
        	});
		});

		// ///////////////////////////////////////////////////////////////////

		$('.line-block-sympathy-user').find(".one-of-sympathy-user").hover(function() {
        	$(this).find('.block-hover-sympathy-user').fadeIn(200);
        	$(this).mouseleave (function() {
        		$(this).find('.block-hover-sympathy-user').fadeOut(200);
        	});
		});


        // ///////////////////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////////////////

        $('.select-ethnicity').click(function () {
            $('.all-block-option-ethnicity').fadeIn(200);
            // $('.scroll-pane-ethnicity').jScrollPane({showArrows: true});
            $('.btn-select-ethnicity').addClass('active');
        });

		$('.points-count-ethnicity.focus').click(function () {
            $('.all-block-option-ethnicity').fadeOut(200);
            $('.btn-select-ethnicity').removeClass('active');
        });

        $(document).click( function(event){
          	if( $(event.target).closest('.select-ethnicity').length )
            	return;
          	$('.btn-select-ethnicity').removeClass('active');
          	$('.all-block-option-ethnicity').fadeOut(200);
         	event.stopPropagation();
        });

    	$('.content-select-ethnicity').find(".points-count-ethnicity").click(function () {
		  	$('.points-count-ethnicity').removeClass('focus');
	        $(this).addClass('focus');
		});

       	$('.points-count-ethnicity.focus').click(function () {
          	var total = document.querySelector('.content-select-ethnicity .points-count-ethnicity.focus').innerHTML;
          	document.getElementById('ethnicity').innerHTML = total;
        });

        // ///////////////////////////////////////////////////////////////////

    </script>

    <script>

			  if(document.documentElement.clientWidth > 1340) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 10,
		          	slidesToScroll: 10
		        });
			  }

			  else if(document.documentElement.clientWidth > 900) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 7,
		          	slidesToScroll: 7
		        });
			  }

			 else if(document.documentElement.clientWidth > 600) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 5,
		          	slidesToScroll: 5
		        });
			  }

			  else if(document.documentElement.clientWidth > 0) {
			    $(".regular-under-header").slick({
		          	dots: false,
		          	autoplay: true,
		           	prevArrow: false,
		            nextArrow: false,
		            // centerMode: true,
		          	autoplaySpeed: 5000,
		          	infinite: true,
		          	slidesToShow: 2,
		          	slidesToScroll: 2
		        });
			  }

			  else {

			  }
        </script>

        <script type="text/javascript">

            $(document).ready(function(){

            $(".btn-mobile-open").click(function(){
                $(".btn-mobile-open").fadeOut(0);
                $(".under-header").animate({'width': "+=250px"}, 500);
                $(".under-header").css({'border': "2px solid white"});
                $(".btn-mobile-close").fadeIn(200);
            });

            $(".btn-mobile-close").click(function(){
                $(".under-header").animate({'width': "-=250px"}, 500);
                $(".under-header").css({'border': "0px"});
                $(".btn-mobile-close").fadeOut(0);
                $(".btn-mobile-open").fadeIn(200);
            });

            });
    </script>



	<!-- =============================================================== -->

  	<!-- end-script -->

    <script src="js/form-validation.js">

    </script>

  </body>
</html>
